//
//  DevgPlayerCache.swift
//  Devg
//
//  Created by Kwan on 11/07/2017.
//  Copyright © 2017 Kwan. All rights reserved.
//

import Foundation
import AVFoundation
import Devg

extension URL {
    func url(withCustomScheme scheme: String) -> URL {
        var components: URLComponents? = URLComponents(url: self, resolvingAgainstBaseURL: false)
        components?.scheme = scheme
        
        return components!.url!
    }
}

public class DevgPlayerCache: NSObject {
    class func savePath() -> NSString {
        let savePath: NSString = NSSearchPathForDirectoriesInDomains(.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] as NSString
        return savePath.appendingPathComponent(DevgPlayerCacheItem.prefix) as NSString
    }
    
    public class func cachedSize() -> UInt {
        var fileSize: UInt = 0
        
        do {
            let files: [String] = try FileManager.default.subpathsOfDirectory(atPath: DevgPlayerCache.savePath() as String)
            
            var fileItems: [[FileAttributeKey : Any]] = Array<Dictionary<FileAttributeKey, Any>>()
            for fileName in files {
                let filePath = DevgPlayerCache.savePath().appendingPathComponent(fileName)
                let file: Dictionary = try FileManager.default.attributesOfItem(atPath: filePath)
                fileItems.append(file)
                fileSize += file[FileAttributeKey.size] as! UInt
            }
        }
        catch {
            e(error)
        }
        
        return fileSize
    }
    
    public class func emptyOldCached(recent: Int) {
        do {
            let files: [String] = try FileManager.default.subpathsOfDirectory(atPath: DevgPlayerCache.savePath() as String)
            
            var fileItems: [[FileAttributeKey : Any]] = Array<Dictionary<FileAttributeKey, Any>>()
            for fileName in files {
                let filePath: String = DevgPlayerCache.savePath().appendingPathComponent(fileName)
                var file: Dictionary = try FileManager.default.attributesOfItem(atPath: filePath)
                file[FileAttributeKey.type] = filePath
                fileItems.append(file)
            }
            
            var isForceSort: Bool = true
            for fileItem in fileItems {
                if let _: Date = fileItem[FileAttributeKey.creationDate] as? Date {}
                else {
                    isForceSort = false
                }
            }
            
            if isForceSort {
                fileItems = fileItems.sorted(by: { $0[FileAttributeKey.creationDate] as! Date > $1[FileAttributeKey.creationDate] as! Date })
            }

            var deleteCount: Int = 0
            if recent < fileItems.count {
                for index in recent..<fileItems.count {
                    if let filePath: String = fileItems[index][FileAttributeKey.type] as? String {
                        try FileManager.default.removeItem(atPath: filePath)
                        deleteCount = deleteCount + 1
                    }
                }
            }
            
            l("\(deleteCount) Deletes", prefix: "DevgPlayerCache")
        }
        catch {
            e(error)
        }
    }
    
    public class func emptyCached() {
        do {
            let files: [String] = try FileManager.default.subpathsOfDirectory(atPath: DevgPlayerCache.savePath() as String)
            
            for fileName in files {
                let filePath: String = DevgPlayerCache.savePath().appendingPathComponent(fileName)
                try FileManager.default.removeItem(atPath: filePath)
            }
        }
        catch {
            e(error)
        }
    }
}

class DevgPlayerCacheItemResourceLoaderDelegate: NSObject {
    var item: DevgPlayerCacheItem?
}

class DevgPlayerCacheItem: AVPlayerItem {
    static let prefix: String = "DevgPlayerCache"
    
    var playingFromCache = false
    var mimeType: String? // is used if we play from cache (with NSData)
    
    let resourceLoaderDelegate: DevgPlayerCacheItemResourceLoaderDelegate = DevgPlayerCacheItemResourceLoaderDelegate()
    
    var session: URLSession?
    var songData: NSMutableData = NSMutableData()
    var response: URLResponse?
    var pendingRequests = Set<AVAssetResourceLoadingRequest>()
    weak var owner: DevgPlayerCacheItem?
    
    var scheme: String?
    var url: URL!
    
    init(url: URL, headers: [String: String]? = nil) {
        self.url = url
        
        let components: URLComponents? = URLComponents(url: self.url, resolvingAgainstBaseURL: false)
        self.scheme = components?.scheme
        
        var options: [String: Any]? = nil
        if let headers: [String: String] = headers {
            options = ["AVURLAssetHTTPHeaderFieldsKey": headers]
        }
        
        var avUrlAsset: AVURLAsset = AVURLAsset(url: self.url, options: options)
        
        avUrlAsset = DevgPlayerCacheAsset.cachedAvUrlAsset(avUrlAsset, originalUrl: self.url)
        
        super.init(asset: avUrlAsset, automaticallyLoadedAssetKeys: nil)

        if !DevgPlayerCacheAsset.isCached(url: self.url) {
            self.resourceLoaderDelegate.item = self
            self.download()
        }
    }
    
    override init(asset: AVAsset, automaticallyLoadedAssetKeys: [String]?) {
        fatalError("not implemented")
    }
    
    // MARK:
    deinit {
        self.session?.invalidateAndCancel()
    }
}

class DevgPlayerCacheAsset {
    class func isCached(url: URL) -> Bool {
        if let cachedFilePath: String = DevgPlayerCacheAsset.cachedFilePath(url), FileManager.default.fileExists(atPath: cachedFilePath) {
            return true
        }
        
        return false
    }
    
    class func cachedAvUrlAsset(_ asset: AVURLAsset, originalUrl: URL) -> AVURLAsset {
        guard let cachedFilePath: String = DevgPlayerCacheAsset.cachedFilePath(originalUrl), FileManager.default.fileExists(atPath: cachedFilePath) else {
            l("Play stream - \(originalUrl.absoluteString)", prefix: "DevgPlayer")
            return asset
        }
        
        let url: URL = URL(fileURLWithPath: cachedFilePath)
        let localAsset: AVURLAsset = AVURLAsset(url: url)
        l("Play cache - \(originalUrl.absoluteString)", prefix: "DevgPlayer")
        
        return localAsset
    }
    
    class func cachedFilePath(_ url: URL?) -> String? {
        guard let url: String = url?.absoluteString, let string: String = url.toURLEncode(.full) else {
            return nil
        }
        
        let fileName: String = string.toBase64()
        var savePath: NSString = DevgPlayerCache.savePath()
        do {
            try FileManager.default.createDirectory(atPath: savePath as String, withIntermediateDirectories: true, attributes: nil)
        }
        catch {
            e(error)
        }
        savePath = savePath.appendingPathComponent(fileName) as NSString
        var fileExtension: String = (url as NSString).pathExtension
        if fileExtension.contains("?") {
            fileExtension = fileExtension.components(separatedBy: "?")[0]
        }
        savePath = savePath.appendingPathExtension(fileExtension)! as NSString
        
        return savePath as String
    }
}

extension DevgPlayerCacheItem {
    func download() {
        if self.session == nil {
            self.startDataRequest(withUrl: self.url)
        }
    }
    
    func startDataRequest(withUrl url: URL) {
        let request = URLRequest(url: url)
        let configuration: URLSessionConfiguration = URLSessionConfiguration.default
        configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        let task = session?.dataTask(with: request)
        task?.resume()
    }
    
    func processPendingRequests() {
        var requestsCompleted = Set<AVAssetResourceLoadingRequest>()
        for loadingRequest in pendingRequests {
            fillInContentInforation(contentInformationRequest: loadingRequest.contentInformationRequest)
            let didRespondCompletely = respondWithDataForRequest(dataRequest: loadingRequest.dataRequest!)
            if didRespondCompletely {
                requestsCompleted.insert(loadingRequest)
                loadingRequest.finishLoading()
            }
        }
        for i in requestsCompleted {
            pendingRequests.remove(i)
        }
    }
    
    func fillInContentInforation(contentInformationRequest: AVAssetResourceLoadingContentInformationRequest?) {
        // if we play from cache we make no url requests, therefore we have no responses, so we need to fill in contentInformationRequest manually
        if playingFromCache {
            contentInformationRequest?.contentType = self.mimeType
            contentInformationRequest?.contentLength = Int64(songData.length)
            contentInformationRequest?.isByteRangeAccessSupported = true
            return
        }
        
        // have no response from the server yet
        if  response == nil {
            return
        }
        
        let mimeType = response?.mimeType
        contentInformationRequest?.contentType = mimeType
        contentInformationRequest?.contentLength = response!.expectedContentLength
        contentInformationRequest?.isByteRangeAccessSupported = true
    }
    
    func respondWithDataForRequest(dataRequest: AVAssetResourceLoadingDataRequest) -> Bool {
        
        let requestedOffset = Int(dataRequest.requestedOffset)
        let requestedLength = dataRequest.requestedLength
        let startOffset = Int(dataRequest.currentOffset)
        
        // Don't have any data at all for this request
        if songData.length < startOffset {
            return false
        }
        
        // This is the total data we have from startOffset to whatever has been downloaded so far
        let bytesUnread = songData.length - Int(startOffset)
        
        // Respond fully or whaterver is available if we can't satisfy the request fully yet
        let bytesToRespond = min(bytesUnread, requestedLength + Int(requestedOffset))
        let nsRange: NSRange = NSMakeRange(startOffset, bytesToRespond)
        dataRequest.respond(with: (songData as NSData).subdata(with: nsRange) as Data)
        
        let didRespondFully = songData.length >= requestedLength + Int(requestedOffset)
        return didRespondFully
    }
}

extension DevgPlayerCacheItem: URLSessionDelegate, URLSessionDataDelegate, URLSessionTaskDelegate {
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        self.songData.append(data)
        self.processPendingRequests()
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        completionHandler(.allow)
        self.songData = NSMutableData()
        self.response = response
        self.processPendingRequests()
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if error != nil {
            self.songData = NSMutableData()
            e(error)
            return
        }
        
        self.processPendingRequests()
        if let cachedFilePath: String = DevgPlayerCacheAsset.cachedFilePath(self.url) {
            self.songData.write(toFile: cachedFilePath, atomically: true)
            if let avUrlAsset: AVURLAsset = self.asset as? AVURLAsset {
                avUrlAsset.resourceLoader.setDelegate(self.resourceLoaderDelegate, queue: DispatchQueue.main)
            }
            l("Cached - \(String.removeNil(self.url.absoluteString))", prefix: "DevgPlayer")
        }
    }
}

extension DevgPlayerCacheItemResourceLoaderDelegate: AVAssetResourceLoaderDelegate {
    func resourceLoader(_ resourceLoader: AVAssetResourceLoader, shouldWaitForLoadingOfRequestedResource loadingRequest: AVAssetResourceLoadingRequest) -> Bool {
        if self.item!.playingFromCache {
            
        } else if self.item!.session == nil { // if we're playing from url, we need to download the file
            if let scheme: String = self.item!.scheme, let interceptedUrl: URL = loadingRequest.request.url?.url(withCustomScheme: scheme) {
                self.item!.startDataRequest(withUrl: interceptedUrl)
            }
        }
        
        self.item?.pendingRequests.insert(loadingRequest)
        self.item?.processPendingRequests()
        return true
    }
    
    func resourceLoader(_ resourceLoader: AVAssetResourceLoader, didCancel loadingRequest: AVAssetResourceLoadingRequest) {
        self.item?.pendingRequests.remove(loadingRequest)
    }
}
