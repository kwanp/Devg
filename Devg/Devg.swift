//
//  Devg.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit

public enum PathType {
    case document
    case cache
}

open class Devg: NSObject {
    // MARK: - System
    
    /// iOS 기기 정보.
    /// https://everymac.com/ultimate-mac-lookup/?search_keywords=iPhone10%2C6
    ///
    /// :returns: 기기 정보.
    open class func device() -> String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        var identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 else {
                return identifier
            }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        identifier = identifier.replacingOccurrences(of: "\0", with: "")
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "iPad6,11", "iPad6,12":                    return "iPad 9.7-Inch (5th Gen)"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9-Inch (2nd Gen)"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5-Inch"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
    /// App Bundle Identifier
    ///
    /// :returns: String
    open class func bundelID() -> String {
        return Devg.defaultPlistValue(forKey: "CFBundleIdentifier") as! String
    }
    
    /// 앱 버전 정보.
    ///
    /// :returns: Float
    open class func appVersion() -> String {
        return Devg.defaultPlistValue(forKey: "CFBundleShortVersionString") as! String
    }
    
    open class func buildVersion() -> String {
        return Devg.defaultPlistValue(forKey: "CFBundleVersion") as! String
    }
    
    open class func appMainScheme() -> String {
        guard let types: Array<Dictionary<String, Any?>> = Devg.defaultPlistValue(forKey: "CFBundleURLTypes") as? Array<Dictionary<String, Any?>> else {
            return ""
        }
        
        if types.count == 1 {
            if let appMainScheme: String = (types[0] as? Dictionary<String, String>)?["CFBundleURLName"] {
                return appMainScheme
            }
        }
        
        for type: Dictionary<String, Any?> in types {
            guard let name: String = type["CFBundleURLName"] as? String else {
                return ""
            }
            
            if name.lowercased().contains("main") || name.lowercased().contains("devg") {
                if let appMainScheme: String = (type["CFBundleURLSchemes"] as? Array<String>)?.get(0) {
                    return appMainScheme
                }
            }
        }
        
        return ""
    }
    
    /// 기본 Plist 파일에서 정보 가져 오기.
    ///
    /// :param: key Plist에 기록된 키.
    /// :returns: 해당 키에 저장된 값.
    open class func defaultPlistValue(forKey key: String) -> Any? {
        return Bundle.main.object(forInfoDictionaryKey: key)
    }
    
    /// NSData로 전달된 기기 토큰을 String으로 변환.
    ///
    /// :param: deviceToken appDelegate에서 전달 받은 기기 토큰 정보.
    /// :retruns: String으로 변환된 토큰.
    open class func tokenString(deviceToken: Data) -> String {
        guard let deviceToken: NSData = deviceToken as NSData? else {
            return ""
        }
        
        let tokenString: String = deviceToken.description.trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
        return tokenString.replacingOccurrences(of: " ", with: "")
    }
    
    
    // MARK: - File
    
    /// OS에서 앱에게 활당해준 경로값을 찾음.
    ///
    /// :param: name 사용자가 사용할 파일 이름.
    /// :param: type OS에서 앱에 활당해준 용도.
    /// :returns: 사용자가 사용할 파일의 실질적인 경로.
    open class func filePath(name: String, type: PathType) -> String {
        var searchPath: FileManager.SearchPathDirectory
        switch type {
        case .document:
            searchPath = .documentDirectory
            break
        case .cache:
            searchPath = .cachesDirectory
        }
        let savePath: NSString = NSSearchPathForDirectoriesInDomains(searchPath, FileManager.SearchPathDomainMask.userDomainMask, true)[0] as NSString
        return savePath.appendingPathComponent(name)
    }
    
    /// 파일 존재 여부 확인.
    ///
    /// :param: name 확인할 사용할 파일 이름.
    /// :param: type 해당 파일을 찾을 폴더의 종류.
    /// :returns: 존재 여부.
    open class func existFile(name: String, type: PathType = .document) -> Bool {
        return FileManager.default.fileExists(atPath: Devg.filePath(name: name, type: type))
    }
    
    /// 파일 삭제.
    ///
    /// :param: name 삭제할 파일 이름.
    /// :param: type 해당 파일을 삭제할 폴더의 종류.
    /// :returns: 삭제 성공 여부.
    public class func deleteFile(name: String, type: PathType = .document) -> Bool {
        if Devg.existFile(name: name, type: type) {
            do {
                try FileManager.default.removeItem(atPath: Devg.filePath(name: name, type: type))
                return true
            }
            catch {
                return false
            }
        }
        else {
            return true
        }
    }
    
    /// 파일 생성.
    ///
    /// :param: name 생성할 파일 이름.
    /// :param: contents 생성할 파일 내용.
    /// :param: type 해당 파일을 생성할 폴더의 종류.
    /// :returns: 생성 성공 여부.
    public class func createFile(name: String, contents: String, type: PathType = .document) -> Bool {
        let _: Bool = Devg.deleteFile(name: name, type: type)
        return FileManager.default.createFile(atPath: Devg.filePath(name: name, type: type), contents: contents.data(using: String.Encoding.utf8, allowLossyConversion: true), attributes: nil)
    }
    
    /// 파일 내용 가져오기.
    ///
    /// :param: name 내용을 가져 올 파일 이름.
    /// :param: type 해당 파일을 가져 올 폴더의 종류.
    /// :returns: 파일 내용.
    public class func getContents(name: String, type: PathType = .document) -> String? {
        do {
            let string: String = try String(contentsOfFile: Devg.filePath(name: name, type: type), encoding: String.Encoding.utf8)
            return string
        }
        catch {
            return nil
        }
    }
    
    /// 앱에 삽입된 파일 내용 가져오기.
    ///
    /// :param: name 내용을 가져 올 파일 이름.
    /// :param: extention 해당 파일을 가져 올 폴더의 종류.
    /// :returns: 파일 내용.
    public class func getContentsFromApp(name: String, extention: String) -> String? {
        if let path: String = Bundle.main.path(forResource: name, ofType: extention) {
            do {
                let string: String = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
                return string
            }
            catch {
                return nil
            }
        }
        else {
            return nil
        }
    }
    
    // MARK: - Store
    
    /// 설정 값을 저장하기.
    ///
    /// :param: key 설정 값을 저장할 키.
    /// :param: value 저장할 설정 값.
    /// :returns: String
    public class func putStore<T>(key: String, value: T) {
        UserDefaults.standard.set(value as Any?, forKey: key)
    }
    
    /// 설정 값을 삭제하기.
    public class func deleteStore(key: String) {
        UserDefaults.standard.removeObject(forKey: key)
    }
    
    /// 설정 값을 가져오기.
    ///
    /// :param: key 설정 값을 가져 올 키.
    /// :return: String
    public class func getStore<T>(key: String) -> T? {
        var returnObject: T? = nil
        if UserDefaults.standard.object(forKey: key) != nil {
            returnObject = UserDefaults.standard.object(forKey: key) as? T
        }
        
        return returnObject
    }
    
    // MARK: - Web
    
    /// Curl Command Line을 URL 형태로 변환.
    ///
    /// :param: curl Curl Command Line
    /// :returns: URL
    public class func curlToUrl(curl: String) -> String {
        let curls: [String] = curl.replacingOccurrences(of: "\"", with: "'").components(separatedBy: "http")
        
        guard let urls: [String] = curls.get(1)?.components(separatedBy: "' -d '") else {
            return ""
        }
        
        var urlString = "http"
        var i = 0
        for url: String in urls {
            if i > 1 {
                urlString = urlString + "&"
            }
            else if i > 0 {
                urlString = urlString + "?"
            }
            
            urlString = urlString + url
            i += 1
        }
        urlString = urlString.replacingOccurrences(of: "'", with: "")
        
        return urlString
    }
    
    /// URL String의 Parameter를 Dictionary로 변환.
    ///
    /// :param: urlString String
    /// :retruns: Dictionary
    public class func urlStringParser(_ urlString: String) -> [String: String] {
        var parse = [String: String]()
        let urls = urlString.components(separatedBy: "?")
        var parameterString: String
        
        if urls.count == 2 {
            parameterString = urls[1]
        }
        else {
            parameterString = urls[0]
        }
        
        let parameters = parameterString.components(separatedBy: "&")
        for parameter in parameters {
            let split = parameter.components(separatedBy: "=")
            if split.count > 1 {
                var value: String = ""
                for i in 1..<split.count {
                    value = value + split[i]
                }
                parse[split[0]] = value
            }
        }
        
        return parse
    }
    
    /// URL String에 get parameter를 추가.
    ///
    /// :param: params 추가할 parameters Dictionary<String, String>
    /// :returns: params가 get으로 추가된 URL String
    public class func addGetParameters(_ params: [String: String], toUrlString: String) -> String {
        var newUrlString = toUrlString
        
        if newUrlString.range(of: "?") != nil{
            newUrlString = "\(newUrlString)&" + Devg.toGetParameters(params)
        }
        else {
            newUrlString = "\(newUrlString)?" + Devg.toGetParameters(params)
        }
        
        return newUrlString
    }
    
    public class func toGetParameters(_ dictionary: [String: String]) -> String {
        var newUrlString = ""
        
        for (key, value) in dictionary {
            if newUrlString.count > 0 {
                newUrlString = "\(newUrlString)&\(key)=\(value)"
            }
            else {
                newUrlString = "\(key)=\(value)"
            }
        }
        
        return newUrlString
    }
    
    public class func sizeToPrettyString(size: UInt) -> String {
        let byteCountFormatter = ByteCountFormatter()
        byteCountFormatter.allowedUnits = .useMB
        byteCountFormatter.countStyle = .file
        let folderSizeToDisplay = byteCountFormatter.string(fromByteCount: Int64(size))
        
        return folderSizeToDisplay
        
    }
}
