//
//  DevgPage.swift
//  Devg
//
//  Created by Kwan on 2016. 1. 27..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import Foundation

public class DevgPage: NSObject {
    private var privateValue: Int = 0
    private var upRunValue: Int = 0
    private var privateStartPage: Int
    var countPerPage: Int
    var isUp: Bool = false
    var isEnd: Bool = false
    var isSetEndOnlyZero: Bool = false
    public var totalCount: Int {
        willSet {
            if self.isUp {
                if self.totalCount < newValue {
                    self.privateValue = self.privateValue + 1
                    
                    if newValue < self.totalCount + self.countPerPage && !self.isSetEndOnlyZero {
                        self.isEnd = true
                    }
                }
                else {
                    self.isEnd = true
                }
                
                self.upCompleted()
            }
            else if self.isFirst() && newValue > 0 && (newValue < self.countPerPage && !self.isSetEndOnlyZero){
                self.isEnd = true
            }
        }
    }
    
    public init(countPerPage: Int, startPage: Int = 1, isSetEndOnlyZero: Bool = false) {
        self.countPerPage = countPerPage
        self.privateStartPage = startPage
        
        self.privateValue = startPage
        self.isSetEndOnlyZero = isSetEndOnlyZero
        self.totalCount = 0
        
    }
    
    // MARK: - Functions
    public func reset() {
        self.privateValue = self.privateStartPage
        self.isUp = false
        self.isEnd = false
        self.totalCount = 0
    }
    
    func up() {
        self.isUp = true
    }
    
    public func up(run: () -> Void, withDisplayIndex displayIndex: Int) {
        if self.int() != self.upRunValue && self.isWillUp(withDisplayIndex: displayIndex) {
            self.isUp = true
            self.upRunValue = self.int()
            run()
        }
    }
    
    public func upCompleted() {
        self.isUp = false
        self.upRunValue = 0
    }
    
    public func upFailed() {
        self.upCompleted()
    }
    
    public func upSuccess() {
        self.upCompleted()
    }
    
    public func int() -> Int {
        if self.isUp {
            return self.privateValue + 1
        }
        else {
            return self.privateValue
        }
    }
    
    public func string() -> String {
        return String(self.int())
    }
    
    public func stringCountPerPage() -> String {
        return String(self.countPerPage)
    }
    
    public func setArray<T>(_ array: inout Array<T>, fromServerArray serverArray: Array<T>) {
        if self.isFirst() {
            array = serverArray
        }
        else {
            array = array + serverArray
        }
        
        self.totalCount = array.count
    }
    
    func isWillUp(withDisplayIndex displayIndex: Int) -> Bool {
        if self.isEnd || displayIndex < self.lastIndex() {
            return false
        }
        
        return true
    }
    
    public func isFirst() -> Bool {
        if self.int() == self.privateStartPage {
            return true
        }
        
        return false
    }
    
    func lastIndex() -> Int {
        return self.totalCount - 1
    }
}
