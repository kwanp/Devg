
//
//  DevgLabel.swift
//  Devg
//
//  Created by Kwan on 2017. 3. 6..
//  Copyright © 2017년 Kwan. All rights reserved.
//

import UIKit
import Foundation
import Devg

public protocol DevgLabelDelegate: class {
    func clickDetection(_ label: DevgLabel, text: String, type: String)
}

@IBDesignable open class DevgLabel: UILabel, CAAnimationDelegate {
    // MARK: - 공통 설정
    @IBInspectable open var scrollEnabled: Bool = false
    @IBInspectable open var scrollSpeed: CGFloat = 5
    @IBInspectable open var scrollFadeLength: CGFloat = 20.0
    
    @IBInspectable open var linkEnabled: Bool = false
    @IBInspectable open var linkTypeMention: Bool = true
    @IBInspectable open var linkTypeTag: Bool = true
    @IBInspectable open var linkTypeURL: Bool = true
    @IBInspectable open var linkTypePhone: Bool = true
    
    @IBInspectable open var mentionColor: UIColor = .blue {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(false)
            }
        }
    }
    @IBInspectable open var mentionSelectedColor: UIColor? {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(false)
            }
        }
    }
    @IBInspectable open var hashtagColor: UIColor = .blue {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(false)
            }
        }
    }
    @IBInspectable open var hashtagSelectedColor: UIColor? {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(false)
            }
        }
    }
    @IBInspectable open var URLColor: UIColor = .blue {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(false)
            }
        }
    }
    @IBInspectable open var URLSelectedColor: UIColor? {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(false)
            }
        }
    }
    @IBInspectable open var phoneColor: UIColor = .blue {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(false)
            }
        }
    }
    @IBInspectable open var phoneSelectedColor: UIColor? {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(false)
            }
        }
    }
    
    @IBInspectable open var lineSpacing: Float = 0 {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(false)
            }
        }
    }
    
    
    open override class var layerClass: AnyClass {
        get {
            return CAReplicatorLayer.self
        }
    }
    
    open override var text: String? {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.text
            }
            else {
                return super.text
            }
        }
        set {
            if self.scrollEnabled == true {
                if self.subLabel.text == newValue {
                    return
                }
                self.subLabel.text = newValue
                self.updateLabel()
                super.text = newValue
            }
            else if self.linkEnabled == true {
                super.text = newValue
                self.updateTextStorage()
            }
        }
    }
    
    open override var textColor: UIColor! {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.textColor
            }
            else {
                return super.textColor
            }
        }
        set {
            if self.scrollEnabled == true {
                self.subLabel.textColor = newValue
                self.updateLabel()
                super.textColor = newValue
            }
            else if self.linkEnabled == true {
                self.updateTextStorage(false)
            }
        }
    }
    
    open override var backgroundColor: UIColor? {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.backgroundColor
            }
            else {
                return super.backgroundColor
            }
        }
        set {
            super.backgroundColor = newValue
            if self.scrollEnabled == true {
                self.subLabel.backgroundColor = newValue
                self.updateLabel()
            }
        }
    }
    
    open override var shadowColor: UIColor? {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.shadowColor
            }
            else {
                return super.shadowColor
            }
        }
        set {
            if self.scrollEnabled == true {
                self.subLabel.shadowColor = newValue
            }
            super.shadowColor = newValue
        }
    }
    
    open override var shadowOffset: CGSize {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.shadowOffset
            }
            else {
                return super.shadowOffset
            }
        }
        set {
            if self.scrollEnabled == true {
                self.subLabel.shadowOffset = newValue
            }
            super.shadowOffset = newValue
        }
    }
    
    open override var highlightedTextColor: UIColor? {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.highlightedTextColor
            }
            else {
                return super.highlightedTextColor
            }
        }
        set {
            if self.scrollEnabled == true {
                self.subLabel.highlightedTextColor = newValue
            }
            super.highlightedTextColor = newValue
        }
    }
    
    open override var isHighlighted: Bool {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.isHighlighted
            }
            else {
                return super.isHighlighted
            }
        }
        set {
            if self.scrollEnabled == true {
                self.subLabel.isHighlighted = newValue
            }
            super.isHighlighted = newValue
        }
    }
    
    open override var isEnabled: Bool {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.isEnabled
            }
            else {
                return super.isEnabled
            }
        }
        set {
            if self.scrollEnabled == true {
                self.subLabel.isEnabled = newValue
            }
            super.isEnabled = newValue
        }
    }
    
    open override var attributedText: NSAttributedString? {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.attributedText
            }
            else {
                return super.attributedText
            }
        }
        set {
            if self.scrollEnabled == true {
                if self.subLabel.attributedText == newValue {
                    return
                }
                self.subLabel.attributedText = newValue
                super.attributedText = newValue
            }
            else if self.linkEnabled == true {
                super.attributedText = newValue
                self.updateTextStorage()
            }
        }
    }
    
    open override var font: UIFont! {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.font
            }
            else {
                return super.font
            }
        }
        set {
            if self.scrollEnabled == true {
                if self.subLabel.font == newValue {
                    return
                }
                self.subLabel.font = newValue
                super.font = newValue
            }
            else if self.scrollEnabled == true {
                self.updateTextStorage(false)
            }
        }
    }
    
    open override var numberOfLines: Int {
        get {
            return super.numberOfLines
        }
        set {
            if self.scrollEnabled == true {
                super.numberOfLines = 1
            }
            else {
                self.textContainer.maximumNumberOfLines = self.numberOfLines
            }
        }
    }
    
    open override var adjustsFontSizeToFitWidth: Bool {
        get {
            return super.adjustsFontSizeToFitWidth
        }
        set {
            if self.scrollEnabled == true {
                super.adjustsFontSizeToFitWidth = false
            }
            else {
                super.adjustsFontSizeToFitWidth = newValue
            }
        }
    }
    
    open override var minimumScaleFactor: CGFloat {
        get {
            return super.minimumScaleFactor
        }
        set {
            if self.scrollEnabled == true {
                super.minimumScaleFactor = 0.0
            }
            else {
                super.minimumScaleFactor = newValue
            }
        }
    }
    
    open override var baselineAdjustment: UIBaselineAdjustment {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.baselineAdjustment
            }
            else {
                return super.baselineAdjustment
            }
        }
        set {
            if self.scrollEnabled == true {
                self.subLabel.baselineAdjustment = newValue
            }
            super.baselineAdjustment = newValue
        }
    }
    
    open override var textAlignment: NSTextAlignment {
        get {
            return super.textAlignment
        }
        set {
            if self.linkEnabled == true {
                self.updateTextStorage(false)
            }
        }
    }
    
    open override var lineBreakMode: NSLineBreakMode {
        get {
            return super.lineBreakMode
        }
        set {
            if self.linkEnabled == true {
                self.textContainer.lineBreakMode = self.lineBreakMode
            }
        }
    }
    
    
    deinit {
        if self.scrollEnabled == true {
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        if self.scrollEnabled == true && self.linkEnabled == true {
            e("스크롤 라벨과 링크라벨을 동시에 사용할수 없습니다")
        }
        if self.scrollEnabled == true {
            self.setup()
        }
        else if linkEnabled == true {
            self.setupLabel()
        }
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        if self.onTouch(touch) {
            return
        }
        super.touchesBegan(touches, with: event)
    }
    
    open override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        if self.onTouch(touch) {
            return
        }
        super.touchesMoved(touches, with: event)
    }
    
    open override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        let _: Bool = self.onTouch(touch)
        super.touchesCancelled(touches, with: event)
    }
    
    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        if self.onTouch(touch) {
            return
        }
        super.touchesEnded(touches, with: event)
    }
    
    open override func drawText(in rect: CGRect) {
        let range = NSRange(location: 0, length: self.textStorage.length)
        
        self.textContainer.size = rect.size
        let newOrigin = self.textOrigin(inRect: rect)
        
        self.layoutManager.drawBackground(forGlyphRange: range, at: newOrigin)
        self.layoutManager.drawGlyphs(forGlyphRange: range, at: newOrigin)
    }
    
    public enum LinkType {
        case mention
        case tag
        case url
        case phone
        case none
    }
    
    enum LinkElement {
        case mention(String)
        case tag(String)
        case url(String)
        case phone(String)
        case none
    }
    
    typealias FilterPredicate = ((String) -> Bool)
    
    static let hashtagRegex = try? NSRegularExpression(pattern: "(?:^|\\s|$)#[\\p{L}0-9_]*", options: [.caseInsensitive])
    static let mentionRegex = try? NSRegularExpression(pattern: "(?:^|\\s|$|[.])@[\\p{L}0-9_]*", options: [.caseInsensitive]);
    static let urlDetector = try? NSRegularExpression(pattern: "(^|[\\s.:;?\\-\\]<\\(])" + "((https?://|www\\.|pic\\.)[-\\w;/?:@&=+$\\|\\_.!~*\\|'()\\[\\]%#,☺]+[\\w/#](\\(\\))?)" + "(?=$|[\\s',\\|\\(\\).:;?\\-\\[\\]>\\)])", options: [.caseInsensitive])
    static let phoneRegex = try? NSRegularExpression(pattern: "(?:^|\\s|$|[.])((\\d{2,3})(?:\\-|\\ )?(\\d{3,4})(?:\\-|\\ )?(\\d{4}))", options: [.caseInsensitive]);
    
    static func getMentions(fromText text: String, range: NSRange) -> [NSTextCheckingResult] {
        guard let mentionRegex = mentionRegex else { return [] }
        return mentionRegex.matches(in: text, options: [], range: range)
    }
    
    static func getHashtags(fromText text: String, range: NSRange) -> [NSTextCheckingResult] {
        guard let hashtagRegex = hashtagRegex else { return [] }
        return hashtagRegex.matches(in: text, options: [], range: range)
    }
    
    static func getURLs(fromText text: String, range: NSRange) -> [NSTextCheckingResult] {
        guard let urlDetector = urlDetector else { return [] }
        return urlDetector.matches(in: text, options: [], range: range)
    }
    
    static func getPhone(fromText text: String, range: NSRange) -> [NSTextCheckingResult] {
        guard let phoneRegex = phoneRegex else { return [] }
        return phoneRegex.matches(in: text, options: [], range: range)
    }
    
    static func createMentionElements(fromText text: String, range: NSRange, filterPredicate: FilterPredicate?) -> [(range: NSRange, element: LinkElement)] {
        let mentions = DevgLabel.getMentions(fromText: text, range: range)
        let nsstring = text as NSString
        var elements: [(range: NSRange, element: LinkElement)] = []
        
        for mention in mentions where mention.range.length > 2 {
            let range = NSRange(location: mention.range.location + 1, length: mention.range.length - 1)
            var word = nsstring.substring(with: range)
            if word.hasPrefix("@") {
                word.remove(at: word.startIndex)
            }
            
            if filterPredicate?(word) ?? true {
                let element = LinkElement.mention(word)
                elements.append((mention.range, element))
            }
        }
        return elements
    }
    
    static func createHashtagElements(fromText text: String, range: NSRange, filterPredicate: FilterPredicate?) -> [(range: NSRange, element: LinkElement)] {
        let hashtags = DevgLabel.getHashtags(fromText: text, range: range)
        let nsstring = text as NSString
        var elements: [(range: NSRange, element: LinkElement)] = []
        
        for hashtag in hashtags where hashtag.range.length > 2 {
            let range = NSRange(location: hashtag.range.location + 1, length: hashtag.range.length - 1)
            var word = nsstring.substring(with: range)
            if word.hasPrefix("#") {
                word.remove(at: word.startIndex)
            }
            
            if filterPredicate?(word) ?? true {
                let element = LinkElement.tag(word)
                elements.append((hashtag.range, element))
            }
        }
        return elements
    }
    
    static func createPhoneElements(fromText text: String, range: NSRange, filterPredicate: FilterPredicate?) -> [(range: NSRange, element: LinkElement)] {
        let phones = DevgLabel.getPhone(fromText: text, range: range)
        let nsstring = text as NSString
        var elements: [(range: NSRange, element: LinkElement)] = []
        
        for phone in phones where phone.range.length > 2 {
            let word = nsstring.substring(with: phone.range)
            
            if filterPredicate?(word) ?? true {
                let element = LinkElement.phone(word)
                elements.append((phone.range, element))
            }
        }
        return elements
    }
    
    static func createURLElements(fromText text: String, range: NSRange) -> [(range: NSRange, element: LinkElement)] {
        let urls = DevgLabel.getURLs(fromText: text, range: range)
        let nsstring = text as NSString
        var elements: [(range: NSRange, element: LinkElement)] = []
        
        for url in urls where url.range.length > 2 {
            let word = nsstring.substring(with: url.range).trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
            let element = LinkElement.url(word)
            elements.append((url.range, element))
        }
        return elements
    }
    
    
    public func filterMention(predicate: @escaping (String) -> Bool) {
        self.mentionFilterPredicate = predicate
        self.updateTextStorage()
    }
    
    public func filterHashtag(predicate: @escaping (String) -> Bool) {
        self.hashtagFilterPredicate = predicate
        self.updateTextStorage()
    }
    
    private var mentionFilterPredicate: ((String) -> Bool)?
    fileprivate var hashtagFilterPredicate: ((String) -> Bool)?
    fileprivate var phoneFilterPredicate: ((String) -> Bool)?
    fileprivate var selectedElement: (range: NSRange, element: LinkElement)?
    fileprivate var heightCorrection: CGFloat = 0
    fileprivate lazy var textStorage = NSTextStorage()
    fileprivate lazy var layoutManager = NSLayoutManager()
    fileprivate lazy var textContainer = NSTextContainer()
    internal lazy var activeElements: [LinkType: [(range: NSRange, element: LinkElement)]] = [.mention: [], .tag: [], .phone: [], .url: []]
    
    fileprivate func setupLabel() {
        if isUserInteractionEnabled == false {
            self.textStorage.addLayoutManager(self.layoutManager)
            self.layoutManager.addTextContainer(self.textContainer)
            self.textContainer.lineFragmentPadding = 0
            self.textContainer.lineBreakMode = self.lineBreakMode
            self.textContainer.maximumNumberOfLines = self.numberOfLines
            isUserInteractionEnabled = true
        }
        self.updateTextStorage()
    }
    
    fileprivate func updateTextStorage(_ parseText: Bool = true) {
        guard let attributedText = self.attributedText, attributedText.length > 0 else {
            self.clearActiveElements()
            self.textStorage.setAttributedString(NSAttributedString())
            setNeedsDisplay()
            return
        }
        
        let mutAttrString = self.addLineBreak(attributedText)
        
        if parseText {
            self.clearActiveElements()
            self.parseTextAndExtractActiveElements(mutAttrString)
        }
        
        self.addLinkAttribute(mutAttrString)
        self.textStorage.setAttributedString(mutAttrString)
        self.setNeedsDisplay()
    }
    
    fileprivate func clearActiveElements() {
        self.selectedElement = nil
        for (type, _) in self.activeElements {
            self.activeElements[type]?.removeAll()
        }
    }
    
    fileprivate func textOrigin(inRect rect: CGRect) -> CGPoint {
        let usedRect = self.layoutManager.usedRect(for: self.textContainer)
        self.heightCorrection = (rect.height - usedRect.height)/2
        let glyphOriginY = self.heightCorrection > 0 ? rect.origin.y + self.heightCorrection : rect.origin.y
        return CGPoint(x: rect.origin.x, y: glyphOriginY)
    }
    
    fileprivate func addLinkAttribute(_ mutAttrString: NSMutableAttributedString) {
        var range = NSRange(location: 0, length: 0)
        var attributes = convertFromNSAttributedStringKeyDictionary(mutAttrString.attributes(at: 0, effectiveRange: &range))
        
        attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.font)] = self.font!
        attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor)] = self.textColor
        mutAttrString.addAttributes(convertToNSAttributedStringKeyDictionary(attributes), range: range)
        
        attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor)] = self.mentionColor
        
        for (type, elements) in self.activeElements {
            switch type {
            case .mention: attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor)] = self.mentionColor
            case .tag: attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor)] = self.hashtagColor
            case .url: attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor)] = self.URLColor
            case .phone: attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor)] = self.phoneColor
            case .none: ()
            }
            
            for element in elements {
                mutAttrString.setAttributes(convertToOptionalNSAttributedStringKeyDictionary(attributes), range: element.range)
            }
        }
    }
    
    fileprivate func parseTextAndExtractActiveElements(_ attrString: NSAttributedString) {
        let textString = attrString.string
        let textLength = textString.utf16.count
        let textRange = NSRange(location: 0, length: textLength)
        
        if self.linkTypeURL == true {
            let urlElements = DevgLabel.createURLElements(fromText: textString, range: textRange)
            self.activeElements[.url]?.append(contentsOf: urlElements)
        }
        
        if self.linkTypeTag == true {
            let hashtagElements = DevgLabel.createHashtagElements(fromText: textString, range: textRange, filterPredicate: self.hashtagFilterPredicate)
            self.activeElements[.tag]?.append(contentsOf: hashtagElements)
        }
        
        if self.linkTypePhone == true {
            let phoneElements = DevgLabel.createPhoneElements(fromText: textString, range: textRange, filterPredicate: self.phoneFilterPredicate)
            self.activeElements[.phone]?.append(contentsOf: phoneElements)
        }
        
        if self.linkTypeMention == true {
            let mentionElements = DevgLabel.createMentionElements(fromText: textString, range: textRange, filterPredicate: self.mentionFilterPredicate)
            self.activeElements[.mention]?.append(contentsOf: mentionElements)
        }
    }
    
    fileprivate func addLineBreak(_ attrString: NSAttributedString) -> NSMutableAttributedString {
        let mutAttrString = NSMutableAttributedString(attributedString: attrString)
        
        var range = NSRange(location: 0, length: 0)
        var attributes = convertFromNSAttributedStringKeyDictionary(mutAttrString.attributes(at: 0, effectiveRange: &range))
        
        let paragraphStyle = attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.paragraphStyle)] as? NSMutableParagraphStyle ?? NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
        paragraphStyle.alignment = textAlignment
        paragraphStyle.lineSpacing = CGFloat(lineSpacing)
        
        attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.paragraphStyle)] = paragraphStyle
        mutAttrString.setAttributes(convertToOptionalNSAttributedStringKeyDictionary(attributes), range: range)
        
        return mutAttrString
    }
    
    fileprivate func updateAttributesWhenSelected(_ isSelected: Bool) {
        guard let selectedElement = self.selectedElement else {
            return
        }
        
        var attributes = convertFromNSAttributedStringKeyDictionary(self.textStorage.attributes(at: 0, effectiveRange: nil))
        if isSelected {
            switch selectedElement.element {
            case .mention(_): attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor)] = self.mentionSelectedColor ?? self.mentionColor
            case .tag(_): attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor)] = self.hashtagSelectedColor ?? self.hashtagColor
            case .url(_): attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor)] = self.URLSelectedColor ?? self.URLColor
            case .phone(_): attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor)] = self.phoneSelectedColor ?? self.phoneColor
            case .none: ()
            }
        } else {
            switch selectedElement.element {
            case .mention(_): attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor)] = self.mentionColor
            case .tag(_): attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor)] = self.hashtagColor
            case .url(_): attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor)] = self.URLColor
            case .phone(_): attributes[convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor)] = self.phoneColor
            case .none: ()
            }
        }
        
        self.textStorage.addAttributes(convertToNSAttributedStringKeyDictionary(attributes), range: selectedElement.range)
        
        setNeedsDisplay()
    }
    
    fileprivate func elementAtLocation(_ location: CGPoint) -> (range: NSRange, element: LinkElement)? {
        guard self.textStorage.length > 0 else {
            return nil
        }
        
        var correctLocation = location
        correctLocation.y = correctLocation.y - self.heightCorrection
        let boundingRect = self.layoutManager.boundingRect(forGlyphRange: NSRange(location: 0, length: self.textStorage.length), in: self.textContainer)
        guard boundingRect.contains(correctLocation) else {
            return nil
        }
        
        let index = self.layoutManager.glyphIndex(for: correctLocation, in: self.textContainer)
        
        for element in self.activeElements.map({ $0.1 }).joined() {
            if index >= element.range.location && index <= element.range.location + element.range.length {
                return element
            }
        }
        
        return nil
    }
    
    func onTouch(_ touch: UITouch) -> Bool {
        let location = touch.location(in: self)
        var avoidSuperCall = false
        
        switch touch.phase {
        case .began, .moved:
            if let element = self.elementAtLocation(location) {
                if element.range.location != self.selectedElement?.range.location || element.range.length != self.selectedElement?.range.length {
                    self.updateAttributesWhenSelected(false)
                    self.selectedElement = element
                    self.updateAttributesWhenSelected(true)
                }
                avoidSuperCall = true
            } else {
                self.updateAttributesWhenSelected(false)
                self.selectedElement = nil
            }
        case .ended:
            guard let selectedElement = self.selectedElement else {
                return avoidSuperCall
            }
            
            switch selectedElement.element {
            case .mention(let userHandle): self.didTapMention(userHandle)
            case .tag(let hashtag): self.didTapHashtag(hashtag)
            case .url(let url): self.didTapStringURL(url)
            case .phone(let phone): self.didTapPhone(phone)
            case .none: ()
            }
            
            let when = DispatchTime.now() + Double(Int64(0.25 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.updateAttributesWhenSelected(false)
                self.selectedElement = nil
            }
            avoidSuperCall = true
        case .cancelled:
            self.updateAttributesWhenSelected(false)
            self.selectedElement = nil
        case .stationary:
            break
        }
        
        return avoidSuperCall
    }
    
    fileprivate func didTapMention(_ username: String) {
        if let click = self.delegate?.clickDetection {
            click(self, username, "Mention")
        }
    }
    
    fileprivate func didTapHashtag(_ hashtag: String) {
        if let click = self.delegate?.clickDetection {
            click(self, hashtag, "Hashtag")
        }
        return
    }
    
    fileprivate func didTapPhone(_ phone: String) {
        if let click = self.delegate?.clickDetection {
            click(self, phone, "Phone")
        }
    }
    
    fileprivate func didTapStringURL(_ stringURL: String) {
        if let click = self.delegate?.clickDetection {
            click(self, stringURL, "URL")
        }
    }
    
    // MARK: -  Scroll Label
    
    public weak var delegate: DevgLabelDelegate? = nil
    
    fileprivate var subLabel: UILabel = UILabel()
    fileprivate var subLabelFrame: CGRect = CGRect.zero
    fileprivate var awayOffset: CGFloat = 0.0
    fileprivate var animationDuration: CGFloat = 0.0
    fileprivate typealias MLAnimationCompletion = (_ finished: Bool) -> ()
    
    fileprivate var maskLayer: CAGradientLayer? {
        return self.layer.mask as! CAGradientLayer?
    }
    
    public enum SpeedLimit {
        case rate(CGFloat)
        case duration(CGFloat)
        
        var value: CGFloat {
            switch self {
            case .rate(let rate):
                return rate
            case .duration(let duration):
                return duration
            }
        }
    }
    
    fileprivate var repliLayer: CAReplicatorLayer {
        return self.layer as! CAReplicatorLayer
    }
    
    public var isPaused: Bool {
        return (self.subLabel.layer.speed == 0.0)
    }
    
    
    
    public var speed: SpeedLimit = .duration(7.0) {
        didSet {
            switch (self.speed, oldValue) {
            case (.rate(let a), .rate(let b)) where a == b:
                return
            case (.duration(let a), .duration(let b)) where a == b:
                return
            default:
                self.updateLabel()
            }
        }
    }
    
    fileprivate struct Scroller {
        typealias Scroll = (layer: CALayer, anim: CAKeyframeAnimation)
        init(generator gen: @escaping (_ interval: CGFloat, _ delay: CGFloat) -> [Scroll]) {
            self.generator = gen
        }
        
        let generator: (_ interval: CGFloat, _ delay: CGFloat) -> [Scroll]
        var scrolls: [Scroll]? = nil
        
        mutating func generate(_ interval: CGFloat, delay: CGFloat) -> [Scroll] {
            if let existing = self.scrolls {
                return existing
            }
            else {
                self.scrolls = self.generator(interval, delay)
                return self.scrolls!
            }
        }
    }
    
    
    
    func setup() {
        if self.subLabel.tag != 100 {
            self.subLabel = UILabel(frame: self.bounds)
            self.subLabel.tag = 100
            self.subLabel.text = super.text
            self.subLabel.font = super.font
            self.subLabel.textColor = super.textColor
            self.subLabel.backgroundColor = super.backgroundColor ?? UIColor.clear
            self.subLabel.shadowColor = super.shadowColor
            self.subLabel.shadowOffset = super.shadowOffset
            self.subLabel.layer.anchorPoint = CGPoint.zero
            super.textColor = UIColor.clear
            super.clipsToBounds = true
            super.numberOfLines = 1
            self.addSubview(self.subLabel)
            
            NotificationCenter.default.addObserver(self, selector: #selector(DevgLabel.restartLabel), name: UIApplication.didBecomeActiveNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(DevgLabel.shutdownLabel), name: UIApplication.didEnterBackgroundNotification, object: nil)
            self.updateLabel()
        }
    }
    
    func updateLabel() {
        let expectedLabelSize = self.subLabelSize()
        invalidateIntrinsicContentSize()
        self.maskLayer?.removeAllAnimations()
        self.subLabel.layer.removeAllAnimations()
        
        if self.labelShouldScroll() == false {
            self.subLabel.textAlignment = super.textAlignment
            self.subLabel.lineBreakMode = super.lineBreakMode
            let labelFrame: CGRect = CGRect(x: 0.0, y: 0.0, width: bounds.size.width, height: bounds.size.height).integral
            self.subLabelFrame = labelFrame
            self.awayOffset = 0.0
            self.repliLayer.instanceCount = 1;
            self.subLabel.frame = labelFrame
            
            self.applyGradientMask(fadeLength: self.scrollFadeLength, animated: true)
            
            return
        }
        
        self.speed = .duration(self.scrollSpeed)
        
        let minTrailing = max(max(0.0, 0.0), self.scrollFadeLength)
        
        self.subLabelFrame = CGRect(x: 0.0, y: 0.0, width: expectedLabelSize.width, height: bounds.size.height).integral
        self.awayOffset = -(self.subLabelFrame.size.width + minTrailing)
        self.subLabel.frame = self.subLabelFrame
        self.repliLayer.instanceCount = 2
        self.repliLayer.instanceTransform = CATransform3DMakeTranslation(-self.awayOffset, 0.0, 0.0)
        
        self.animationDuration = {
            switch self.speed {
            case .rate(let rate):
                return CGFloat(abs(self.awayOffset) / rate)
            case .duration(let duration):
                return duration
            }
        }()
        self.applyGradientMask(fadeLength: self.scrollFadeLength, animated: true)
        self.scrollContinuous(interval: self.animationDuration, delay: 0)
    }
    
    private func scrollContinuous(interval: CGFloat, delay: CGFloat) {
        let homeOrigin = self.subLabelFrame.origin
        let awayOrigin = CGPoint(x: self.subLabelFrame.origin.x + self.awayOffset, y: self.subLabelFrame.origin.y)
        let scroller = Scroller(generator: { (interval: CGFloat, delay: CGFloat) -> [(layer: CALayer, anim: CAKeyframeAnimation)] in
            let values: [NSValue] = [NSValue(cgPoint: homeOrigin), NSValue(cgPoint: homeOrigin), NSValue(cgPoint: awayOrigin)]
            let layer = self.subLabel.layer
            let animation = CAKeyframeAnimation(keyPath: "position")
            let timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            let totalDuration: CGFloat
            totalDuration = delay + interval
            animation.keyTimes = [0.0, NSNumber(value: Float(delay/totalDuration)), 1.0]
            animation.timingFunctions = [timingFunction,timingFunction]
            animation.values = values
            return [(layer: layer, anim: animation)]
        })
        self.scroll(interval: interval, delay: delay, scroller: scroller, fader: nil)
    }
    
    private func scroll(interval: CGFloat, delay: CGFloat, scroller: Scroller, fader: CAKeyframeAnimation?) {
        var scroller = scroller
        if self.superview == nil {
            return
        }
        if self.window == nil {
            return
        }
        let viewController = self.traverseResponderChainForFirstViewController()
        if viewController != nil {
            if !viewController!.isViewLoaded {
                return
            }
        }
        
        CATransaction.begin()
        let speed = ((self.subLabelSize().width / 10) / interval)
        let transDuration = TimeInterval(speed)
        CATransaction.setAnimationDuration(transDuration)
        
        let gradientAnimation: CAKeyframeAnimation?
        if self.scrollFadeLength > 0.0 {
            if let setupAnim = self.maskLayer?.animation(forKey: "setupFade") as? CABasicAnimation, let finalColors = setupAnim.toValue as? [CGColor] {
                self.maskLayer?.colors = finalColors
            }
            self.maskLayer?.removeAnimation(forKey: "setupFade")
            if let previousAnimation = fader {
                gradientAnimation = previousAnimation
            } else {
                gradientAnimation = self.keyFrameAnimationForGradient(fadeLength: self.scrollFadeLength, interval: interval, delay: delay)
            }
            self.maskLayer?.add(gradientAnimation!, forKey: "gradient")
        } else {
            gradientAnimation = nil
        }
        
        let completion = CompletionBlock<MLAnimationCompletion>({ (finished: Bool) -> () in
            guard finished else {
                return
            }
            guard self.window != nil else {
                return
            }
            guard self.subLabel.layer.animation(forKey: "position") == nil else {
                return
            }
            if self.labelShouldScroll() == true {
                self.scroll(interval: interval, delay: delay, scroller: scroller, fader: gradientAnimation)
            }
        })
        let scrolls = scroller.generate(interval, delay: delay)
        for (index, scroll) in scrolls.enumerated() {
            let layer = scroll.layer
            let anim = scroll.anim
            if index == 0 {
                anim.setValue(completion as AnyObject, forKey: "MLAnimationCompletion")
                anim.delegate = self
            }
            layer.add(anim, forKey: "position")
        }
        CATransaction.commit()
        
    }
    
    private func keyFrameAnimationForGradient(fadeLength: CGFloat, interval: CGFloat, delay: CGFloat) -> CAKeyframeAnimation {
        let values: [[CGColor]]
        let keyTimes: [CGFloat]
        let transp = UIColor.clear.cgColor
        let opaque = UIColor.black.cgColor
        let animation = CAKeyframeAnimation(keyPath: "colors")
        let timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        let totalDuration = delay + interval
        let offsetDistance = self.awayOffset
        let startFadeFraction = abs((self.subLabel.bounds.size.width) / offsetDistance)
        let startFadeTimeFraction = timingFunction.durationPercentage(forPositionPercentage: startFadeFraction, duration: totalDuration)
        let startFadeTime = delay + CGFloat(startFadeTimeFraction) * interval
        
        keyTimes = [0.0, delay/totalDuration, (delay + 0.2)/totalDuration, startFadeTime/totalDuration, (startFadeTime + 0.1)/totalDuration, 1.0]
        
        let mask = self.maskLayer?.presentation() as CAGradientLayer?
        let currentValues = mask?.colors as? [CGColor]
        
        values = [currentValues ?? [opaque, opaque, opaque, transp], [opaque, opaque, opaque, transp], [transp, opaque, opaque, transp], [transp, opaque, opaque, transp], [opaque, opaque, opaque, transp], [opaque, opaque, opaque, transp]]
        
        animation.values = values
        animation.keyTimes = keyTimes as [NSNumber]?
        animation.timingFunctions = [timingFunction, timingFunction, timingFunction, timingFunction]
        
        return animation
    }
    
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if anim is CABasicAnimation {
            if let setupAnim = self.maskLayer?.animation(forKey: "setupFade") as? CABasicAnimation, let finalColors = setupAnim.toValue as? [CGColor] {
                self.maskLayer?.colors = finalColors
            }
            self.maskLayer?.removeAnimation(forKey: "setupFade")
        } else {
            if let completion = anim.value(forKey: "MLAnimationCompletion") as? CompletionBlock<MLAnimationCompletion> {
                completion.f(flag)
            }
        }
    }
    
    private func subLabelSize() -> CGSize {
        let maximumLabelSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)
        var expectedLabelSize = self.sizeThatFits(maximumLabelSize)
        expectedLabelSize.width = min(expectedLabelSize.width, 5461.0)
        expectedLabelSize.height = bounds.size.height
        return expectedLabelSize
    }
    
    private func labelShouldScroll() -> Bool {
        if self.subLabel.text == nil {
            return false
        }
        if self.subLabel.text!.isEmpty {
            return false
        }
        
        let labelTooLarge = (self.subLabelSize().width) > self.bounds.size.width
        let animationHasDuration = self.speed.value > 0.0
        return (labelTooLarge && animationHasDuration)
    }
    
    private func applyGradientMask(fadeLength: CGFloat, animated: Bool) {
        self.maskLayer?.removeAllAnimations()
        
        if (fadeLength <= 0.0) {
            self.layer.mask = nil
            return
        }
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        let gradientMask: CAGradientLayer
        if let currentMask = self.maskLayer {
            gradientMask = currentMask
        } else {
            gradientMask = CAGradientLayer()
            gradientMask.shouldRasterize = true
            gradientMask.rasterizationScale = UIScreen.main.scale
            gradientMask.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradientMask.endPoint = CGPoint(x: 1.0, y: 0.5)
            
            let leftFadeStop = fadeLength/self.bounds.size.width
            let rightFadeStop = fadeLength/self.bounds.size.width
            gradientMask.locations = [0.0, NSNumber(value: Float(leftFadeStop)), NSNumber(value: 1.0 - Double(rightFadeStop)), 1.0]
        }
        
        
        let transparent = UIColor.clear.cgColor
        let opaque = UIColor.black.cgColor
        
        
        self.layer.mask = gradientMask
        
        gradientMask.bounds = self.layer.bounds
        gradientMask.position = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        
        let adjustedColors: [CGColor]
        let trailingFadeNeeded = self.labelShouldScroll()
        adjustedColors = [opaque, opaque, opaque, (trailingFadeNeeded ? transparent : opaque)]
        
        if (animated) {
            CATransaction.commit()
            let colorAnimation = CABasicAnimation(keyPath: "colors")
            colorAnimation.fromValue = gradientMask.colors
            colorAnimation.toValue = adjustedColors
            colorAnimation.fillMode = CAMediaTimingFillMode.forwards
            colorAnimation.isRemovedOnCompletion = false
            colorAnimation.delegate = self
            gradientMask.add(colorAnimation, forKey: "setupFade")
        } else {
            gradientMask.colors = adjustedColors
            CATransaction.commit()
        }
    }
    
    @objc public func shutdownLabel() {
        if self.scrollEnabled == true {
            self.maskLayer?.removeAllAnimations()
            self.subLabel.layer.removeAllAnimations()
            applyGradientMask(fadeLength: self.scrollFadeLength, animated: false)
        }
    }
    
    
    @objc public func restartLabel() {
        if self.scrollEnabled == true {
            self.shutdownLabel()
            self.scrollContinuous(interval: self.animationDuration, delay: 0)
        }
    }
    
    public func pauseLabel() {
        if self.scrollEnabled == true {
            var awayFromHome: Bool = true
            let presentationLayer: CALayer = CALayer(layer: self.subLabel.layer.presentation)
            awayFromHome = !(presentationLayer.position.x == self.subLabelFrame.origin.x)
            if self.isPaused == true && awayFromHome == false {
                return
            }
            
            let labelPauseTime = self.subLabel.layer.convertTime(CACurrentMediaTime(), from: nil)
            self.subLabel.layer.speed = 0.0
            self.subLabel.layer.timeOffset = labelPauseTime
            
            let gradientPauseTime = self.maskLayer?.convertTime(CACurrentMediaTime(), from:nil)
            self.maskLayer?.speed = 0.0
            self.maskLayer?.timeOffset = gradientPauseTime!
        }
    }
    
    public func reuseLabel() {
        if self.scrollEnabled == true {
            if self.isPaused == false {
                return
            }
            
            let labelPauseTime = self.subLabel.layer.timeOffset
            self.subLabel.layer.speed = 1.0
            self.subLabel.layer.timeOffset = 0.0
            self.subLabel.layer.beginTime = 0.0
            self.subLabel.layer.beginTime = self.subLabel.layer.convertTime(CACurrentMediaTime(), from: nil) - labelPauseTime
            
            let gradientPauseTime = self.maskLayer?.timeOffset
            self.maskLayer?.speed = 1.0
            self.maskLayer?.timeOffset = 0.0
            self.maskLayer?.beginTime = 0.0
            self.maskLayer?.beginTime = self.maskLayer!.convertTime(CACurrentMediaTime(), from:nil) - gradientPauseTime!
        }
    }
}

private extension CAMediaTimingFunction {
    func durationPercentage(forPositionPercentage positionPercentage: CGFloat, duration: CGFloat) -> CGFloat {
        let controlPoints = self.controlPoints()
        let epsilon: CGFloat = 1.0 / (100.0 * CGFloat(duration))
        let tFound = self.solveT(forY: positionPercentage, epsilon: epsilon, controlPoints: controlPoints)
        let durationPercentage = self.XforCurve(at: tFound, controlPoints: controlPoints)
        
        return durationPercentage
    }
    
    func solveT(forY y0: CGFloat, epsilon: CGFloat, controlPoints: [CGPoint]) -> CGFloat {
        var t0 = y0
        var t1 = y0
        var f0, df0: CGFloat
        
        for _ in 0..<15 {
            t0 = t1
            f0 = self.XforCurve(at: t0, controlPoints:controlPoints) - y0
            if (abs(f0) < epsilon) {
                return t0
            }
            df0 = self.derivativeCurveYValue(at: t0, controlPoints:controlPoints)
            if (abs(df0) < 1e-6) {
                break
            }
            t1 = t0 - f0/df0
        }
        return t0
    }
    
    func YforCurveAt(t: CGFloat, controlPoints:[CGPoint]) -> CGFloat {
        let P0 = controlPoints[0]
        let P1 = controlPoints[1]
        let P2 = controlPoints[2]
        let P3 = controlPoints[3]
        let y0 = (pow((1.0 - t),3.0) * P0.y)
        let y1 = (3.0 * pow(1.0 - t, 2.0) * t * P1.y)
        let y2 = (3.0 * (1.0 - t) * pow(t, 2.0) * P2.y)
        let y3 = (pow(t, 3.0) * P3.y)
        
        return y0 + y1 + y2 + y3
    }
    
    func XforCurve(at t: CGFloat, controlPoints: [CGPoint]) -> CGFloat {
        let P0 = controlPoints[0]
        let P1 = controlPoints[1]
        let P2 = controlPoints[2]
        let P3 = controlPoints[3]
        let x0 = (pow((1.0 - t),3.0) * P0.x)
        let x1 = (3.0 * pow(1.0 - t, 2.0) * t * P1.x)
        let x2 = (3.0 * (1.0 - t) * pow(t, 2.0) * P2.x)
        let x3 = (pow(t, 3.0) * P3.x)
        
        return x0 + x1 + x2 + x3
    }
    
    func derivativeCurveYValue(at t: CGFloat, controlPoints: [CGPoint]) -> CGFloat {
        let P0 = controlPoints[0]
        let P1 = controlPoints[1]
        let P2 = controlPoints[2]
        let P3 = controlPoints[3]
        
        let dy0 = (P0.y + 3.0 * P1.y + 3.0 * P2.y - P3.y) * -3.0
        let dy1 = t * (6.0 * P0.y + 6.0 * P2.y)
        let dy2 = (-3.0 * P0.y + 3.0 * P1.y)
        
        return dy0 * pow(t, 2.0) + dy1 + dy2
    }
    
    func controlPoints() -> [CGPoint] {
        var point: [Float] = [0.0, 0.0]
        var pointArray = [CGPoint]()
        for i in 0...3 {
            self.getControlPoint(at: i, values: &point)
            pointArray.append(CGPoint(x: CGFloat(point[0]), y: CGFloat(point[1])))
        }
        
        return pointArray
    }
}

private class CompletionBlock<T> {
    let f : T
    init (_ f: T) { self.f = f }
}

private extension UIResponder {
    func traverseResponderChainForFirstViewController() -> UIViewController? {
        if let nextResponder = self.next {
            if nextResponder is UIViewController {
                return nextResponder as? UIViewController
            }
            else if nextResponder is UIView {
                return nextResponder.traverseResponderChainForFirstViewController()
            }
            else {
                return nil
            }
        }
        return nil
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKeyDictionary(_ input: [NSAttributedString.Key: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToNSAttributedStringKeyDictionary(_ input: [String: Any]) -> [NSAttributedString.Key: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
