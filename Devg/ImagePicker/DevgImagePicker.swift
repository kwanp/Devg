//
//  DevgImagePicker.swift
//  Devg
//
//  Created by Kwan on 2016. 3. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//
//
//  # DevgImagePicker
//
//  ## 특징
//  기존 UIImagePickerController대신 최신 프레임워크인 Photos를 이용
//  기본 사진뿐 아니라 클라우드, 사용자 폴더, 고속연사촬영, 스크린샷등 폴더 형태로 재공
//  기존 델리게이트의 복잡함을 빼고 Bolck 으로 간단히 선택 & 촬영후 UIImage 와 NSData로 받을 수 있다
//  아이폰 모든 해상도 대응
//  앨범중에 동영상이나, 타임렙스, 슬로모션은 자동으로 숨길까? 사진으로 취급할까? -- 미정(우선은 사진으로 취급)
//
//  ## 요구사항 및 지원사항
//  iOS 8.0 이상, iPhone 4s 이상
//  DeVice Orientation - Portrait만 지원
//
//  ## 한글 앨범명 사용법
//  1. 적용하고자 하는 프로젝트내 info.Plist 클릭
//  2. Custom iOS Target Properties 에 Key 'Localizations' 추가
//  3. item0: English(Default로 추가되어 있음), item1: Korean 등록
//  4. Enjoy!
//
//  ## 커스텀 이미지 사용법
//  1. 적용하고자 하는 이미지를 프로젝트내 Assets.xcassets에 추가
//  2. PhotoPicker.sharedInstance.show() 할때 topImageString에 이미지 이름 String으로 넣는다
//  3. Enjoy!
//
//  ## StatusBar Color 변경적용하기
//  1. 적용하고자 하는 프로젝트내 info.Plist 클릭
//  2. Custom iOS Targer Properties에 Key 'View controller-based status bar appearance' 추가
//  3. Boolean = NO 로 설정
//  4. Enjoy!
//
//  ## Error Code설명
//  1. 901 사진첩 권한 없음
//  2. 902 카메라 권한 없음
//  3. 904 사진 불러오기 실패

import UIKit
import Devg
import Photos
import AssetsLibrary

typealias ImageSelectBlock = ((_ image: UIImage, _ imageData: Data) -> Void)
typealias ImageErrorBlock = ((_ errorCode: Int, _ errorString: String) -> Void)

extension IndexSet {
    func indexPathsForSection(_ section: Int) -> [IndexPath] {
        return self.map { IndexPath(item: $0, section: section) }
    }
}

public class DevgImagePicker {
    var statusBarStyle: UIStatusBarStyle = .lightContent
    var viewControllers: [UIViewController] = Array<UIViewController>()
    
    private var _initPicker: DevgImagePickerViewController!
    var initPicker: DevgImagePickerViewController! {
        get {
            if self._initPicker == nil {
                self._initPicker = self.viewControllers[0] as? DevgImagePickerViewController
            }
            return self._initPicker
        }
    }
    
    private var _initDetailPicker: DevgImageDetailViewController!
    var initDetailPicker: DevgImageDetailViewController! {
        get {
            if self._initDetailPicker == nil {
                self._initDetailPicker = self.viewControllers[1] as? DevgImageDetailViewController
            }
            return self._initDetailPicker
        }
    }
    
    private static var _sharedInstance: DevgImagePicker!
    public static var shared: DevgImagePicker {
        get {
            if self._sharedInstance == nil {
                self._sharedInstance = DevgImagePicker()
                self._sharedInstance.viewControllers = DevgImagePicker.imagePickerFormNib()
            }
            return self._sharedInstance
        }
    }
    
    
    private class func imagePickerFormNib() -> [UIViewController] {
        let nibs = UINib(nibName: "DevgImagePicker", bundle: Bundle(identifier: "net.devg.DevgImagePicker")).instantiate(withOwner: nil, options: nil)
        return nibs as! [UIViewController]
    }
    
    public func show(_ viewController: UIViewController, topImageNamed: String? = nil, textColor: UIColor = UIColor.white, statusBarStyle: UIStatusBarStyle = .lightContent , selectImage: @escaping (_ image: UIImage, _ imageData: Data) -> Void, errorImage: @escaping (_ errorCode: Int, _ errorString: String) -> Void) {
        self.statusBarStyle = statusBarStyle
        self.initPicker.show(viewController, topImageNamed: topImageNamed, textColor: textColor, statusBarStyle: statusBarStyle, selectImage: selectImage, errorImage: errorImage)
    }
}

class DevgImagePickerViewController: UIViewController, PHPhotoLibraryChangeObserver, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIViewControllerPreviewingDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var topLineView: UIView!
    @IBOutlet weak var topButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    
    
    var navi: UINavigationController!
    var flowLayout: UICollectionViewFlowLayout!
    var textColors: UIColor!
    var photoList: PHFetchResult<PHAsset>!
    var albumList: [PHFetchResult<PHAssetCollection>] = Array<PHFetchResult<PHAssetCollection>>()
    var photoCollection: PHAssetCollection = PHAssetCollection()
    var gridThumbnailSize: CGSize!
    //    TODO: - 임시
    //    var imageManager: PHCachingImageManager = PHCachingImageManager()
    
    var selectImageBlock: ImageSelectBlock!
    var errorImageBlock: ImageErrorBlock!
    
    var imagePicker: UIImagePickerController = UIImagePickerController()
    var oldStatusBarStyle: UIStatusBarStyle!
    var newStatusBarStyle: UIStatusBarStyle!

    var preView: UIViewControllerPreviewing? = nil
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.oldStatusBarStyle = UIApplication.shared.statusBarStyle
        self.collectionView.register(DevgImagePickerCollectionViewCell.self, forCellWithReuseIdentifier: "CollectionCells")
        self.flowLayout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        self.tableView.register(DevgImagePickerTableViewCell.self, forCellReuseIdentifier: "TableCell")
        
        let allPhotoOptions: PHFetchOptions = PHFetchOptions()
        allPhotoOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        //    TODO: - 임시 미디어 타입에 대한 결정을 여기서해야 한다
        //    allPhotoOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.Image.rawValue)
        
        
        self.photoList = PHAsset.fetchAssets(with: allPhotoOptions)
        if let list = self.photoList as? PHFetchResult<PHAssetCollection> {
            self.albumList.append(list)
        }
        
        let smartAlbum = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .albumRegular, options: nil)
        //    TODO: - 임시 미디어 타입에 대한 결정을 여기서해야 한다
        /*
        smartAlbum.enumerateObjectsUsingBlock { (assetCollection, index, stop) -> Void in
        if let assetCollection: PHAssetCollection = assetCollection as? PHAssetCollection {
        let assetsFetchResult: PHFetchResult = PHAsset.fetchAssetsInAssetCollection(assetCollection, options: allPhotoOptions)
        print("sub album title is \(assetCollection.localizedTitle), count is \(assetsFetchResult.count)")
        }
        }
        */
        self.albumList.append(smartAlbum)
        
        let topLevelUserCollections = PHCollectionList.fetchTopLevelUserCollections(with: nil)
        //    TODO: - 임시 미디어 타입에 대한 결정을 여기서해야 한다
        /*
        topLevelUserCollections.enumerateObjectsUsingBlock { (assetCollection, index, stop) -> Void in
        if let assetCollection: PHAssetCollection = assetCollection as? PHAssetCollection {
        let assetsFetchResult: PHFetchResult = PHAsset.fetchAssetsInAssetCollection(assetCollection, options: allPhotoOptions)
        print("sub album title is \(assetCollection.localizedTitle), count is \(assetsFetchResult.count)")
        }
        }
        */
        if let list = topLevelUserCollections as? PHFetchResult<PHAssetCollection> {
            self.albumList.append(list)
        }
        
        let scale: CGFloat = UIScreen.main.scale
        let cellSize: CGSize = self.flowLayout.itemSize
        self.gridThumbnailSize = CGSize(width: cellSize.width * scale, height: cellSize.height * scale);
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        PHPhotoLibrary.shared().register(self)
        UIApplication.shared.setStatusBarStyle(self.newStatusBarStyle, animated: true)
        self.tableView.reloadData()
        self.collectionView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
        UIApplication.shared.setStatusBarStyle(self.oldStatusBarStyle, animated: true)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if #available(iOS 9.0, *) {
            if( traitCollection.forceTouchCapability == .available){
                if self.preView == nil {
                    self.preView = registerForPreviewing(with: self, sourceView: self.collectionView)
                }
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return DevgImagePicker.shared.statusBarStyle
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: UIViewControllerPreviewingDelegate methods
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        if let indexPath = self.collectionView.indexPathForItem(at: location) {
            if let cell = self.collectionView.cellForItem(at: indexPath) {
                if let detailView = DevgImagePicker.shared.initDetailPicker {
                    detailView.asset = self.photoList[indexPath.item - 1]
                    detailView.assetCollection = self.photoCollection
                    detailView.topViewImageView.image = self.topImageView.image
                    detailView.selectImageBlock = self.selectImageBlock
                    detailView.errorImageBlock = self.errorImageBlock
                    detailView.textColor = self.textColors
                    detailView.oldStatusBarStyle = self.oldStatusBarStyle
                    detailView.newStatusBarStyle = self.newStatusBarStyle
                    detailView.preferredContentSize = CGSize(width: 0.0, height: 350)
                    detailView.isPreview = true
                    if #available(iOS 9.0, *) {
                        previewingContext.sourceRect = cell.frame
                    } else {
                        // Fallback on earlier versions
                    }
                    return detailView
                }
                return self
            }
            return self
        } 
        return self
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        if let detailView: DevgImageDetailViewController = viewControllerToCommit as? DevgImageDetailViewController {
            detailView.isPreview = false
            show(detailView, sender: self)
        }
        
    }
    
    // MARK: - Functions
    func show(_ viewController: UIViewController, topImageNamed: String?, textColor: UIColor, statusBarStyle: UIStatusBarStyle, selectImage: @escaping ImageSelectBlock, errorImage: @escaping ImageErrorBlock) {
        self.selectImageBlock = selectImage
        self.errorImageBlock = errorImage
        self.textColors = textColor
        self.newStatusBarStyle = statusBarStyle
        if ALAssetsLibrary.authorizationStatus() == .denied {
            self.errorImageBlock(901, "사진 접근 권한 없음")
            return
        }
        
        if AVCaptureDevice.authorizationStatus(for: AVMediaType(rawValue: convertFromAVMediaType(AVMediaType.video))) == .denied {
            self.errorImageBlock(902,"카메라 접근 권한 없음")
            return
        }
        
        self.navi = UINavigationController(rootViewController: self)
        self.navi.navigationBar.isHidden = true
        self.navi.interactivePopGestureRecognizer?.delegate = nil
        self.topButton.setTitle("모든사진 ▼", for: .normal)
        self.topButton.setTitle("모든사진 ▲", for: .selected)
        self.flowLayout.sectionInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        self.closeButton.setTitleColor(self.textColors, for: .normal)
        self.topButton.setTitleColor(self.textColors, for: .normal)
        
        if let topBackgroundImageString: String = topImageNamed {
            self.topImageView.image = UIImage(named: topBackgroundImageString)
            self.topLineView.isHidden = true
        } else {
            self.topImageView.image = nil
            self.topLineView.isHidden = false
        }
        
        viewController.present(self.navi, animated: true) { () -> Void in
            self.tableView.isHidden = false
            self.tableViewBottomConstraint.constant = self.collectionView.frame.size.height
            self.view.layoutIfNeeded()
        }
        self.collectionView.reloadData()
    }
    
    @IBAction func closeButtonPush(_ sender: Any) {
        self.dismiss(animated: true) { () -> Void in
            self.tableView.isHidden = true
            self.topButton.isSelected = false
            self.tableViewBottomConstraint.constant = 0
            self.photoList = self.albumList[0] as? PHFetchResult<PHAsset>
            
        }
    }
    
    @IBAction func topButtonPush(_ sender: UIButton) {
        if sender.isSelected == true {
            sender.isSelected = false
            self.tableViewBottomConstraint.constant = self.collectionView.frame.size.height
        }
        else {
            sender.isSelected = true
            self.tableViewBottomConstraint.constant = 0
        }
        
        UIView.animate(withDuration: 0.3) { () -> Void in
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let assetsFetchResults: PHFetchResult = self.photoList {
            return assetsFetchResults.count + 1
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: DevgImagePickerCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCells", for: indexPath as IndexPath) as! DevgImagePickerCollectionViewCell
        
        if indexPath.item == 0 {
            if let cameraIcon: UIImage = UIImage(named: "ShotCamera", in: Bundle(identifier: "net.devg.DevgImagePicker"), compatibleWith: nil) {
                cell.imageView.image = cameraIcon
                cell.imageView.contentMode = .center
            }
        }
        else {
            if let assetsFetchResults: PHFetchResult = self.photoList {
                let assset: PHAsset = assetsFetchResults[indexPath.item - 1]
                let options: PHImageRequestOptions = PHImageRequestOptions()
                options.deliveryMode = .highQualityFormat
                options.isNetworkAccessAllowed = true
                PHImageManager.default().requestImage(for: assset, targetSize: self.gridThumbnailSize, contentMode: .aspectFill, options: options, resultHandler: { (image, info) -> Void in
                    cell.imageView.contentMode = .scaleAspectFill
                    cell.imageView.image = image
                })
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.size.width / 3) - 2, height: (self.view.frame.size.width / 3) - 2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return (self.view.frame.size.width - (((self.view.frame.size.width / 3) - 2) * 3)) / 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return (self.view.frame.size.width - (((self.view.frame.size.width / 3) - 2) * 3)) / 2
    }
    
    class DevgImagePickerCollectionViewCell: UICollectionViewCell {
        var imageView: UIImageView!
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.contentView.backgroundColor = UIColor.lightGray
            
            self.imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
            self.imageView.backgroundColor = UIColor.clear
            self.imageView.contentMode = .scaleAspectFill
            self.imageView.clipsToBounds = true
            self.contentView.addSubview(self.imageView)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 0 {
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        else {
            if let detailView: DevgImageDetailViewController = DevgImagePicker.shared.initDetailPicker {
                let asset: PHAsset = self.photoList[indexPath.item - 1]
                detailView.asset = asset
                detailView.assetCollection = self.photoCollection
                detailView.topViewImageView.image = self.topImageView.image
                detailView.selectImageBlock = self.selectImageBlock
                detailView.errorImageBlock = self.errorImageBlock
                detailView.textColor = self.textColors
                detailView.oldStatusBarStyle = self.oldStatusBarStyle
                detailView.newStatusBarStyle = self.newStatusBarStyle
                detailView.isPreview = false
                self.navigationController?.pushViewController(detailView, animated: true)
            }
        }
    }
    
    
    // MARK: - UITableViewDelegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.albumList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows: Int = 0
        if section == 0 {
            numberOfRows = 1
        }
        else  {
            let fetchResult: PHFetchResult = self.albumList[section]
            numberOfRows = fetchResult.count
        }
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DevgImagePickerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TableCell") as! DevgImagePickerTableViewCell
        
        let fetchResult: PHFetchResult = self.albumList[indexPath.section]
        if indexPath.section == 0 {
            cell.titleLabel.text = "모든사진"
            cell.countLabel.text = String(fetchResult.count)
        }
        else {
            let assetCollection: PHAssetCollection = fetchResult[indexPath.row]
            cell.countLabel.text = String(PHAsset.fetchAssets(in: assetCollection, options: nil).count)
            
            let collection: PHCollection = fetchResult[indexPath.row]
            cell.titleLabel.text = collection.localizedTitle!
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.photoList = self.albumList[indexPath.section] as? PHFetchResult<PHAsset>
        if indexPath.section == 0 {
            self.topButton.setTitle("모든사진 ▼", for: .normal)
            self.topButton.setTitle("모든사진 ▲", for: .selected)
        }
        else {
            self.photoCollection = self.albumList[indexPath.section][indexPath.row]
            self.photoList = PHAsset.fetchAssets(in: self.albumList[indexPath.section][indexPath.row], options: nil)
            self.topButton.setTitle("\(self.albumList[indexPath.section][indexPath.row].localizedTitle!) ▼", for: .normal)
            self.topButton.setTitle("\(self.albumList[indexPath.section][indexPath.row].localizedTitle!) ▲", for: .selected)
        }
        self.collectionView.reloadData()
        self.topButtonPush(self.topButton)
    }
    
    class DevgImagePickerTableViewCell: UITableViewCell {
        var imageViews: UIImageView!
        var titleLabel: UILabel!
        var countLabel: UILabel!
        
        override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            self.contentView.backgroundColor = UIColor.white
            self.imageViews = UIImageView(frame: CGRect(x: 5, y: 5, width: 50, height: 50))
            self.imageViews.contentMode = .scaleAspectFill
            self.imageViews.clipsToBounds = true
            self.contentView.addSubview(self.imageViews)
            
            self.titleLabel = UILabel(frame: CGRect(x: 15, y: 5, width: 250, height: 25))
            self.titleLabel.font = UIFont.systemFont(ofSize: 14)
            self.contentView.addSubview(self.titleLabel)
            
            self.countLabel = UILabel(frame: CGRect(x: 15, y: 30, width: 250, height: 25))
            self.countLabel.font = UIFont.systemFont(ofSize: 13)
            self.countLabel.textColor = UIColor.lightGray
            self.contentView.addSubview(self.countLabel)
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
    
    // MARK: - PHPhotoLibraryChangeObserver
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        guard let assetsFetchResults: PHFetchResult = self.photoList else {
            return
        }
        
        
        guard let collectionChanges: PHFetchResultChangeDetails = changeInstance.changeDetails(for: assetsFetchResults) else {
            return
        }
        
        DispatchQueue.main.async {
            self.photoList = collectionChanges.fetchResultAfterChanges
            
            if collectionChanges.hasIncrementalChanges == false  || collectionChanges.hasMoves == true {
                self.collectionView.reloadData()
            }
            else {
                self.collectionView.performBatchUpdates({ () -> Void in
                    if let removedIndexes: IndexSet = collectionChanges.removedIndexes {
                        self.collectionView.deleteItems(at: removedIndexes.indexPathsForSection(0))
                    }
                    
                    if let insertedIndexes: IndexSet = collectionChanges.insertedIndexes {
                        self.collectionView.insertItems(at: insertedIndexes.indexPathsForSection(0))
                    }
                    
                    if let changedIndexes: IndexSet = collectionChanges.changedIndexes {
                        self.collectionView.reloadItems(at: changedIndexes.indexPathsForSection(0))
                    }
                    }, completion:nil)
            }
            
            
            var updateAlbumList: [PHFetchResult] = self.albumList
            var isReload: Bool = false
            
            for (index, collectionFetchResult) in self.albumList.enumerated() {
                if let changeDetails: PHFetchResultChangeDetails = changeInstance.changeDetails(for: collectionFetchResult) {
                    updateAlbumList[index] = changeDetails.fetchResultAfterChanges
                    isReload = true
                }
                
            }
            if isReload == true {
                self.albumList = updateAlbumList
                self.tableView.reloadData()
            }
        }
    }
    
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            self.imagePicker.dismiss(animated: true) { () -> Void in
                PHPhotoLibrary.shared().performChanges({ () -> Void in
                    PHAssetChangeRequest.creationRequestForAsset(from: image)
                }, completionHandler: { (success, rrror) -> Void in
                    self.dismiss(animated: true, completion: { () -> Void in
                        if let imageData: Data = image.jpegData(compressionQuality: 1.0) {
                            self.selectImageBlock(image, imageData)
                        }
                        else {
                            self.errorImageBlock(904, "이미지 불러오기 실패")
                        }
                    })
                })
            }
        }
    }
    
}


class DevgImageDetailViewController: UIViewController {
    @IBOutlet weak var topLineView: UIView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var topViewImageView: UIImageView!
    @IBOutlet weak var topViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var navigationViewHeightConstraint: NSLayoutConstraint!
    var isPreview: Bool = false
    var isStatusBar: Bool = true {
        didSet {
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    var asset: PHAsset!
    var textColor: UIColor!
    var selectImage: UIImage? = nil
    var errorImageBlock: ImageErrorBlock!
    var assetCollection: PHAssetCollection!
    var selectImageBlock: ImageSelectBlock!
    var oldStatusBarStyle: UIStatusBarStyle!
    var newStatusBarStyle: UIStatusBarStyle!
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(DevgImageDetailViewController.imageTapped(image:)))
        self.detailImageView.isUserInteractionEnabled = true
        self.detailImageView.addGestureRecognizer(tapGestureRecognizer)
        self.navigationController?.hidesBarsOnTap = true
        
        if self.topViewImageView.image == nil {
            self.topLineView.isHidden = false
        }
        else {
            self.topLineView.isHidden = true
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.setStatusBarStyle(self.newStatusBarStyle, animated: true)
        self.closeButton.setTitleColor(self.textColor, for: .normal)
        self.sendButton.setTitleColor(self.textColor, for: .normal)
        
        let options: PHImageRequestOptions = PHImageRequestOptions()
        options.deliveryMode = .highQualityFormat
        options.isNetworkAccessAllowed = true
        options.progressHandler = { (progress, error, stop, info) -> Void in
            DispatchQueue.main.async {
                self.progressView.progress = Float(progress)
            }
        }
        PHImageManager.default().requestImage(for: self.asset, targetSize: self.targetSize(), contentMode: .aspectFit, options: options) { (resultImage, info) -> Void in
            self.progressView.isHidden = true
            guard let image: UIImage = resultImage else {
                return
            }
            self.selectImage = image
            self.detailImageView.image = image
        }
        if self.isPreview == false {
            self.navigationViewHeightConstraint.constant = 64
            self.view.layoutIfNeeded()
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.setStatusBarStyle(self.oldStatusBarStyle, animated: true)
        self.navigationViewHeightConstraint.constant = 0
        self.view.layoutIfNeeded()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return DevgImagePicker.shared.statusBarStyle
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    override var prefersStatusBarHidden: Bool {
        return !self.isStatusBar
    }
    
    @available(iOS 9.0, *)
    override var previewActionItems: [UIPreviewActionItem] {
        let likeAction = UIPreviewAction(title: "선택", style: .default) { (action, viewController) -> Void in
            if let image = self.selectImage {
                if let imageData: Data = image.jpegData(compressionQuality: 1.0) {
                    DevgImagePicker.shared.initPicker.dismiss(animated: true, completion: {
                        self.selectImageBlock(image, imageData)
                        self.selectImage = nil
                    })
                }
                else {
                    self.errorImageBlock(904, "이미지 불러오기 실패")
                }
            }
            else {
                self.errorImageBlock(904, "이미지 불러오기 실패")
            }
        }
        
        let deleteAction = UIPreviewAction(title: "취소", style: .default) { (action, viewController) -> Void in
            
        }
        return [likeAction, deleteAction]
    }
    
    // MARK: - Actions
    @IBAction func backButtonPush(_ sender: Any) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectButtonPush(_ sender: Any) {
        self.dismiss(animated: true) { () -> Void in
            if let image = self.selectImage {
                if let imageData: Data = image.jpegData(compressionQuality: 1.0) {
                    self.selectImageBlock(image, imageData)
                    self.selectImage = nil
                }
                else {
                    self.errorImageBlock(904, "이미지 불러오기 실패")
                }
            }
            else {
                self.errorImageBlock(904, "이미지 불러오기 실패")
            }
        }
    }
    
    // MARK: - Function
    @objc func imageTapped(image: Any)
    {
        if self.topViewTopConstraint.constant == 0 {
            self.topViewTopConstraint.constant = -64
            
            UIApplication.shared.setStatusBarHidden(true, with: .slide)
            self.isStatusBar = false
            
        }
        else {
            self.topViewTopConstraint.constant = 0
            
            UIApplication.shared.setStatusBarHidden(false, with: .slide)
            self.isStatusBar = true
        }
        
        UIView.animate(withDuration: 0.2) { () -> Void in
            self.view.layoutIfNeeded()
        }
    }
    
    func targetSize() -> CGSize {
        let scale: CGFloat =  UIScreen.main.scale
        let targetSize: CGSize = CGSize(width: self.detailImageView.bounds.size.width * scale, height: self.detailImageView.bounds.size.height * scale)
        return targetSize
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVMediaType(_ input: AVMediaType) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
