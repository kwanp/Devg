//
//  DevgExtensionArray.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import Foundation

public extension Array {
    func get(_ index: Int) -> Element? {
        if 0 <= index && index < count {
            return self[index]
        } else {
            return nil
        }
    }
    
    public mutating func removeObject<U: Equatable>(_ object: U) {
        var index: Int?
        for (idx, objectToCompare) in self.enumerated() {
            if let to = objectToCompare as? U {
                if object == to {
                    index = idx
                }
            }
        }
        
        if(index != nil) {
            self.remove(at: index!)
        }
    }
    
    public func randomIndex() -> Int {
        return Int(arc4random_uniform(UInt32(self.count)))
    }
    
    public func randomIndexes(_ count: Int) -> [Int] {
        var selfArrayCount: [Int] = Array<Int>()
        var indexArray: [Int] = Array<Int>()
        for index in 0...self.count-1 {
            selfArrayCount.append(index)
        }
        var recount: Int = self.count - count
        if recount < 0 {
            recount = 0
        }
        while selfArrayCount.count > recount {
            let arrayKey = Int(arc4random_uniform(UInt32(selfArrayCount.count)))
            let randNum = selfArrayCount[arrayKey]
            indexArray.append(randNum)
            selfArrayCount.remove(at: arrayKey)
        }
        return indexArray
    }
}

public extension MutableCollection where Index == Int {
    public mutating func shuffle() {
        if count < 2 { return }
        
        for i in startIndex ..< endIndex - 1 {
            let j = Int(arc4random_uniform(UInt32(endIndex - i))) + i
            if i != j {
                self.swapAt(i, j)
            }
        }
    }
}
