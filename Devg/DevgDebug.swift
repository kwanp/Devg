//
//  DevgDebug.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import Foundation

public func l(_ log: Any!, prefix: String = "LOG", file: String = #file, line: Int = #line, function: String = #function) {
    if Devg.isNotFromAppStore() {
        let string: String = debugString(log, prefix: prefix, file: file, line: line, function: function)
        var existContents: String = ""
        if Devg.existFile(name: "log.txt", type: .cache) {
            if let string: String = Devg.getContents(name: "log.txt", type: .cache) {
                existContents = string
            }
        }
        let _: Bool = Devg.createFile(name: "log.txt", contents: existContents + "# " + timestamp() + "\n" + string, type: .cache)
        print(string)
    }
}

public func d(_ log: Any!, prefix: String = "DEBUG", file: String = #file, line: Int = #line, function: String = #function) {
    if Devg.isNotFromAppStore() {
        let string: String = debugString(log, prefix: prefix, file: file, line: line, function: function)
        print(string)
    }
}

public func e(_ log: Any!, prefix: String = "ERROR", file: String = #file, line: Int = #line, function: String = #function) {
    if Devg.isNotFromAppStore() {
        let string: String = debugString(log, prefix: prefix, file: file, line: line, function: function)
        var existContents: String = ""
        if Devg.existFile(name: "err.txt", type: .cache) {
            if let string: String = Devg.getContents(name: "err.txt", type: .cache) {
                existContents = string
            }
        }
        let _: Bool = Devg.createFile(name: "err.txt", contents: existContents + "# " + timestamp() + "\n" + string, type: .cache)
        print(string)
    }
}

func debugString(_ log: Any!, prefix: String = "LOG", file: String = #file, line: Int = #line, function: String = #function) -> String {
    let lastFile: String = (file as NSString).lastPathComponent
    return "\n[\(prefix):] \(function) in \(lastFile)(\(line))\n" + "\(String(describing: log))".unwrap() + "\n[:\(prefix)] \(function) in \(lastFile)(\(line))\n"
}

func timestamp() -> String {
    let timestamp: String = DateFormatter.localizedString(from: Date(), dateStyle: .short, timeStyle: .short)
    return timestamp
}

var mobileProvision: NSDictionary!
extension Devg {
    public class func isNotFromAppStore() -> Bool {
        if TARGET_OS_SIMULATOR != 0 || TARGET_IPHONE_SIMULATOR != 0 {
            return true
        }
        else if (mobileProvision == nil) {
            let provisioningPath: String! = Bundle.main.path(forResource: "embedded", ofType: "mobileprovision")
            if (provisioningPath == nil) {
                return false
            }
            
            let binaryString: String
            do {
                let string: String = try String(contentsOfFile: provisioningPath, encoding: String.Encoding.isoLatin1)
                binaryString = string
            }
            catch {
                return false
            }
            
            let scanner: Scanner = Scanner(string: binaryString)
            var ok: Bool = scanner.scanUpTo("<plist", into: nil)
            if !ok {
                return false
            }
            var plistString: NSString?
            ok = scanner.scanUpTo("</plist>", into: &plistString)
            if !ok {
                return false
            }
            plistString = NSString(format: "%@</plist>", plistString!)
            
            if let plistdata_latin1: Data  = plistString?.data(using: String.Encoding.isoLatin1.rawValue) as Data? {
                do {
                    mobileProvision = try PropertyListSerialization.propertyList(from: plistdata_latin1 as Data, options: PropertyListSerialization.ReadOptions.init(rawValue: 0), format: nil) as? NSDictionary
                }
                catch {
                    return false
                }
                
            }
            else {
                return false
            }
        }
        
        if mobileProvision != nil {
            if let _ = mobileProvision["ProvisionedDevices"] {
                return true
            }
        }
        
        return false
    }
}
