//
//  DevgExtensionUIImageView.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import Foundation
import UIKit

class DevgImageLoaderManager: NSObject {
    static var shared: DevgImageLoaderManager = DevgImageLoaderManager()
    var imageLoaders: [UIImageView: DevgImageLoader] = Dictionary<UIImageView, DevgImageLoader>()
    
    func getImage(fromUrl urlString: String, imageView: UIImageView, defaultImage: UIImage?, completion: ((_ image: UIImage, _ isCache: Bool) -> Void)?, error: ((_ error: NSError?) -> Void)? = nil)  {
        if let imageLoader: DevgImageLoader = self.imageLoaders[imageView] {
            imageLoader.connection?.cancel()
            self.imageLoaders[imageView] = nil
        }
        
        let imageLoader: DevgImageLoader = DevgImageLoader()
        imageLoader.getImageFromUrl(urlString, imageView: imageView, defaultImage: defaultImage, completion: completion, error: error)
        
        self.imageLoaders[imageView] = imageLoader
    }
}

public class DevgImageLoader: NSObject, NSURLConnectionDelegate, NSURLConnectionDataDelegate {
    static let prefix: String = "DevgImageCache"
    
    var urlString = ""
    var imageData: NSMutableData = NSMutableData()
    var imageView: UIImageView!
    var completion: ((_ image: UIImage, _ isCache: Bool) -> Void)?
    var error: ((_ error: NSError?) -> Void)?
    var connection: NSURLConnection?
    
    func getImageFromUrl(_ urlString: String, imageView: UIImageView, defaultImage: UIImage?, completion: ((_ image: UIImage, _ isCache: Bool) -> Void)?, error: ((_ error: NSError?) -> Void)? = nil)  {
        self.imageView = imageView
        
        if self.urlString != urlString {
            self.urlString = urlString
            self.completion = completion
            self.error = error
            
            if let string: String = urlString.toURLEncode(.full) {
                let fileName = string.toBase64()
                let imagePath: String = DevgImageLoader.savePath() + "/" + fileName
                if FileManager.default.fileExists(atPath: imagePath) {
                    self.imageView.image = UIImage(contentsOfFile: imagePath)
                    if self.imageView.image != nil && self.completion != nil {
                        self.completion!(self.imageView.image!, true)
                        DevgImageLoaderManager.shared.imageLoaders[self.imageView] = nil
                    }
                }
                else {
                    self.imageData = NSMutableData()
                    if let url = URL(string: urlString) {
                        let request: NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
                        request.addValue("image/*", forHTTPHeaderField: "Accept")
                        self.connection = NSURLConnection(request: request as URLRequest, delegate: self)!
                        self.connection?.start()
                    }
                    else {
                        let error = NSError(domain: Devg.bundelID(), code: 404, userInfo: ["URL": urlString])
                        self.error?(error)
                        DevgImageLoaderManager.shared.imageLoaders[self.imageView] = nil
                    }
                }
            }
            else {
                let error = NSError(domain: Devg.bundelID(), code: 404, userInfo: ["URL": urlString])
                self.error?(error)
                DevgImageLoaderManager.shared.imageLoaders[self.imageView] = nil
            }
        }
    }
    
    public func connection(_ connection: NSURLConnection, didReceive response: URLResponse) {
    }
    
    public func connection(_ connection: NSURLConnection, didFailWithError error: Error) {
        self.error?(error as NSError)
        DevgImageLoaderManager.shared.imageLoaders[self.imageView] = nil
    }
    
    public func connection(_ connection: NSURLConnection, didReceive data: Data) {
        self.imageData.append(data as Data)
    }
    
    public func connectionDidFinishLoading(_ connection: NSURLConnection) {
        if let image = UIImage(data: self.imageData as Data) {
            if let string: String = urlString.toURLEncode(.full) {
                let fileName: String = string.toBase64()
                let imagePath: String = DevgImageLoader.savePath() + "/" + fileName
                
                do {
                    try FileManager.default.createDirectory(atPath: DevgImageLoader.savePath(), withIntermediateDirectories: true, attributes: nil)
                }
                catch {
                    e(error)
                }
                
                let bool: Bool = self.imageData.write(toFile: imagePath, atomically: true)
                if !bool {
                    e(imagePath, prefix: "ERROR - Image cache")
                }
                
                self.imageView.image = image
                if self.imageView.image != nil && self.completion != nil {
                    self.completion!(self.imageView.image!, false)
                    DevgImageLoaderManager.shared.imageLoaders[self.imageView] = nil
                }
                else if self.error != nil && self.imageView.image == nil {
                    self.error!(nil)
                    DevgImageLoaderManager.shared.imageLoaders[self.imageView] = nil
                }
            }
            else {
                self.error!(nil)
                DevgImageLoaderManager.shared.imageLoaders[self.imageView] = nil
            }
        }
        else if self.error != nil {
            self.error!(nil)
            DevgImageLoaderManager.shared.imageLoaders[self.imageView] = nil
        }
    }
    
    public class func savePath() -> String {
        let savePath: NSString = NSSearchPathForDirectoriesInDomains(.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] as NSString
        return savePath.appendingPathComponent(DevgImageLoader.prefix)
    }
}

public extension UIImageView {
    public func setImage(fromUrl urlString: String, defaultImage: UIImage? = nil, completion: ((_ image: UIImage, _ isCache: Bool) -> Void)? = nil, error: ((_ error: NSError?) -> Void)? = nil) {
        if let image: UIImage = defaultImage {
            self.image = image
        }
        DevgImageLoaderManager.shared.getImage(fromUrl: urlString, imageView: self, defaultImage: defaultImage, completion: completion, error: error)
    }
    
    public class func cachedSize() -> UInt {
        var fileSize: UInt = 0
        
        do {
            let files: [String] = try FileManager.default.subpathsOfDirectory(atPath: DevgImageLoader.savePath() as String)
            
            for fileName in files {
                let filePath = (DevgImageLoader.savePath() as NSString).appendingPathComponent(fileName)
                let file: Dictionary = try FileManager.default.attributesOfItem(atPath: filePath)
                fileSize += file[FileAttributeKey.size] as! UInt
            }
        }
        catch {
            e(error)
        }
        
        return fileSize
    }
    
    public class func emptyCached() {
        do {
            let files: [String] = try FileManager.default.subpathsOfDirectory(atPath: DevgImageLoader.savePath() as String)
            
            for fileName in files {
                let filePath: String = (DevgImageLoader.savePath() as NSString).appendingPathComponent(fileName)
                try FileManager.default.removeItem(atPath: filePath)
            }
        }
        catch {
            e(error)
        }
    }
}
