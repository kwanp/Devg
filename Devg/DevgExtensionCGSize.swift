//
//  DevgExtensionCGSize.swift
//  Devg
//
//  Created by Kwan on 19/05/2017.
//  Copyright © 2017 Kwan. All rights reserved.
//

import Foundation
import UIKit

public extension CGSize {
    public func resizeHeight(width: CGFloat) -> CGFloat {
        let ratio: CGFloat = width / self.width
        return self.height * ratio
    }
}
