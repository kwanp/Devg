//
//  DevgExtensionString.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import Foundation

public extension String {
    public static func removeNil(_ string: String?) -> String {
        guard let string: String = string else {
            return ""
        }
        
        return string.unwrap()
    }
    
    public enum PhoneNumberType {
        case koreanCell
        case koreanPhone
    }
    
    public enum PhoneNumberSecretType {
        case none
        case half
        case all
    }
    
    public enum NumberFormatType {
        case thousandSeparator
        case dollar
    }
    
    public enum PasswordType {
        case normal
        case koreanGovernment
        case koreanGovernmentLow
        case alphabetAndNumber
    }
    
    public enum URLEncodeType {
        case url
        case normal
        case full
    }
    
    public func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    public func unwrap() -> String {
        if self.hasPrefix("Optional(") && self.hasSuffix(")") {
            return self.substring(start: 9, length: UInt(self.count - 10))
        }
        else {
            return self
        }
    }
    
    public func toJSON() -> Any? {
        var json: Any? = nil
        if let data = self.data(using: String.Encoding.utf8) {
            do {
                try json = JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            }
            catch let error {
                e(error)
            }
        }
        
        return json
    }
    
    public func toBool() -> Bool {
        var bool: Bool = false
        let trueStrings: [String] = ["true", "yes", "1", "y"]
        if trueStrings.contains(self.lowercased()) {
            bool = true
        }
        
        return bool
    }
    
    public func toInt() -> Int {
        var response: Int = 0
        if let int: Int = Int(self) {
            response = int
        }
        
        return response
    }
    
    public func toFloat() -> Float {
        var response: Float = 0.0
        let array: [String] = self.components(separatedBy: ".")
        if array.count > 1 {
            let before = Int(array[0])
            if before == nil {
                return response
            }
            else {
                response = Float(before!)
                let after = Int(array[1])
                if after == nil {
                    return response
                }
                else {
                    let length = array[1].count
                    var point: Float = Float(after!)
                    for _ in 0..<length {
                        point = point * 0.1
                    }
                    response = response + point
                }
            }
        }
        else {
            let all = Int(self)
            if all == nil {
                return response
            }
            else {
                response = Float(all!)
            }
        }
        
        return response
    }
    
    public func substring(start: Int) -> String {
        if start < 0 {
            if abs(start) > self.count {
                return self
            }
        }
        else {
            if start >= self.count {
                return ""
            }
        }
        
        var startIndex: String.Index
        
        if start < 0 {
            startIndex = self.endIndex
        }
        else {
            startIndex = self.startIndex
        }
        
        return String(self[self.index(startIndex, offsetBy: start)...])
    }
    
    public func substring(start: Int, length: UInt) -> String {
        let stringForStart: String = self.substring(start: start)
        
        if Int(length) > stringForStart.count {
            return stringForStart
        }
        
        return String(stringForStart[..<stringForStart.index(stringForStart.startIndex, offsetBy: Int(length))])
    }
    
    public func toPhoneNumberFormat(_ type: PhoneNumberType, secretType: PhoneNumberSecretType = .none) -> String {
        var phoneNumber: String = ""
        var formatPhoneNumber = ""
        
        for char in [Character](self) {
            let charToString = String(char)
            if Int(charToString) != nil {
                phoneNumber += charToString
            }
        }
        
        let phoneNumberCount: Int = phoneNumber.count
        formatPhoneNumber = phoneNumber
        
        switch type {
        case .koreanCell:
            var separate1 = ""
            var separate2: String
            var separate3: String
            if phoneNumberCount > 3 {
                separate1 = phoneNumber.substring(start: 0, length: 3)
                separate2 = phoneNumber.substring(start: 3, length: UInt(phoneNumberCount - 3))
                
                formatPhoneNumber = "\(separate1)-\(separate2)"
            }
            
            if phoneNumberCount > 3 + 4 {
                separate2 = phoneNumber.substring(start: 3, length: 4)
                separate3 = phoneNumber.substring(start: 3 + 4, length: UInt(phoneNumberCount - (3 + 4)))
                
                formatPhoneNumber = "\(separate1)-\(separate2)-\(separate3)"
            }
            
            if phoneNumberCount == 3 + 3 + 4 {
                separate2 = phoneNumber.substring(start: 3, length: 3)
                separate3 = phoneNumber.substring(start: 3 + 3, length: 4)
                
                formatPhoneNumber = "\(separate1)-\(separate2)-\(separate3)"
            }
            break
        case .koreanPhone:
            var separate1Length = 3
            if phoneNumber.hasPrefix("02") {
                separate1Length = 2
            }
            
            var separate1 = ""
            var separate2: String
            var separate3: String
            if phoneNumberCount > separate1Length {
                separate1 = phoneNumber.substring(start: 0, length: UInt(separate1Length))
                separate2 = phoneNumber.substring(start: separate1Length, length: UInt(phoneNumberCount - separate1Length))
                
                formatPhoneNumber = "\(separate1)-\(separate2)"
            }
            
            if phoneNumberCount > separate1Length + 4 {
                separate2 = phoneNumber.substring(start: separate1Length, length: 4)
                separate3 = phoneNumber.substring(start: separate1Length + 4, length: UInt(phoneNumberCount - (separate1Length + 4)))
                
                formatPhoneNumber = "\(separate1)-\(separate2)-\(separate3)"
            }
            
            if phoneNumberCount == separate1Length + 3 + 4 {
                separate2 = phoneNumber.substring(start: separate1Length, length: 3)
                separate3 = phoneNumber.substring(start: separate1Length + 3, length: 4)
                
                formatPhoneNumber = "\(separate1)-\(separate2)-\(separate3)"
            }
            break
        }
        
        if secretType != .none {
            let numberSeparates: [String] = formatPhoneNumber.components(separatedBy: "-")
            if numberSeparates.count > 1 {
                var newSeparate2: String
                if secretType == .half {
                    newSeparate2 = numberSeparates[1].substring(start: 0, length: 2)
                    let separateCount: Int = numberSeparates[1].count - 2
                    for _ in 0..<separateCount {
                        newSeparate2 = newSeparate2 + "*"
                    }
                }
                else {
                    newSeparate2 = ""
                    let separateCount: Int = numberSeparates[1].count
                    for _ in 0..<separateCount {
                        newSeparate2 = newSeparate2 + "*"
                    }
                }
                
                formatPhoneNumber = numberSeparates[0] + "-" + newSeparate2
                if numberSeparates.count > 2 {
                    formatPhoneNumber = formatPhoneNumber + "-" + numberSeparates[2]
                }
            }
        }
        
        return formatPhoneNumber
    }
    
    public func toNumberFormat(_ type: NumberFormatType = .thousandSeparator) -> String {
        let formatter = NumberFormatter()
        
        switch type {
        case .dollar:
            formatter.numberStyle = NumberFormatter.Style.currency
            formatter.currencyCode = "USD"
            formatter.currencySymbol = ""
        default:
            formatter.numberStyle = NumberFormatter.Style.decimal
        }
        
        var numberString: String?
        if let toDouble = Double(self) {
            if let formatString = formatter.string(from: NSNumber(value: toDouble)) {
                numberString = formatString
            }
        }
        
        guard let returnString: String = numberString else {
            return ""
        }
        
        return returnString
    }
    
    public func toBase64() -> String {
        let plainData = (self as NSString).data(using: String.Encoding.utf8.rawValue)
        let base64Data = plainData?.base64EncodedData(options: [])
        
        var returnString = ""
        if let data = base64Data {
            returnString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
        }
        
        return returnString
    }
    
    public func fromBase64() -> String {
        let decodedData: Data? = Data(base64Encoded: self, options: [])
        
        var returnString: String = ""
        if let data: Data = decodedData {
            returnString = NSString(data: data as Data, encoding: String.Encoding.utf8.rawValue)! as String
        }
        
        return returnString
    }
    
    public func isSizeMinimum(_ minimum: Int, maximum: Int? = nil) -> Bool {
        var isAllow: Bool = true
        
        if self.count < minimum {
            isAllow = false
        }
        
        if maximum != nil {
            if self.count > maximum! {
                isAllow = false
            }
        }
        
        return isAllow
    }
    
    public func isEmail() -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil
        }
        catch {
            return false
        }
    }
    
    public func isPhoneNumber(_ type: PhoneNumberType) -> Bool {
        return self.isRegex("^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?[0-9]{3,4}-?[0-9]{4}$")
    }
    
    public func isSignId() -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[A-Z]{1}[A-Z0-9]", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil
        }
        catch {
            return false
        }
    }
    
    public func isRegex(_ pattern: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: [])
            return regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil
        }
        catch {
            return false
        }
    }
    
    public func isPassword(_ type: PasswordType = .normal) -> Bool {
        var isAllow = false
        isAllow = self.isRegex("(?=.*[a-zA-Z]|.*[!@#$%^&*?+=_-~]|.*[0-9]).{1,100}$")
        
        if type == .alphabetAndNumber {
            let isAlphabet: Bool = self.isRegex("[a-z]") || self.isRegex("[A-Z]")
            let isNumber: Bool = self.isRegex("[0-9]")
            if isAlphabet && isNumber {
                return true
            }
            else {
                return false
            }
        }
        
        if (type == .koreanGovernment || type == .koreanGovernmentLow) && isAllow {
            var checkCount: Int = 0
            if type == .koreanGovernment {
                checkCount = 3
            }
            else if type == .koreanGovernmentLow {
                checkCount = 2
            }
            var isCount = 0
            
            if self.isRegex("[a-z]") {
                isCount += 1
            }
            
            if self.isRegex("[A-Z]") {
                isCount += 1
            }
            
            if self.isRegex("[0-9]") {
                isCount += 1
            }
            
            if self.isRegex("[!,@,#,$,%,^,&,*,?,+,=,_,\\-,~]") {
                isCount += 1
            }
            
            if isCount >= checkCount {
                isAllow = true
            }
            else {
                isAllow = false
            }
        }
        
        return isAllow
    }
    
    /// URL Encode.
    ///
    /// :returns: Encoded string for URL
    public func toURLEncode(_ type: URLEncodeType = .normal) -> String? {
        var characterSet: CharacterSet
        switch type {
        case .url:
            characterSet = CharacterSet.urlPathAllowed
            break
        case .normal:
            characterSet = CharacterSet.urlHostAllowed
            break
        case .full:
            characterSet = CharacterSet.urlUserAllowed
            break
        }
        if let encoded: String = self.addingPercentEncoding(withAllowedCharacters: characterSet) {
            return encoded
        }
        else {
            return nil
        }
    }
    
    /// URL Decode.
    ///
    /// :returns: Normal string.
    public func toURLDecode() -> String? {
        if let normal = self.removingPercentEncoding {
            return normal
        }
        else {
            return nil
        }
    }
    
    /// toDate.
    ///
    /// :param: 포멧 String
    /// :returns: Normal string.
    public func toDate(_ format: String, timeZone: Date.TimeZoneType = Date.TimeZoneType.local) -> Date? {
        let dateFormatter: DateFormatter = DateFormatter()
        switch timeZone {
        case .korea:
            dateFormatter.timeZone = TimeZone.init(identifier: "Asia/Seoul")!
            break
        case .system:
            dateFormatter.timeZone = TimeZone.ReferenceType.system
            break
        case .local:
            dateFormatter.timeZone = TimeZone.current
            break
        }
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)
    }
    
    /// localized.
    ///
    /// 반드시 "Localizable"로 파일명을 정해야 한다.
    /// :returns: 나라설정에 맞는 언어로 번역하여 제공
    public func localized() -> String {
        var language: String = "en"
        if let currentLanguage = UserDefaults.standard.object(forKey: "LCLCurrentLanguageKey") as? String {
            language = currentLanguage
        }
        
        guard let preferredLanguage = Bundle.main.preferredLocalizations.first else {
            return self
        }
        
        let availableLanguages: [String] = Bundle.main.localizations
        if (availableLanguages.contains(preferredLanguage)) {
            language = preferredLanguage
        }
        
        if let path = Bundle.main.path(forResource: language, ofType: "lproj"), let bundle = Bundle(path: path) {
            return bundle.localizedString(forKey: self, value: nil, table: nil)
        }
        else if let path = Bundle.main.path(forResource: "Base", ofType: "lproj"), let bundle = Bundle(path: path) {
            return bundle.localizedString(forKey: self, value: nil, table: nil)
        }
        return self
    }
    
    func nsrange(of subString: String) -> NSRange? {
        guard let subStringRange: Range = self.range(of: subString) else {
            return nil
        }
        
        let start: Int = self.distance(from: self.startIndex, to: subStringRange.lowerBound)
        return NSMakeRange(start, subString.count)
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)

        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)

        return ceil(boundingBox.width)
    }
}

extension NSAttributedString {
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)

        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)

        return ceil(boundingBox.width)
    }
}
