//
//  DevgExtensionUIButton.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit

public extension UIButton {
    func indexPathAt(tableView: UITableView) -> IndexPath? {
        let position: CGPoint = self.convert(CGPoint(x: 0, y: 0), to: tableView)
        
        return tableView.indexPathForRow(at: position)
    }
    
    func indexPathAt(collectionView: UICollectionView) -> IndexPath? {
        let position: CGPoint = self.convert(CGPoint(x: 0, y: 0), to: collectionView)
        
        return collectionView.indexPathForItem(at: position)
    }
    
    func enable() {
        self.isEnabled = true
    }
    
    func disable() {
        self.isEnabled = false
    }
    
    func select() {
        self.isSelected = true
    }
    
    func deselect() {
        self.isSelected = false
    }
}
