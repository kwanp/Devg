//
//  DevgExtensionUIFont.swift
//  Devg
//
//  Created by Kwan on 2019/10/31.
//  Copyright © 2019 Kwan. All rights reserved.
//

import Foundation

extension UIFont {
    open class func regularSystemFont(ofSize: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: ofSize)
    }
    
    open class func mediumSystemFont(ofSize: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: ofSize, weight: .medium)
    }
}
