import UIKit

//
//  DevgExtensionDate.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import Foundation

public extension Date {
    public enum TimeZoneType {
        case korea
        case system
        case local
    }
    
    /// Date Compare
    ///
    /// :param: 비교할 Date
    /// :returns: Bool
    public func compareDate(to: Date? = nil, from: Date? = nil) -> Bool {
        var compare: Bool = false
        if let toDate: Date = to {
            if self.compare(toDate) == .orderedDescending {
                compare = true
            }
        }
        
        if let fromDate: Date = from {
            if self.compare(fromDate) == .orderedAscending {
                compare = true
            }
        }
        
        return compare
    }
    
    /// gmt Date
    ///
    /// :returns: NSDate
    public func gmt() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .full
        dateFormatter.timeStyle = .full
        let timeZomeOffset: TimeInterval = TimeInterval(TimeZone.ReferenceType.system.secondsFromGMT(for: self))
        return dateFormatter.date(from: dateFormatter.string(from: self.addingTimeInterval(-timeZomeOffset)))!
    }
    
    /// Date to String
    ///
    /// :param: 포멧 String
    /// :returns: Date String
    public func toString(_ format: String, timeZone: Date.TimeZoneType = Date.TimeZoneType.local) -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        switch timeZone {
        case .korea:
            dateFormatter.timeZone = TimeZone.init(identifier: "Asia/Seoul")!
            break
        case .system:
            dateFormatter.timeZone = TimeZone.ReferenceType.system
            break
        case .local:
            dateFormatter.timeZone = TimeZone.current
            break
        }
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    public var timestamp: Int {
        return Int(self.timeIntervalSince1970)
    }
    
    public func toSeconds(timeZone: Date.TimeZoneType = Date.TimeZoneType.local) -> Int {
        let format: String = "yyyyMMddHHmmss"
        let string: String = self.toString(format, timeZone: timeZone)
        if let date: Date = string.toDate(format, timeZone: timeZone) {
            return Int(date.timeIntervalSince1970)
        }
        else {
            return Int(self.timeIntervalSince1970)
        }
    }
}
