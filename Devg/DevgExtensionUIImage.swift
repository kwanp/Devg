//
//  DevgExtensionUIImage.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import Foundation
import UIKit

public extension UIImage {
    public func aspectFit(_ size: CGSize) -> UIImage? {
        var newImage: UIImage?
        let originalWidth = self.size.width
        let originalHeight = self.size.height
        var scaleFactor: CGFloat = 0.0
        var newSize: CGSize
        
        if size.width > 0 && size.height > 0 {
            if originalWidth > originalHeight {
                scaleFactor = size.width / originalWidth
            }
            else {
                scaleFactor = size.height / originalHeight
            }
        }
        else {
            if size.width > 0 {
                scaleFactor = size.width / originalWidth
            }
            else if size.height > 0 {
                scaleFactor = size.height / originalHeight
            }
            else {
                e("Resize and Crop Failed. \(size)")
                return nil
            }
        }
        
        newSize = CGSize(width: originalWidth * scaleFactor, height: originalHeight * scaleFactor)
        
        UIGraphicsBeginImageContext(newSize)
        
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        
        newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        if newImage == nil {
            e("Resize and Crop Failed. \(size)")
        }
        
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    public func aspectFill(_ size: CGSize) -> UIImage? {
        let sourceImage: UIImage = self
        var newImage: UIImage?
        let imageSize: CGSize = sourceImage.size
        let width: CGFloat = imageSize.width
        let height: CGFloat = imageSize.height
        let targetWidth: CGFloat = size.width
        let targetHeight = size.height
        var scaleFactor: CGFloat = 0.0
        var scaledWidth: CGFloat = targetWidth
        var scaledHeight: CGFloat = targetHeight
        var thumbnailPoint = CGPoint(x: 0.0, y: 0.0)
        
        if !imageSize.equalTo(size) {
            let widthFactor: CGFloat = targetWidth / width
            let heightFactor: CGFloat = targetHeight / height
            
            if widthFactor > heightFactor {
                scaleFactor = widthFactor
            }
            else {
                scaleFactor = heightFactor
            }
            
            scaledWidth = width * scaleFactor
            scaledHeight = height * scaleFactor
            
            if widthFactor > heightFactor {
                thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5
            }
            else {
                if widthFactor < heightFactor {
                    thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5
                }
            }
        }
        
        UIGraphicsBeginImageContext(size)
        
        var thumbnailRect: CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
        thumbnailRect.origin = thumbnailPoint
        thumbnailRect.size.width = scaledWidth
        thumbnailRect.size.height = scaledHeight
        
        sourceImage.draw(in: thumbnailRect)
        
        newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        if newImage == nil {
            e("Resize and Crop Failed. \(size)")
        }
        
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    public func widthFit(_ width: CGFloat) -> UIImage? {
        var newImage: UIImage?
        let ratio = width / self.size.width
        let height = self.size.height * ratio
        var newSize: CGSize
        newSize = CGSize(width: width, height: height)
        UIGraphicsBeginImageContext(newSize)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        newImage = UIGraphicsGetImageFromCurrentImageContext()
        if newImage == nil {
        }
        UIGraphicsEndImageContext()
        return newImage
    }
    
    public var averageColor: UIColor {
        guard let cgImage: CGImage = self.cgImage else {
            return UIColor.white
        }
        
        let rgba = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: 4)
        let colorSpace: CGColorSpace = CGColorSpaceCreateDeviceRGB()
        let info = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context: CGContext = CGContext(data: rgba, width: 1, height: 1, bitsPerComponent: 8, bytesPerRow: 4, space: colorSpace, bitmapInfo: info.rawValue)!
        
        context.draw(cgImage, in: CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0))
        
        if rgba[3] > 0 {
            let alpha: CGFloat = CGFloat(rgba[3]) / 255.0
            let multiplier: CGFloat = alpha / 255.0
            
            return UIColor(red: CGFloat(rgba[0]) * multiplier, green: CGFloat(rgba[1]) * multiplier, blue: CGFloat(rgba[2]) * multiplier, alpha: alpha)
        } else {
            return UIColor(red: CGFloat(rgba[0]) / 255.0, green: CGFloat(rgba[1]) / 255.0, blue: CGFloat(rgba[2]) / 255.0, alpha: CGFloat(rgba[3]) / 255.0)
        }
    }
    
    public func pickColor(_ pos: CGPoint) -> UIColor {
        let pixelData = self.cgImage?.dataProvider?.data
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)

        let pixelInfo: Int = ((Int(self.size.width) * Int(pos.y)) + Int(pos.x)) * 4

        let b = CGFloat(data[pixelInfo])     / CGFloat(255.0)
        let g = CGFloat(data[pixelInfo + 1]) / CGFloat(255.0)
        let r = CGFloat(data[pixelInfo + 2]) / CGFloat(255.0)
        let a = CGFloat(data[pixelInfo + 3]) / CGFloat(255.0)
        
        return UIColor(red: r, green: g, blue: b, alpha: a)
    }
}
