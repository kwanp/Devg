//
//  DevgAd.swift
//  Devg
//
//  Created by Kwan on 2017. 3. 6..
//  Copyright © 2017년 Kwan. All rights reserved.
//

import Foundation
import AdSupport
import Devg

public class DevgAd: NSObject {
    public class func uuid() -> String {
        var uuid: String?
        let keyName: String = "AdUUID"
        
        let SecMatchLimit: String = kSecMatchLimit as String
        let SecReturnData: String = kSecReturnData as String
        let SecValueData: String = kSecValueData as String
        let SecAttrAccessible: String = kSecAttrAccessible as String
        let SecClass: String = kSecClass as String
        let SecAttrService: String = kSecAttrService as String
        let SecAttrGeneric: String = kSecAttrGeneric as String
        let SecAttrAccount: String = kSecAttrAccount as String
        
        var readKeychainQuery: [String: AnyObject] = [SecClass:kSecClassGenericPassword]
        readKeychainQuery[SecAttrService] = Devg.bundelID() as AnyObject?
        let encodedIdentifier: Data? = keyName.data(using: .utf8)
        readKeychainQuery[SecAttrGeneric] = encodedIdentifier as AnyObject?
        readKeychainQuery[SecAttrAccount] = encodedIdentifier as AnyObject?
        
        var result: AnyObject?
        readKeychainQuery[SecMatchLimit] = kSecMatchLimitOne
        readKeychainQuery[SecReturnData] = kCFBooleanTrue
        let status = withUnsafeMutablePointer(to: &result) {
            SecItemCopyMatching(readKeychainQuery as CFDictionary, UnsafeMutablePointer($0))
        }
        let keychainData: Data? = status == noErr ? result as? Data : nil
        var stringValue: String?
        if let data: Data = keychainData {
            stringValue = String(data: data, encoding: .utf8)
        }
        
        uuid = stringValue
        
        if uuid == nil { // create
            let newUuid: String = ASIdentifierManager.shared().advertisingIdentifier.uuidString
            
            var saveKeychainQuery: [String: AnyObject] = [SecClass:kSecClassGenericPassword]
            saveKeychainQuery[SecAttrService] = Devg.bundelID() as AnyObject?
            let encodedIdentifier: Data? = keyName.data(using: .utf8)
            saveKeychainQuery[SecAttrGeneric] = encodedIdentifier as AnyObject?
            saveKeychainQuery[SecAttrAccount] = encodedIdentifier as AnyObject?
            
            saveKeychainQuery[SecValueData] = newUuid.data(using: .utf8) as AnyObject?
            saveKeychainQuery[SecAttrAccessible] = kSecAttrAccessibleWhenUnlocked
            
            uuid = newUuid
        }
        
        return uuid!
    }
}
