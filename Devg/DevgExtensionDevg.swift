//
//  DevgExtensionDevg.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit

public extension Devg {
    /// iOS 버전 정보.
    ///
    /// :returns: Float
    public class func osVersion() -> String {
        return UIDevice.current.systemVersion
    }
    
    public class func timezone() -> String {
        let secondsGMT: Int = TimeZone.current.secondsFromGMT()
        let plus: String = (secondsGMT < 0) ? "-" : "+"
        let minutes: Int = (secondsGMT / 60) % 60
        let hours: Int = (secondsGMT / 60) / 60
        let string: String = plus + String(format: "%02d", hours) + ":" + String(format: "%02d", minutes)
        return string
    }
    
    public class func languageCode() -> String {
        guard let language: String = Locale.preferredLanguages.get(0) else {
            return "en"
        }
        
        if let languageCode: String = Locale.components(fromIdentifier: language)["kCFLocaleLanguageCodeKey"] {
            return languageCode
        }
        else {
            return "en"
        }
    }
    
    public class func serverIsOnline(url: String, timeout: TimeInterval = 2, completion: @escaping (Bool) -> Void) -> Void {
        guard let url: URL = URL(string: url) else {
            completion(false)
            return
        }
        
        let session = URLSession.shared
        let request: URLRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: timeout)
        let dataTask: URLSessionDataTask = session.dataTask(with: request) { (data, response, error) in
            if let _: HTTPURLResponse = response as? HTTPURLResponse {
                completion(true)
            }
            else {
                completion(false)
            }
        }
        
        dataTask.resume()
    }
    
    /// 앱 스토어의 앱 버전 정보.
    ///
    /// :returns: Void
    public class func appVersionFromAppStore(_ completion: @escaping ((_ version: String) -> Void), error: @escaping ((_ error: NSError) -> Void)) {
        guard let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(Devg.bundelID())") else {
            error(NSError(domain: "", code: 404, userInfo: nil))
            return
        }
        let request = URLRequest(url: url as URL)
        
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main) {(response, data, err) in
            if err == nil {
                guard let object: Any = String(data: data!, encoding:String.Encoding.utf8)?.toJSON() else {
                    error(NSError(domain: "", code: 500, userInfo: nil))
                    return
                }
                
                guard let dict: NSDictionary = object as? NSDictionary else {
                    return
                }
                
                guard let array: [AnyObject] = dict.object(forKey: "results") as? [AnyObject] else {
                    error(NSError(domain: "", code: 500, userInfo: nil))
                    return
                }
                
                guard let dictionary: [String: AnyObject] = array.get(0) as? [String: AnyObject] else {
                    error(NSError(domain: "", code: 500, userInfo: nil))
                    return
                }
                
                guard let version: String = dictionary["version"] as? String else {
                    error(NSError(domain: "", code: 500, userInfo: nil))
                    return
                }
                
                completion(version)
            }
            else {
                error(err! as NSError)
            }
        }
    }
    
    /// UUID
    ///
    /// :returns: String
    public class func uuid() -> String {
        var uuid: String?
        let keyName: String = "UUID"
        
        let SecMatchLimit: String = kSecMatchLimit as String
        let SecReturnData: String = kSecReturnData as String
        let SecValueData: String = kSecValueData as String
        let SecAttrAccessible: String = kSecAttrAccessible as String
        let SecClass: String = kSecClass as String
        let SecAttrService: String = kSecAttrService as String
        let SecAttrGeneric: String = kSecAttrGeneric as String
        let SecAttrAccount: String = kSecAttrAccount as String
        
        var readKeychainQuery: [String: AnyObject] = [SecClass:kSecClassGenericPassword]
        readKeychainQuery[SecAttrService] = Devg.bundelID() as AnyObject?
        let encodedIdentifier: Data? = keyName.data(using: String.Encoding.utf8) as Data?
        readKeychainQuery[SecAttrGeneric] = encodedIdentifier as AnyObject?
        readKeychainQuery[SecAttrAccount] = encodedIdentifier as AnyObject?
        
        var result: AnyObject?
        readKeychainQuery[SecMatchLimit] = kSecMatchLimitOne
        readKeychainQuery[SecReturnData] = kCFBooleanTrue
        let status = withUnsafeMutablePointer(to: &result) {
            SecItemCopyMatching(readKeychainQuery as CFDictionary, UnsafeMutablePointer($0))
        }
        let keychainData: Data? = status == noErr ? result as? Data : nil
        var stringValue: String?
        if let data = keychainData {
            stringValue = NSString(data: data as Data, encoding: String.Encoding.utf8.rawValue) as String?
        }
        
        uuid = stringValue
        
        if uuid == nil { // create
            let newUuid: String = UIDevice.current.identifierForVendor!.uuidString
            
            var saveKeychainQuery: [String: AnyObject] = [SecClass:kSecClassGenericPassword]
            saveKeychainQuery[SecAttrService] = Devg.bundelID() as AnyObject?
            let encodedIdentifier: Data? = keyName.data(using: String.Encoding.utf8) as Data?
            saveKeychainQuery[SecAttrGeneric] = encodedIdentifier as AnyObject?
            saveKeychainQuery[SecAttrAccount] = encodedIdentifier as AnyObject?
            
            saveKeychainQuery[SecValueData] = newUuid.data(using: String.Encoding.utf8) as AnyObject?
            saveKeychainQuery[SecAttrAccessible] = kSecAttrAccessibleWhenUnlocked
            
            uuid = newUuid
        }
        
        return uuid!
    }
    
    public class func isAppActive() -> Bool {
        if UIApplication.shared.applicationState == .active {
            return true
        }
        
        return false
    }
    
    // TODO: - AppDelegate 해결
    public class func viewControllerFromStoryboard(_ name: String, identifier: String) -> UIViewController {
        let storyboard: UIStoryboard = UIStoryboard(name: name, bundle: nil)
        let viewController: UIViewController = storyboard.instantiateViewController(withIdentifier: identifier)
        
        return viewController
    }
    
    public class func visibleViewController() -> UIViewController {
        var visibleViewController: UIViewController = UIViewController()
        if let rootViewController: UIViewController = UIApplication.shared.delegate!.window!!.rootViewController {
            visibleViewController = self.getVisibleWithPresented(rootViewController)
        }
        return visibleViewController
    }
    
    fileprivate class func getVisibleWithPresented(_ presented: UIViewController) -> UIViewController {
        if presented.presentedViewController == nil {
            if presented is UINavigationController {
                let navigation: UINavigationController = presented as! UINavigationController
                let last: UIViewController = navigation.viewControllers.last!
                
                return self.getVisibleWithPresented(last)
            }
            else if presented is UITabBarController {
                let tabBar: UITabBarController = presented as! UITabBarController
                if let seleted: UIViewController = tabBar.selectedViewController {
                    return self.getVisibleWithPresented(seleted)
                }
                else {
                    return UIViewController()
                }
            }
            else {
                return presented
            }
        }
        else if presented.presentedViewController is UINavigationController {
            let navigation: UINavigationController = presented.presentedViewController as! UINavigationController
            let last: UIViewController = navigation.viewControllers.last!
            
            return self.getVisibleWithPresented(last)
        }
        else if presented.presentedViewController is UITabBarController {
            let tabBar: UITabBarController = presented.presentedViewController as! UITabBarController
            if let seleted: UIViewController = tabBar.selectedViewController {
                return self.getVisibleWithPresented(seleted)
            }
            else {
                return self.getVisibleWithPresented(UIViewController())
            }
        }
        else {
            return self.getVisibleWithPresented(presented.presentedViewController!)
        }
    }
    
    // MARK: - Web
    
    /// 사파리로 이동.
    public class func safari(_ url: String) -> Bool {
        var aUrl: String
        if !url.isEmpty {
            aUrl = url
        }
        else {
            return false
        }
        
        if !url.isRegex("://") && !url.isRegex("tel:") {
            if url.isPhoneNumber(.koreanPhone) {
                aUrl = "telprompt://" + url
            }
            else {
                aUrl = "http://" + url
            }
        }
        if let bUrl: URL = URL(string: aUrl) {
            return UIApplication.shared.openURL(bUrl as URL)
        }
        else {
            return false
        }
    }
}
