//
//  DevgKeyboard.swift
//  Devg
//
//  Created by Kwan on 2016. 3. 14..
//  Copyright © 2016년 Kwan. All rights reserved.
//
import Foundation
import UIKit

public protocol DevgKeyboardDelegate: AnyObject {
    func keyboardChangeStatus(_ status: DevgKeyboard.KeyboardStatus, notification: Notification, size: CGSize)
}

public class DevgKeyboard: NSObject {
    public enum KeyboardStatus: Int {
        case willShow
        case didShow
        case willHide
        case didHide
    }
    
    var isArrow: Bool = true
    
    var textItemArray: [AnyObject] = Array<AnyObject>()
    var toolbarArray: [UIToolbar] = Array<UIToolbar>()
    var targetView: UIView!
    var defaultColor: UIColor = UIColor(red: 98/255, green: 172/255, blue: 255/255, alpha: 1)
    var defaultNonColor: UIColor = UIColor(red: 128/255, green: 128/255, blue: 128/255, alpha: 1)
    weak var delegate: DevgKeyboardDelegate? = nil
    
    override init() {
        
    }
    
    public convenience init(target: DevgKeyboardDelegate, isArrow: Bool = true) {
        self.init()
        self.isArrow = isArrow
        self.delegate = target
        if let targetViewController: UIViewController = target as? UIViewController {
            self.targetView = targetViewController.view
        }
        self.registerDevgKeyboard()
    }
    
    // MARK: - register
    func registerDevgKeyboard() {
        self.textItemArray.removeAll()
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(DevgKeyboard.changeKeyboardStatus(with:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DevgKeyboard.changeKeyboardStatus(with:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DevgKeyboard.changeKeyboardStatus(with:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DevgKeyboard.changeKeyboardStatus(with:)), name: UIResponder.keyboardDidHideNotification, object: nil)
        
        self.addTextItemArray(view: self.targetView)
        self.itemsSetAccessoryView()
    }
    
    
    func unRegisterDevgKeyboard() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    // MARK: - function
    func addTextItemArray(view: UIView) {
        
        let subView: [UIView] = view.subviews
        if subView.count == 0 {
            return
        }
        
        for object: UIView in subView {
            if let textField: UITextField = object as? UITextField {
                self.textItemArray.append(textField)
            }
            if let textView: UITextView = object as? UITextView {
                self.textItemArray.append(textView)
            }
            self.addTextItemArray(view: object)
        }
    }
    
    func itemsSetAccessoryView() {
        let imageSize: CGFloat = 20
        
        for (index, textItem) in self.textItemArray.enumerated() {
            let bundle: Bundle = Bundle(for: type(of: self))
            
            let prevButton: UIButton = UIButton(type: .system)
            prevButton.frame = CGRect(x: 0, y: 0, width: imageSize, height: imageSize)
            prevButton.setTitle("", for: .normal)
            prevButton.setImage(UIImage(named: "Up", in: bundle, compatibleWith: nil), for: .normal)
            prevButton.setImage(UIImage(named: "UpDisabled", in: bundle, compatibleWith: nil), for: .disabled)
            prevButton.addTarget(self, action: #selector(DevgKeyboard.prevButtonPush(sender:)), for: .touchUpInside)
            prevButton.tag = index
            
            let nextButton: UIButton = UIButton(type: .system)
            nextButton.frame = CGRect(x: 10, y: 0, width: imageSize, height: imageSize)
            nextButton.setTitle("", for: .normal)
            nextButton.setImage(UIImage(named: "Down", in: bundle, compatibleWith: nil), for: .normal)
            nextButton.setImage(UIImage(named: "DownDisabled", in: bundle, compatibleWith: nil), for: .disabled)
            nextButton.addTarget(self, action: #selector(DevgKeyboard.nextButtonPush(sender:)), for: .touchUpInside)
            nextButton.tag = index
            
            let centerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 36, height: 4.5))
            centerView.layer.cornerRadius = 2.5
            centerView.layer.masksToBounds = true
            centerView.backgroundColor = UIColor.clear
            
            
            let prevButtonItem: UIBarButtonItem = UIBarButtonItem(customView: prevButton)
            let nextButtonItem: UIBarButtonItem = UIBarButtonItem(customView: nextButton)
            let centerButtinItem: UIBarButtonItem = UIBarButtonItem(customView: centerView)
            let doneButtonItem: UIBarButtonItem = UIBarButtonItem(title: "완료", style: .done, target: self, action: #selector(DevgKeyboard.doneButtonPush(sender:)))
            let spaceButtonItem: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let fixButtonItem: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
            fixButtonItem.width = 30
            doneButtonItem.width = 20
            doneButtonItem.tag = index
            
            
            let toolBar: UIToolbar = UIToolbar()
            toolBar.barStyle = .default
            toolBar.isTranslucent = true
            toolBar.tintColor = self.defaultColor
            if self.isArrow {
                toolBar.setItems([prevButtonItem, nextButtonItem, spaceButtonItem, centerButtinItem, spaceButtonItem, fixButtonItem, doneButtonItem], animated: false)
            }
            else {
                toolBar.setItems([spaceButtonItem, centerButtinItem, spaceButtonItem, fixButtonItem, doneButtonItem], animated: false)
            }
            toolBar.sizeToFit()
            
            self.toolbarArray.append(toolBar)
            if index == self.textItemArray.count - 1 {
                nextButtonItem.isEnabled = false
            }
            if index == 0 {
                prevButtonItem.isEnabled = false
            }
            
            
            if let field: UITextField = textItem as? UITextField {
                field.inputAccessoryView = toolBar
            }
            if let view: UITextView = textItem as? UITextView {
                view.inputAccessoryView = toolBar
            }
            
            
        }
    }
    
    
    @objc func changeKeyboardStatus(with notification: Notification) {
        guard let keyboardValue: NSValue = notification.userInfo?["UIKeyboardFrameEndUserInfoKey"] as? NSValue else {
            return
        }
        
        let keyboardbounds: CGRect = keyboardValue.cgRectValue
        var keyboardStatus: KeyboardStatus = .willHide
        switch notification.name {
        case UIResponder.keyboardWillShowNotification:
            keyboardStatus = .willShow
            break
        case UIResponder.keyboardDidShowNotification:
            keyboardStatus = .didShow
            break
        case UIResponder.keyboardWillHideNotification:
            keyboardStatus = .willHide
            break
        case UIResponder.keyboardDidHideNotification:
            keyboardStatus = .didHide
            break
        default :
            break
        }
        
        self.delegate?.keyboardChangeStatus(keyboardStatus, notification: notification, size: keyboardbounds.size)
    }
    
    
    // MARK: - Action
    @objc func prevButtonPush(sender: UIButton) {
        if let item: UITextField = self.textItemArray[sender.tag - 1] as? UITextField {
            item.becomeFirstResponder()
        }
        else if let item: UITextView = self.textItemArray[sender.tag - 1] as? UITextView {
            item.becomeFirstResponder()
        }
    }
    
    @objc func nextButtonPush(sender: UIButton) {
        if let item: UITextField = self.textItemArray[sender.tag + 1] as? UITextField {
            item.becomeFirstResponder()
        }
        else if let item: UITextView = self.textItemArray[sender.tag + 1] as? UITextView {
            item.becomeFirstResponder()
        }
    }
    
    @objc func doneButtonPush(sender: UIBarButtonItem) {
        self.targetView.endEditing(true)
    }
}
