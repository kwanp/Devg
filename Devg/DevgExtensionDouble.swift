//
//  DevgExtensionDouble.swift
//  Devg
//
//  Created by Kwan on 23/07/2017.
//  Copyright © 2017 Kwan. All rights reserved.
//

import Foundation

extension Double {
    public func nanToNil() -> Double? {
        if self.isNaN {
            return nil
        }
        
        return self
    }
}
