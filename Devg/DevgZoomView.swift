//
//  DevgZoomView.swift
//  Devg
//
//  Created by Lee JinWook on 2016. 3. 25..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit

@IBDesignable public class DevgZoomView: UIView, UIScrollViewDelegate {
    public var imageView: UIImageView = UIImageView()
    public var imageScrollView: UIScrollView = UIScrollView()
    
    @IBInspectable public var image: UIImage? {
        didSet {
            self.imageView.image = self.image
        }
    }
    
    @IBInspectable public var minimumZoomScale: CGFloat = 1.0 {
        didSet {
            self.imageScrollView.minimumZoomScale = self.minimumZoomScale
        }
    }
    
    @IBInspectable public var maximumZoomScale: CGFloat = 2.0 {
        didSet {
            self.imageScrollView.maximumZoomScale = self.maximumZoomScale
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        self.setup()
    }
    
    func setup() {
        if self.gestureRecognizers?.count == 0 || self.gestureRecognizers == nil {
            let doubleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DevgZoomView.doubleTapGesture(_:)))
            doubleTap.numberOfTapsRequired = 2
            self.imageView.addGestureRecognizer(doubleTap)
            self.imageView.isUserInteractionEnabled = true
            self.imageView.frame = self.frame
            self.imageView.contentMode = self.contentMode
            self.imageScrollView.frame = self.frame
            self.imageScrollView.delegate = self
            self.imageScrollView.addSubview(self.imageView)
            self.imageScrollView.scrollsToTop = false
            
            self.addSubview(self.imageScrollView)
            
            let topScrollViewConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self.imageScrollView, attribute: .top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)
            let leftScrollViewConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self.imageScrollView, attribute: .leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
            let bottomScrollViewConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self.imageScrollView, attribute: .bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
            let rightScrollViewConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self.imageScrollView, attribute: .trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
            
            self.addConstraints([topScrollViewConstraint, leftScrollViewConstraint, bottomScrollViewConstraint, rightScrollViewConstraint])
            
            let topImageViewConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self.imageView, attribute: .top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.imageScrollView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)
            let leftImageViewConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self.imageView, attribute: .leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.imageScrollView, attribute: .leading, multiplier: 1, constant: 0)
            let bottomImageViewConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self.imageView, attribute: .bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.imageScrollView, attribute: .bottom, multiplier: 1, constant: 0)
            let rightImageViewConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self.imageView, attribute: .trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.imageScrollView, attribute: .trailing, multiplier: 1, constant: 0)
            
            self.imageScrollView.addConstraints([topImageViewConstraint, leftImageViewConstraint, bottomImageViewConstraint, rightImageViewConstraint])
        }
    }
    
    @objc public func doubleTapGesture(_ gestureRecognizer: UITapGestureRecognizer) {
        if self.imageScrollView.zoomScale == 1.0 {
            self.imageScrollView.zoom(to: self.zoomRect(scale: self.maximumZoomScale, center: gestureRecognizer.location(in: gestureRecognizer.view)), animated: true)
        }
        else {
            self.imageScrollView.zoom(to: self.zoomRect(scale: self.minimumZoomScale, center: gestureRecognizer.location(in: gestureRecognizer.view)), animated: true)
        }
    }
    
    public func zoomToScale(scale: CGFloat) {
        self.imageScrollView.zoom(to: self.zoomRect(scale: scale, center: self.imageView.center), animated: true)
    }
    
    func zoomRect(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect: CGRect = self.imageScrollView.frame
        zoomRect.size.height = self.imageScrollView.frame.height / scale
        zoomRect.size.width = self.imageScrollView.frame.width / scale
        zoomRect.origin.x = center.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = center.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
    // MARK: - UIScrollViewDelegate
    public func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
}
