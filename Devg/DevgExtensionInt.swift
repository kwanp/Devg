//
//  DevgExtensionInt.swift
//  Devg
//
//  Created by Kwan on 2018. 4. 3..
//  Copyright © 2018년 Kwan. All rights reserved.
//

import Foundation

public extension Int {
    public func toString() -> String {
        return String(self)
    }
}
