//
//  DevgExtensionUILabel.swift
//  Devg
//
//  Created by Kwan on 24/03/2017.
//  Copyright © 2017 KimMinKu. All rights reserved.
//

import UIKit

public extension UILabel {
    func changeColor(_ color: UIColor, of subString: String) {
        guard let range: NSRange = self.text?.nsrange(of: subString), let oldAttributedString: NSAttributedString = self.attributedText else {
            return
        }
        
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(attributedString: oldAttributedString)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        self.attributedText = attributedString
    }
    
    func changeFont(_ font: UIFont, of subString: String) {
        guard let range: NSRange = self.text?.nsrange(of: subString), let oldAttributedString: NSAttributedString = self.attributedText else {
            return
        }
        
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(attributedString: oldAttributedString)
        attributedString.addAttribute(NSAttributedString.Key.font, value: font, range: range)
        self.attributedText = attributedString
    }
    
    func height(width: CGFloat) -> CGFloat {
        guard let string: String = self.text else {
            return 0
        }
        
        return string.height(withConstrainedWidth: width, font: self.font)
    }
    
    func width(height: CGFloat) -> CGFloat {
        guard let string: String = self.text else {
            return 0
        }
        
        return string.width(withConstrainedHeight: height, font: self.font)
    }
}
