//
//  DevgExtensionUIView.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import Foundation
import UIKit

public extension UIView {
    var isShow: Bool {
        get {
            return !self.isHidden
        }
        set(value) {
            self.isHidden = !value
        }
    }
    
    func showView() {
        self.isShow = true
    }
    
    func hideView() {
        self.isHidden = true
    }
    
    func capture() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: false)
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return image
        }
        else {
            return nil
        }
    }
    
    func addSubview(_ view: UIView, topConstant: CGFloat?, leftConstant: CGFloat?, rightConstant: CGFloat?, bottomConstant: CGFloat?) {
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        if let top: CGFloat = topConstant {
            view.removeConstant(NSLayoutConstraint.Attribute.top)
            let topConstraint: NSLayoutConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: top)
            self.addConstraint(topConstraint)
        }
        if let left: CGFloat = leftConstant {
            view.removeConstant(NSLayoutConstraint.Attribute.leading)
            let leftConstraint: NSLayoutConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: left)
            self.addConstraint(leftConstraint)
        }
        if let right: CGFloat = rightConstant {
            view.removeConstant(NSLayoutConstraint.Attribute.trailing)
            let rightConstraint: NSLayoutConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: right)
            self.addConstraint(rightConstraint)
        }
        if let bottom: CGFloat = bottomConstant {
            view.removeConstant(NSLayoutConstraint.Attribute.bottom)
            let bottomConstraint: NSLayoutConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: bottom)
            self.addConstraint(bottomConstraint)
        }
        
        self.setNeedsLayout()
        self.superview?.setNeedsLayout()
    }
    
    func topConstant(_ constant: CGFloat = 0, toView: UIView, toAttribute: NSLayoutConstraint.Attribute = .bottom) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.removeConstant(NSLayoutConstraint.Attribute.top)
        
        let topConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: toView, attribute: toAttribute, multiplier: 1, constant: constant)
        self.superview?.addConstraint(topConstraint)
        
        self.setNeedsLayout()
        self.superview?.setNeedsLayout()
    }
    
    func leftConstant(_ constant: CGFloat = 0, toView: UIView, toAttribute: NSLayoutConstraint.Attribute = .trailing) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.removeConstant(NSLayoutConstraint.Attribute.leading)
        
        let leftConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: toView, attribute: toAttribute, multiplier: 1, constant: constant)
        self.superview?.addConstraint(leftConstraint)
        
        self.setNeedsLayout()
        self.superview?.setNeedsLayout()
    }
    
    func rightConstant(_ constant: CGFloat = 0, toView: UIView, toAttribute: NSLayoutConstraint.Attribute = .leading) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.removeConstant(NSLayoutConstraint.Attribute.trailing)
        
        let rightConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: toView, attribute: toAttribute, multiplier: 1, constant: constant)
        self.superview?.addConstraint(rightConstraint)
        
        self.setNeedsLayout()
        self.superview?.setNeedsLayout()
    }
    
    func bottomConstant(_ constant: CGFloat = 0, toView: UIView, toAttribute: NSLayoutConstraint.Attribute = .top) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.removeConstant(NSLayoutConstraint.Attribute.bottom)
        
        let bottomConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: toView, attribute: toAttribute, multiplier: 1, constant: constant)
        self.superview?.addConstraint(bottomConstraint)
        
        self.setNeedsLayout()
        self.superview?.setNeedsLayout()
    }
    
    func widthConstant(_ constant: CGFloat) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.removeConstant(NSLayoutConstraint.Attribute.width)
        
        let widthConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: constant)
        self.addConstraint(widthConstraint)
        
        self.setNeedsLayout()
        self.superview?.setNeedsLayout()
    }
    
    func heightConstant(_ constant: CGFloat) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.removeConstant(NSLayoutConstraint.Attribute.height)
        
        let heightConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: constant)
        self.addConstraint(heightConstraint)
        
        self.setNeedsLayout()
        self.superview?.setNeedsLayout()
    }
    
    func removeConstant(_ attribute: NSLayoutConstraint.Attribute) {
        for constraint in self.constraints {
            if constraint.firstAttribute == attribute {
                self.removeConstraint(constraint)
            }
        }
    }
    
    class var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    class var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    
    class var insetSafeAreaTop: CGFloat {
        if #available(iOS 11.0, *) {
            return UIApplication.shared.keyWindow?.rootViewController?.view.safeAreaInsets.top ?? 20
        }
        else {
            return 20
        }
    }
    
    class var insetSafeAreaBottom: CGFloat {
        if #available(iOS 11.0, *) {
            return UIApplication.shared.keyWindow?.rootViewController?.view.safeAreaInsets.bottom ?? 0
        }
        else {
            return 0
        }
    }
}
