//
//  DevgPlayer.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 10..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MediaPlayer
import Devg
import CoreTelephony

public protocol DevgPlayerDelegate: AnyObject {
    func player(_ player: DevgPlayer, changeStatus status: DevgPlayerStatus.Enum)
    func player(_ player: DevgPlayer, duration: DevgPlayerAV.Duration)
    func player(_ player: DevgPlayer, playStartQueue queue: DevgPlayerQueue.Struct?)
    func player(_ player: DevgPlayer, playEndQueue queue: DevgPlayerQueue.Struct?)
    func player(_ player: DevgPlayer, removedCurrentQueue queue: DevgPlayerQueue.Struct?)
}

open class DevgPlayer: NSObject, DevgPlayerAVDelegate, DevgPlayerNetworkDelegate, DevgPlayerQueueDelegate {
    open var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid

    open weak var delegate: DevgPlayerDelegate?
    open var timer: Timer?
    open var playingQueue: DevgPlayerQueue.Struct?
    
    open var status: DevgPlayerStatus = DevgPlayerStatus()
    open var queue: DevgPlayerQueue = DevgPlayerQueue()
    open var network: DevgPlayerNetwork = DevgPlayerNetwork()
    open var av: DevgPlayerAV = DevgPlayerAV()
    
    open var isResume: Bool = false
    open var callCenter = CTCallCenter()
    
    public static var shared: DevgPlayer = DevgPlayer()
    
    public override init() {
        super.init()
        MPRemoteCommandCenter.shared().skipBackwardCommand.isEnabled = false
        MPRemoteCommandCenter.shared().seekBackwardCommand.isEnabled = false
        MPRemoteCommandCenter.shared().skipForwardCommand.isEnabled = false
        MPRemoteCommandCenter.shared().seekForwardCommand.isEnabled = false
        self.network.delegate = self
        self.av.delegate = self
        self.queue.delegate = self
    }
    
    // MARK: - Functions
    // MARK: settings
    open func setHeaders(_ headers: [String: String]) {
        self.av.headers = headers
    }
    
    open func setRepeatType(_ repeatType: DevgPlayerQueue.RepeatType) {
        self.queue.repeatType = repeatType
    }
    
    open func currentRepeatType() -> DevgPlayerQueue.RepeatType {
        return self.queue.repeatType
    }
    
    open func setResumeMode(_ bool: Bool) {
        self.isResume = bool
    }
    
    func currentResumeMode() -> Bool {
        return self.isResume
    }
    
    // MARK: control
    open func play() {
        var allow: Bool = false
        if self.network.isForcePlay {
            allow = true
        }
        else if self.network.allowPlay() {
            allow = true
        }
        
        self.network.isForcePlay = false
        if self.playingQueue == nil {
            if let queue: DevgPlayerQueue.Struct = self.queue.current() {
                self.playingQueue = queue
                
                if queue.isStreaming {
                    self.status.setCurrent(DevgPlayerStatus.Enum.start)
                    self.delegate?.player(self, changeStatus: self.status.current())
                    self.delegate?.player(self, playStartQueue: self.queue.current())
                }
                
                var resumeTime: Int? = nil
                if self.isResume {
                    resumeTime = queue.resumeTime
                }
                self.av.setContentURL(queue.url, isStreaming: queue.isStreaming, isCache: queue.isCache, resumeTime: resumeTime)
            }
        }
        
        if let queue: DevgPlayerQueue.Struct = self.playingQueue, allow || !queue.isStreaming {
            self.av.play()
        }
        else {
            self.status.setCurrent(DevgPlayerStatus.Enum.stop)
            self.delegate?.player(self, changeStatus: self.status.current())
        }
    }
    
    open func pause() {
        self.status.setCurrent(DevgPlayerStatus.Enum.pause)
        self.delegate?.player(self, changeStatus: self.status.current())
        
        self.av.pause()
    }
    
    open func stop(_ resetIndex: Bool = false) {
        if self.status.current() != DevgPlayerStatus.Enum.stop {
            self.playingQueue = nil
            self.status.setCurrent(DevgPlayerStatus.Enum.stop)
            self.delegate?.player(self, changeStatus: self.status.current())
            
            self.av.stop()
        }
    }
    
    open func newPlay() {
        self.stop()
        self.playingQueue = nil
        self.play()
    }
    
    open func next() {
        if self.queue.repeatType == .normal && self.queue.index + 1 >= self.queue.queues.count {
            self.stop()
        }
        else {
            self.queue.index = self.queue.nextQueueIndex()
            self.newPlay()
        }
    }
    
    open func previous() {
        if self.queue.repeatType == .normal && self.queue.index - 1 < 0 {
            self.stop()
        }
        else {
            self.queue.index = self.queue.previousQueueIndex()
            self.newPlay()
        }
    }
    
    open func currentSpeed() -> Float {
        return self.av.currentSpeed()
    }
    
    open func changeSpeed(_ rate: Float) {
        if rate > 0.0 {
            self.av.changeSpeed(rate)
        }
    }
    
    open func timeToJump(_ jump: Double) {
        self.av.timeToJump(jump)
    }
    
    open func timeToJump(_ jump: Double, completion: @escaping (Bool) -> Void) {
        self.av.timeToJump(jump, completion: completion)
    }
    
    open func timeToSeek(_ seek: Double) {
        self.av.timeToSeek(seek)
    }
    
    open func timeToSeek(_ seek: Double, completion: @escaping (Bool) -> Void) {
        self.av.timeToSeek(seek, completion: completion)
    }
    
    open func singlePlayNewQueue(_ queue: DevgPlayerQueue.Struct) {
        self.stop()
        self.resetQueues()
        self.addQueue(queue)
        self.newPlay()
    }
    
    open func singlePlayVideoOnFullScreen(_ url: String) {
        self.stop()
        
        if FileManager.default.fileExists(atPath: url) {
            let fileUrl: URL = URL(fileURLWithPath: url)
            
            let avViewController: AVPlayerViewController = AVPlayerViewController()
            avViewController.showsPlaybackControls = false
            avViewController.player = AVPlayer(url: fileUrl)
            Devg.visibleViewController().present(avViewController, animated: true, completion: nil)
        }
        else {
            var aUrl: String = url
            if !url.isRegex("://") {
                aUrl = "http://" + url
            }
            
            if let bUrl: URL = URL(string: aUrl) {
                let avViewController: AVPlayerViewController = AVPlayerViewController()
                avViewController.showsPlaybackControls = false
                avViewController.player = AVPlayer(url: bUrl)
                Devg.visibleViewController().present(avViewController, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: status
    open func currentStatus() -> DevgPlayerStatus.Enum {
        return self.status.current()
    }
    
    // MARK: playlist
    open func currentQueue() -> DevgPlayerQueue.Struct? {
        return self.queue.current()
    }
    
    open func playQueue(_ queue: DevgPlayerQueue.Struct) {
        if let findIndex = self.queue.findIndex(of: queue) {
            self.setIndex(findIndex)
            self.newPlay()
        }
    }
    
    open func playIndex(_ index: Int) {
        self.setIndex(index)
        self.newPlay()
    }
    
    open func syncQueueIndex(_ queue: DevgPlayerQueue.Struct) {
        if let findIndex: Int = self.queue.findIndex(of: queue) {
            self.queue.index = findIndex
        }
    }
    
    open func addQueue(_ queue: DevgPlayerQueue.Struct) {
        self.queue.add(queue)
    }
    
    open func removeQueueIndex(_ index: Int) {
        self.queue.remove(at: index)
    }
    
    open func removeQueue(_ queue: DevgPlayerQueue.Struct) {
        if let findIndex: Int = self.queue.findIndex(of: queue) {
            self.removeQueueIndex(findIndex)
        }
    }
    
    open func changeIndex(_ index: Int, queue: DevgPlayerQueue.Struct) {
        self.queue.changeIndex(index, queue: queue)
    }
    
    open func setIndex(_ index: Int, beforeStop isStop: Bool = true) {
        if isStop {
            self.stop()
        }
        self.queue.index = index
    }
    
    open func setIndexWithQueue(_ queue: DevgPlayerQueue.Struct, beforeStop isStop: Bool = true) {
        guard let findIndex: Int = self.queue.findIndex(of: queue) else {
            return
        }
        
        self.setIndex(findIndex, beforeStop: isStop)
    }
    
    open func setQueues(_ queues: [DevgPlayerQueue.Struct], currentIndex index: Int = 0, beforeStop isStop: Bool = true) {
        self.queue.queues = queues
        self.setIndex(index, beforeStop: isStop)
    }
    
    open func resetQueues() {
        self.queue.reset()
    }
    
    open func queuesCount() -> Int {
        return self.queue.queues.count
    }
    
    // MARK: network
    open func setAllowNetwork(_ allowNetwork: DevgPlayerNetwork.Allow) {
        self.network.allow = allowNetwork
    }
    
    // MARK: etc
    @objc open func timeLoop() {
        guard let player: AVPlayer = self.av.av else {
            return
        }
        
        if self.status.current() != .playing {
            return
        }
        
        self.callCenter.callEventHandler = { (call:CTCall) in
            switch call.callState {
            case CTCallStateConnected, CTCallStateDialing, CTCallStateIncoming:
                self.callConnected()
                break
            case CTCallStateDisconnected:
                break
            default:
                break
            }
        }
        
        if self.isResume, let seconds: Double = player.currentTime().seconds.nanToNil() {
            self.queue.setResumeTime(Int(seconds), atIndex: self.queue.index)
        }
        
        if var songInfo: [String : AnyObject] = MPNowPlayingInfoCenter.default().nowPlayingInfo as [String : AnyObject]?, let currentItem: AVPlayerItem = player.currentItem {
            if let seconds: Double = player.currentTime().seconds.nanToNil() {
                songInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = NSNumber(value: Int32(seconds) as Int32)
            }
            if let seconds: Double = currentItem.asset.duration.seconds.nanToNil() {
                songInfo[MPMediaItemPropertyPlaybackDuration] = NSNumber(value: Int32(seconds) as Int32)
            }
            MPNowPlayingInfoCenter.default().nowPlayingInfo = songInfo
        }
        
        self.delegate?.player(self, duration: self.av.duration())
    }
    
    open func lockScreenWithQueue(_ queue: DevgPlayerQueue.Struct?) {
        guard let songQueue: DevgPlayerQueue.Struct = queue else {
            return
        }
        
        if NSClassFromString("MPNowPlayingInfoCenter") == nil {
            return
        }
        
        var isNew: Bool = true
        if let nowInfo: [String: AnyObject] = MPNowPlayingInfoCenter.default().nowPlayingInfo as [String : AnyObject]? {
            if String(describing: nowInfo[MPMediaItemPropertyTitle]).unwrap() == songQueue.title && String(describing: nowInfo[MPMediaItemPropertyArtist]).unwrap() == songQueue.artist && String(describing: nowInfo[MPMediaItemPropertyAlbumTitle]).unwrap() == songQueue.album {
                isNew = false
            }
        }
        
        if isNew {
            var songInfo: [String: AnyObject] = [MPMediaItemPropertyTitle: songQueue.title as AnyObject]
            if let string: String = songQueue.artist {
                songInfo[MPMediaItemPropertyArtist] = string as AnyObject?
            }
            if let string: String = songQueue.album {
                songInfo[MPMediaItemPropertyAlbumTitle] = string as AnyObject?
            }
            
            if let image: UIImage = songQueue.albumArt as? UIImage {
                songInfo[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(image: image)
                MPNowPlayingInfoCenter.default().nowPlayingInfo = songInfo
            }
            else if let string: String = songQueue.albumArt as? String {
                UIImageView().setImage(fromUrl: string, defaultImage: UIImage(), completion: { (image, isCache) -> Void in
                    songInfo[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(image: image)
                    MPNowPlayingInfoCenter.default().nowPlayingInfo = songInfo
                }, error: { (error) -> Void in
                    e(error)
                    MPNowPlayingInfoCenter.default().nowPlayingInfo = songInfo
                })
            }
            else {
                MPNowPlayingInfoCenter.default().nowPlayingInfo = songInfo
            }
        }
    }
    
    open func registerBackgroundTask() {
        if self.backgroundTask != UIBackgroundTaskIdentifier.invalid {
            return
        }
        
        l("백그라운드 등록")
        
        self.backgroundTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            UIApplication.shared.endBackgroundTask(convertToUIBackgroundTaskIdentifier(self.backgroundTask.rawValue))
            self.backgroundTask = UIBackgroundTaskIdentifier.invalid
        })
        if self.backgroundTask == UIBackgroundTaskIdentifier.invalid {
            e("백그라운드 실패")
        }
    }
    
    open func endBackgroundTask() {
        if self.backgroundTask == UIBackgroundTaskIdentifier.invalid {
            return
        }
        
        l("백그라운드 해제")
        
        UIApplication.shared.endBackgroundTask(convertToUIBackgroundTaskIdentifier(self.backgroundTask.rawValue))
        self.backgroundTask = UIBackgroundTaskIdentifier.invalid
    }
    
    // MARK: - Player Network delegate
    open func wifiToCellular() {
        if self.status.current() == DevgPlayerStatus.Enum.playing {
            switch self.network.allow {
            case .onlyWifi, .questionCellular:
                var isDeny: Bool = true
                if let queue: DevgPlayerQueue.Struct = self.playingQueue {
                    if !queue.isStreaming {
                        isDeny = false
                    }
                }
                
                if isDeny {
                    if #available(iOS 8.3, *) {
                        let alert: UIAlertController = UIAlertController(title: "네트워크 연결", message: "Wi-Fi 연결이 3G/4G 로 변경되어 종료되었습니다.", preferredStyle: .alert)
                        let cancelAction: UIAlertAction = UIAlertAction(title: "확인", style: .cancel, handler: nil)
                        alert.addAction(cancelAction)
                        Devg.visibleViewController().present(alert, animated: true, completion: nil)
                    }
                    else {
                        let alertView = UIAlertView(title: "네트워크 연결", message: "Wi-Fi 연결이 3G/4G 로 변경되어 종료되었습니다.", delegate: nil, cancelButtonTitle: "확인")
                        alertView.show()
                    }
                    self.pause()
                }
                break;
            default:
                break;
            }
        }
    }
    
    open func forcePlay() {
        self.network.isForcePlay = true
        self.play()
    }
    
    /* in AppDelegate.swift
     override func remoteControlReceivedWithEvent(event: UIEvent?) {
     Player.sharedInstance.remoteControlReceivedWithEvent(event)
     }
     */
    open func remoteControlReceivedWithEvent(_ event: UIEvent?) {
        if let resiveEvent: UIEvent = event {
            if resiveEvent.type == UIEvent.EventType.remoteControl {
                switch resiveEvent.subtype {
                case .remoteControlPlay:
                    self.play()
                case .remoteControlStop:
                    self.stop()
                case .remoteControlPause:
                    self.pause()
                case .remoteControlPreviousTrack:
                    self.previous()
                case .remoteControlNextTrack:
                    self.next()
                case .remoteControlTogglePlayPause:
                    if self.currentStatus() == DevgPlayerStatus.Enum.playing {
                        self.pause()
                    }
                    else {
                        self.play()
                    }
                default:
                    break
                }
            }
        }
    }
    
    // MARK: - Player AV delegate
    open func av(_ av: AVPlayer, changePlaybackState state: AVPlayerItem.Status, error: NSError?) {
        if let _: NSError = error {
            if #available(iOS 8.3, *) {
                let alert: UIAlertController = UIAlertController(title: "", message: "해당 미디어를 재생할 수 없습니다. 네트워크 상태나 미디어의 상태를 확인해 주세요.", preferredStyle: .alert)
                let okAction: UIAlertAction = UIAlertAction(title: "확인", style: .cancel, handler: nil)
                alert.addAction(okAction)
                Devg.visibleViewController().present(alert, animated: true, completion: nil)
            }
            else {
                UIAlertView(title: "", message: "해당 미디어를 재생할 수 없습니다. 네트워크 상태나 미디어의 상태를 확인해 주세요.", delegate: nil, cancelButtonTitle: "확인").show()
            }
            self.stop()
        }
        
    }
    
    open func av(_ av: AVPlayer, accessLogWithLog: AVPlayerItemAccessLog?) {
        if ((av.rate != 0) && (av.error == nil)) {
            self.status.setCurrent(DevgPlayerStatus.Enum.playing)
            
            if self.timer == nil {
                self.timeLoop()
                self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DevgPlayer.timeLoop), userInfo: nil, repeats: true)
            }
            
            self.delegate?.player(self, changeStatus: self.status.current())
        } else {
            self.status.setCurrent(DevgPlayerStatus.Enum.pause)
            self.delegate?.player(self, changeStatus: self.status.current())
        }
    }
    
    open func av(_ av: AVPlayer, errorLogWithLog: AVPlayerItemErrorLog?, error: NSError?) {
        if #available(iOS 8.3, *) {
            let alert: UIAlertController = UIAlertController(title: "", message: "해당 미디어를 재생할 수 없습니다. 네트워크 상태나 미디어의 상태를 확인해 주세요.", preferredStyle: .alert)
            let okAction: UIAlertAction = UIAlertAction(title: "확인", style: .cancel, handler: nil)
            alert.addAction(okAction)
            Devg.visibleViewController().present(alert, animated: true, completion: nil)
        }
        else {
            UIAlertView(title: "", message: "해당 미디어를 재생할 수 없습니다. 네트워크 상태나 미디어의 상태를 확인해 주세요.", delegate: nil, cancelButtonTitle: "확인").show()
        }
        self.stop()
    }
    
    open func av(_ av: AVPlayer, playbackFinish: AVPlayerItemAccessLog?) {
        self.delegate?.player(self, playEndQueue: self.queue.current())
        self.next()
    }
    
    // MARK: - Player Queue delegate
    open func queue(_ queue: DevgPlayerQueue, removedCurrentQueue removedQueue: DevgPlayerQueue.Struct) {
        self.stop()
        self.delegate?.player(self, removedCurrentQueue: removedQueue)
    }
    
    // MARK: -  CTCallCenterEventHandler
    open func callConnected() {
        if self.status.current() == .playing {
            self.pause()
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIBackgroundTaskIdentifier(_ input: Int) -> UIBackgroundTaskIdentifier {
	return UIBackgroundTaskIdentifier(rawValue: input)
}
