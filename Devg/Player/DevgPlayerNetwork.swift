//
//  DevgPlayerNetwork.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 10..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit
import Devg
import DevgAlert
import Reachability

public protocol DevgPlayerNetworkDelegate: AnyObject {
    func wifiToCellular()
    func forcePlay()
}

open class DevgPlayerNetwork: NSObject, UIAlertViewDelegate {
    public enum Allow: Int {
        case all
        case onlyWifi
        case lostWifi
        case questionCellular
    }
    
    public enum Connection: Int {
        case notConnected
        case wifi
        case cellular
    }
    
    open var isForcePlay: Bool = false
    
    open weak var delegate: DevgPlayerNetworkDelegate?
    open var connection: DevgPlayerNetwork.Connection = DevgPlayerNetwork.Connection.notConnected
    
    internal var allow: DevgPlayerNetwork.Allow = DevgPlayerNetwork.Allow.questionCellular
    fileprivate var reachability: Reachability?
    
    // MARK: - Overrides
    public override init() {
        super.init()
        
        self.reachability = Reachability.init()
        NotificationCenter.default.addObserver(self, selector: #selector(DevgPlayerNetwork.reachabilityChanged(_:)),name: ReachabilityChangedNotification, object: reachability)
        
        reachability?.whenReachable = { reachability in
            DispatchQueue.main.async {
                if reachability.isReachableViaWiFi {
                    self.connection = .wifi
                } else {
                    if self.connection == .wifi {
                        self.delegate?.wifiToCellular()
                    }
                    
                    self.connection = .cellular
                }
            }
        }
        reachability?.whenUnreachable = { reachability in
            DispatchQueue.main.async {
                self.connection = .notConnected
            }
        }
        
        if (self.reachability?.isReachable)! {
            if (self.reachability?.isReachableViaWiFi)! {
                self.connection = .wifi
            } else {
                self.connection = .cellular
            }
        }
        
        do {
            try self.reachability?.startNotifier()
        }
        catch let error {
            e(error)
        }
    }
    
    deinit {
        self.reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: ReachabilityChangedNotification, object: reachability)
    }
    
    // MARK: - Functions
    open func allowPlay() -> Bool {
        var allowPlay: Bool = true
        if self.connection == .cellular {
            switch self.allow {
            case .onlyWifi, .lostWifi:
                if #available(iOS 8.3, *) {
                    let alert: UIAlertController = UIAlertController(title: "네트워크 연결", message: "3G/4G 환경에서 재생하실 수 없습니다.", preferredStyle: .alert)
                    let cancelAction: UIAlertAction = UIAlertAction(title: "확인", style: .cancel, handler: nil)
                    alert.addAction(cancelAction)
                    Devg.visibleViewController().present(alert, animated: true, completion: nil)
                }
                else {
                    UIAlertView(title: "네트워크 연결", message: "3G/4G 환경에서 재생하실 수 없습니다.", delegate: nil, cancelButtonTitle: "확인").show()
                }
                allowPlay = false
                break;
            case .questionCellular:
                if #available(iOS 8.3, *) {
                    let alert: UIAlertController = UIAlertController(title: "네트워크 연결", message: "3G/4G 환경입니다. 데이타요금이 발생할 수 있습니다. 재생하시겠습니까?", preferredStyle: .alert)
                    let cancelAction: UIAlertAction = UIAlertAction(title: "취소", style: .cancel, handler: nil)
                    alert.addAction(cancelAction)
                    let playAction: UIAlertAction = UIAlertAction(title: "재생", style: .default, handler: { (action) -> Void in
                        self.delegate?.forcePlay()
                        self.isForcePlay = false
                    })
                    alert.addAction(playAction)
                    Devg.visibleViewController().present(alert, animated: true, completion: nil)
                }
                else {
                    let alertView = UIAlertView(title: "네트워크 연결", message: "3G/4G 환경입니다. 데이타요금이 발생할 수 있습니다. 재생하시겠습니까?", delegate: self, cancelButtonTitle: "취소", otherButtonTitles: "재생")
                    alertView.tag = 1
                    alertView.show()
                }
                allowPlay = false
                break;
            default:
                break;
            }
        } else if self.connection == .notConnected {
            DevgAlert.show(withViewController: Devg.visibleViewController(), title: "네트워크 연결", message: "인터넷 연결상태를 확인 해 주세요.", buttons: ["확인"], destructiveIndex: nil, handler: { (buttonIndex) in
            })
            allowPlay = false
        }
        
        return allowPlay
    }
    
    // MARK: - Alert View Delegate
    open func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        switch alertView.tag {
        case 1:
            if buttonIndex == 1 {
                self.delegate?.forcePlay()
                self.isForcePlay = false
            }
            break;
        default:
            break;
        }
    }
    
    @objc func reachabilityChanged(_ note: Notification) {
        let reachability = note.object as! Reachability
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                if reachability.isReachableViaWiFi {
                    self.connection = .wifi
                } else {
                    if self.connection == .wifi {
                        self.delegate?.wifiToCellular()
                    }
                }
            } else {
                self.connection = .cellular
            }
        } else {
            self.connection = .notConnected
            
        }
    }
}
