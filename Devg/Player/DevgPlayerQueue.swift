//
//  DevgPlayerQueue.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 10..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit

public protocol DevgPlayerQueueDelegate: AnyObject {
    func queue(_ queue: DevgPlayerQueue, removedCurrentQueue removedQueue: DevgPlayerQueue.Struct)
}

open class DevgPlayerQueue: NSObject {
    public struct Struct {
        public var trackNo: String
        public var trackMax: String? = nil
        public var title: String
        public var artist: String? = nil
        public var album: String? = nil
        public var albumArt: AnyObject? = nil
        public var url: String
        public var resumeTime: Int?
        public var isVideo: Bool
        public var isStreaming: Bool
        public var isCache: Bool
    }
    
    public enum RepeatType: Int {
        case normal
        case all
        case one
    }
    
    open weak var delegate: DevgPlayerQueueDelegate?
    open var queues: [DevgPlayerQueue.Struct] = Array<DevgPlayerQueue.Struct>()
    open var index: Int = 0
    open var repeatType: DevgPlayerQueue.RepeatType = DevgPlayerQueue.RepeatType.normal
    
    // MARK: - Functions
    open class func create(trackNo: String, trackMax: String?, title: String, artist: String?, album: String?, albumArt: AnyObject?, url: String, resumeTime: Int?, isVideo: Bool, isStreaming: Bool, isCache: Bool = false) -> DevgPlayerQueue.Struct {
        return DevgPlayerQueue.Struct(trackNo: trackNo, trackMax: trackMax, title: title, artist: artist, album: album, albumArt: albumArt, url: url, resumeTime: resumeTime, isVideo: isVideo, isStreaming: isStreaming, isCache: isCache)
    }
    
    open func current() -> DevgPlayerQueue.Struct? {
        return self.queue(at: self.index)
    }
    
    open func add(_ queue: DevgPlayerQueue.Struct) {
        self.queues.append(queue)
    }
    
    open func add(_ queue: DevgPlayerQueue.Struct, toIndex index: Int) {
        self.queues.insert(queue, at: index)
    }
    
    open func remove(_ queue: DevgPlayerQueue.Struct) {
        if let findIndex = self.findIndex(of: queue) {
            self.queues.remove(at: findIndex)
        }
    }
    
    open func remove(at index: Int) {
        if let removeQueue: DevgPlayerQueue.Struct = self.queue(at: index) {
            self.queues.remove(at: index)
            
            if index == self.index {
                if self.index != 0 && self.index >= self.queues.count {
                    self.index = self.queues.count - 1
                }
                
                self.delegate?.queue(self, removedCurrentQueue: removeQueue)
            }
        }
    }
    
    open func setResumeTime(_ resumeTime: Int?, atIndex index: Int) {
        if self.queues.count > index {
            self.queues[index].resumeTime = resumeTime
        }
    }
    
    open func currentChangeAlbumArt(_ imageString: String) {
        if var queue = self.queues.get(self.index) {
            queue.albumArt = imageString as AnyObject?
        }
    }
    
    open func queue(at index: Int) -> DevgPlayerQueue.Struct? {
        if index < self.queues.count && index >= 0 {
            if let queue = self.queues.get(index) {
                return queue
            }
            return nil
        }
        else {
            return nil
        }
    }
    
    open func changeIndex(_ index: Int, queue: DevgPlayerQueue.Struct) {
        if let fromIndex: Int = self.findIndex(of: queue) {
            if fromIndex != index {
                if let fromQueue: DevgPlayerQueue.Struct = self.queues.get(fromIndex) {
                    var currentQueue: DevgPlayerQueue.Struct?
                    if let queue: DevgPlayerQueue.Struct = self.current() {
                        currentQueue = queue
                    }
                    
                    if fromIndex < index {
                        self.queues.remove(at: fromIndex)
                        self.queues.insert(fromQueue, at: index)
                    }
                    else {
                        self.queues.insert(fromQueue, at: index)
                        self.queues.remove(at: fromIndex + 1)
                    }
                    
                    if let queue: DevgPlayerQueue.Struct = currentQueue {
                        if let findIndex = self.findIndex(of: queue) {
                            self.index = findIndex
                        }
                    }
                }
            }
        }
    }
    
    open func reset() {
        if self.queues.count > 0 {
            self.queues.removeAll()
        }
        self.index = 0
    }
    
    open func findIndex(of queue: DevgPlayerQueue.Struct) -> Int? {
        var i: Int = -1
        for findQueue: DevgPlayerQueue.Struct in self.queues {
            i = i + 1
            var isFind: Bool = true
            
            if queue.trackNo != findQueue.trackNo {
                isFind = false
                continue
            }
            if queue.trackMax != findQueue.trackMax {
                isFind = false
                continue
            }
            if queue.title != findQueue.title {
                isFind = false
                continue
            }
            if queue.artist != findQueue.artist {
                isFind = false
                continue
            }
            if queue.album != findQueue.album {
                isFind = false
                continue
            }
            if queue.url != findQueue.url {
                isFind = false
                continue
            }
            if queue.isVideo != findQueue.isVideo {
                isFind = false
                continue
            }
            if queue.isStreaming != findQueue.isStreaming {
                isFind = false
                continue
            }
            
            if isFind {
                return i
            }
        }
        
        return nil
    }
    
    open func nextQueueIndex() -> Int {
        var nextIndex: Int = self.index + 1
        switch self.repeatType {
        case .normal:
            if nextIndex >= self.queues.count {
                nextIndex = self.index
            }
            break
        case .all:
            if nextIndex >= self.queues.count {
                nextIndex = 0
            }
        case .one:
            nextIndex = self.index
        }
        
        return nextIndex
    }
    
    open func previousQueueIndex() -> Int {
        var previousIndex: Int = self.index - 1
        switch self.repeatType {
        case .normal:
            if previousIndex < 0 {
                previousIndex = 0
            }
            break
        case .all:
            if previousIndex < 0 {
                previousIndex = self.queues.count - 1
            }
        case .one:
            previousIndex = self.index
        }
        
        return previousIndex
    }
}
