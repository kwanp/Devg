//
//  DevgPlayerAV.swift
//  Devg
//
//  Created by Kwan on 2017. 3. 6..
//  Copyright © 2017년 Kwan. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Devg


public protocol DevgPlayerAVDelegate: AnyObject {
    func av(_ av: AVPlayer, changePlaybackState state: AVPlayerItem.Status, error: NSError?)
    func av(_ av: AVPlayer, playbackFinish: AVPlayerItemAccessLog?)
    func av(_ av: AVPlayer, accessLogWithLog: AVPlayerItemAccessLog?)
    func av(_ av: AVPlayer, errorLogWithLog: AVPlayerItemErrorLog?, error: NSError?)
}

open class DevgPlayerAV: NSObject {
    let notificationCenter: NotificationCenter = NotificationCenter()
    
    var headers: [String: String]?
    
    var isUserStop: Bool = false
    var resumeTime: Int?
    open var playerUrl: URL?
    open weak var delegate: DevgPlayerAVDelegate?
    var checkPlayingTimer: Timer?
    var av: AVPlayer?
    var avItem: AVPlayerItem?
    var isFile: Bool = false
    var isCache: Bool = false
    
    public struct Duration {
        public var current: Int
        public var total: Int
    }
    
    open var volume: Float {
        get {
            if let volume: Float = self.av?.volume {
                return volume
            }
            
            return 0
        }
        set(value) {
            self.av?.volume = value
        }
    }
    
    // MARK: - Overrides
    override init() {
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(DevgPlayerAV.newAccessLogEntry(_:)), name: NSNotification.Name.AVPlayerItemNewAccessLogEntry, object: self.avItem)
        NotificationCenter.default.addObserver(self, selector: #selector(DevgPlayerAV.newErrorLogEntry(_:)), name: NSNotification.Name.AVPlayerItemNewErrorLogEntry, object: self.avItem)
        NotificationCenter.default.addObserver(self, selector: #selector(DevgPlayerAV.failedToPlayToEndTime(_:)), name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: self.avItem)
        NotificationCenter.default.addObserver(self, selector: #selector(DevgPlayerAV.didPlayToEndTime(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.avItem)

        NotificationCenter.default.addObserver(self, selector: #selector(audioHardwareChanged), name: AVAudioSession.routeChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(audioInterruption), name: AVAudioSession.interruptionNotification, object: nil)
        
        UIApplication.shared.beginReceivingRemoteControlEvents()
    }
    
    deinit {
        if let cacheItem: DevgPlayerCacheItem = self.avItem as? DevgPlayerCacheItem {
            cacheItem.session?.invalidateAndCancel()
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemNewAccessLogEntry, object: self.avItem)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemNewErrorLogEntry, object: self.avItem)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: self.avItem)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.avItem)
        self.avItem?.removeObserver(self, forKeyPath: "status")
        self.av?.removeObserver(self, forKeyPath: "status")
    }
    
    override open func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "status" {
            if let player = object as? AVPlayer {
                if player.status == .failed {
                    self.delegate?.av(player, errorLogWithLog: player.currentItem?.errorLog(), error: player.currentItem?.error as NSError?)
                }
                else if player.status == .readyToPlay {
                    if self.resumeTime != nil {
                        let t = CMTime(seconds: Double(self.resumeTime!), preferredTimescale: 1000000000)
                        player.seek(to: t)
                        self.resumeTime = nil
                    }
                    
                    player.play()
                    if #available(iOS 10.0, *) {
                        player.playImmediately(atRate: player.rate)
                    }
                    
                    if let player: AVPlayer = self.av, let item: AVPlayerItem = player.currentItem {
                        self.delegate?.av(player, changePlaybackState: item.status, error: item.error as NSError?)
                        self.delegate?.av(player, accessLogWithLog: item.accessLog())
                    }
                }
                else if player.status == .unknown {
                }
            }
        }
    }
    
    // MARK: - Functions
    open func play() {
        self.audioSessionActive()
        
        self.isUserStop = false
        
        if let player: AVPlayer = self.av {
            if self.resumeTime != nil {
                let t = CMTime(seconds: Double(self.resumeTime!), preferredTimescale: 1000000000)
                player.seek(to: t)
                self.resumeTime = nil
            }
            
            player.play()
            if #available(iOS 10.0, *) {
                player.playImmediately(atRate: player.rate)
            }
        }
        else if let url: URL = self.playerUrl {
            if self.isCache {
                self.avItem = DevgPlayerCacheItem(url: url, headers: self.headers)
            }
            else {
                var avUrlAsset: AVURLAsset = AVURLAsset(url: url)
                if let headers: [String: String] = self.headers {
                    avUrlAsset = AVURLAsset(url: url, options: ["AVURLAssetHTTPHeaderFieldsKey": headers])
                }
                self.avItem = AVPlayerItem(asset: avUrlAsset)
            }
            if let avItems = self.avItem {
                avItems.addObserver(self, forKeyPath: "status", options: .new, context: nil)
                self.av = AVPlayer(playerItem: avItems)
                if let avs = self.av {
                    avs.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions.new, context: nil)      // 상태가 알수 없음, 재생준비 일때와, 유효하지 않은 주소를 참조했을때 발생하는 에러도 이곳으로 타게
                }
            }
        }
        
        if let player: AVPlayer = self.av, let item: AVPlayerItem = player.currentItem {
            self.delegate?.av(player, changePlaybackState: item.status, error: item.error as NSError?)
            self.delegate?.av(player, accessLogWithLog: item.accessLog())
        }
    }
    
    open func isPlaying() -> Bool {
        guard let player: AVPlayer = self.av else {
            return false
        }
        if player.rate == 0 {
            return false
        }
        else {
            return true
        }
    }
    
    open func pause() {
        guard let player: AVPlayer = self.av else {
            return
        }
        guard let item: AVPlayerItem = player.currentItem else {
            return
        }
        player.pause()
        if self.playerUrl?.absoluteString.range(of: ".m3u8") != nil {
            self.delegate?.av(player, changePlaybackState: item.status, error: item.error as NSError?)
            self.delegate?.av(player, accessLogWithLog: item.accessLog())
        }
    }
    
    open func stop() {
        if let cacheItem: DevgPlayerCacheItem = self.avItem as? DevgPlayerCacheItem {
            cacheItem.session?.invalidateAndCancel()
        }
        if let player = self.av {
            self.isUserStop = true
            player.pause()
            player.removeObserver(self, forKeyPath: "status", context: nil)
            self.av = nil
        }
        if let item: AVPlayerItem = self.avItem {
            item.removeObserver(self, forKeyPath: "status")
            self.avItem = nil
        }
    }
    
    func timeToJump(_ jump: Double) {
        if let player = self.av {
            let t: CMTime = CMTime(seconds: jump, preferredTimescale: 1000000000)
            player.seek(to: t)
        }
    }
    
    func timeToJump(_ jump: Double, completion: @escaping (Bool) -> Void) {
        if let player = self.av {
            let t: CMTime = CMTime(seconds: jump, preferredTimescale: 1000000000)
            player.seek(to: t, completionHandler: completion)
        }
    }
    
    func timeToSeek(_ seek: Double) {
        if let player = self.av {
            player.currentTime()
            let t: CMTime = CMTime(seconds: seek, preferredTimescale: 1000000000)
            let resultT = CMTimeAdd(player.currentTime(), t)
            player.seek(to: resultT)
        }
    }
    
    func timeToSeek(_ seek: Double, completion: @escaping (Bool) -> Void) {
        if let player = self.av {
            player.currentTime()
            let t: CMTime = CMTime(seconds: seek, preferredTimescale: 1000000000)
            let resultT = CMTimeAdd(player.currentTime(), t)
            player.seek(to: resultT, completionHandler: completion)
        }
    }
    
    func currentSpeed() -> Float {
        if let player = self.av {
            return player.rate
        }
        return 1
    }
    
    func changeSpeed(_ rate: Float) {
        if let player = self.av {
            player.rate = rate
        }
    }
    
    open func duration() -> DevgPlayerAV.Duration {
        if let player = self.av, let currentItem: AVPlayerItem = player.currentItem {
            if let current: Double = player.currentTime().seconds.nanToNil(), let total: Double = currentItem.asset.duration.seconds.nanToNil() {
                return DevgPlayerAV.Duration(current: Int(current), total: Int(total))
            }
        }
        return DevgPlayerAV.Duration(current: 0, total: 0)
    }
    
    open func setContentURL(_ urlString: String, isStreaming: Bool, isCache: Bool) {
        self.setContentURL(urlString, isStreaming: isStreaming, isCache: isCache, resumeTime: nil)
    }
    
    open func setContentURL(_ urlString: String, isStreaming: Bool, isCache: Bool, resumeTime: Int?) {
        self.resumeTime = resumeTime
        
        if let player = self.av {
            if ((player.rate != 0) && (player.error == nil)) {
                self.stop()
            }
        }
        
        if isStreaming {
            self.isFile = false
            if let url: URL = URL(string: urlString) {
                self.playerUrl = url
            }
        }
        else {
            self.isFile = true
            self.playerUrl = URL(fileURLWithPath: urlString)
        }
        
        self.isCache = isCache
    }
    
    func checkPlaying() {
        guard let player: AVPlayer = self.av else {
            return
        }
        guard let item: AVPlayerItem = player.currentItem else {
            return
        }
        
        if player.status == .unknown || player.rate == 0 {
            self.stop()
            self.delegate?.av(player, errorLogWithLog: item.errorLog(), error: item.error as NSError?)
        }
    }
    
    // MARK: - AVSession
    fileprivate func audioSessionDeactive() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        }
        catch let error {
            e(error)
        }
    }
    
    fileprivate func audioSessionActive() {
        do {
            if #available(iOS 10.0, *) {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playback)), mode: .default)
            } else {
                // Fallback on earlier versions
            }
            try AVAudioSession.sharedInstance().setActive(true)
        }
        catch let error {
            e(error)
        }
    }
    
    // MARK: - Notifications
    @objc func audioInterruption(notification: Notification) {
        if let userInfo = notification.userInfo {
            if let reason = userInfo[AVAudioSessionInterruptionTypeKey] as? Int {
                if reason == AVAudioSession.InterruptionType.ended.hashValue {
                    self.pause()
                }
            }
        }
    }
    
    @objc func audioHardwareChanged(notification: Notification) {
        if let userInfo = notification.userInfo {
            if let reason = userInfo[AVAudioSessionRouteChangeReasonKey] as? Int {
                if reason == AVAudioSession.RouteChangeReason.oldDeviceUnavailable.hashValue {
                    self.pause()
                }
            }
        }
    }
    
    @objc func newAccessLogEntry(_ notification: Notification) {
        if !self.isCorrectItem(notification.object) {
            return
        }
        
        guard let player: AVPlayer = self.av, let item: AVPlayerItem = player.currentItem else {
            return
        }
        
        self.delegate?.av(player, changePlaybackState: item.status, error: item.error as NSError?)
        self.delegate?.av(player, accessLogWithLog: item.accessLog())
    }
    
    @objc func newErrorLogEntry(_ notification: Notification) {
        if !self.isCorrectItem(notification.object) {
            return
        }
        
        guard let player: AVPlayer = self.av,
            let item: AVPlayerItem = player.currentItem,
            let log = item.errorLog(),
            let event = log.events.get(0)
            else {
                return
        }
        
        if event.errorStatusCode != -12318 {
            self.stop()
            self.delegate?.av(player, errorLogWithLog: item.errorLog(), error: item.error as NSError?)
        }
    }
    
    @objc func failedToPlayToEndTime(_ notification: Notification) {
        if !self.isCorrectItem(notification.object) {
            return
        }
        
        guard let player: AVPlayer = self.av, let item: AVPlayerItem = player.currentItem else {
            return
        }
        
        self.stop()
        self.delegate?.av(player, errorLogWithLog: item.errorLog(), error: item.error as NSError?)
    }
    
    @objc func didPlayToEndTime(_ notification: Notification) {
        if !self.isCorrectItem(notification.object) {
            return
        }
        
        guard let player: AVPlayer = self.av, let item: AVPlayerItem = player.currentItem else {
            return
        }
        
        self.delegate?.av(player, playbackFinish: item.accessLog())
    }
    
    func isCorrectItem(_ object: Any?) -> Bool {
        guard let notificationItem: AVPlayerItem = object as? AVPlayerItem else {
            return false
        }
        
        if self.avItem == notificationItem {
            return true
        }
        
        return false
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}
