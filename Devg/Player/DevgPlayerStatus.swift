//
//  DevgPlayerStatus.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 10..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit

open class DevgPlayerStatus: NSObject {
    public enum Enum: Int {
        case start
        case playing
        case pause
        case stop
        case error
    }
    
    open var index: Int = 2
    
    // MARK: - Overrides
    override init() {
        super.init()
        
        self.setCurrent(DevgPlayerStatus.Enum.stop)
    }
    
    // MARK: - Functions
    open func current() -> DevgPlayerStatus.Enum {
        return DevgPlayerStatus.Enum(rawValue: self.index)!
    }
    
    open func setCurrent(_ status: DevgPlayerStatus.Enum) {
        self.index = status.rawValue
    }
}
