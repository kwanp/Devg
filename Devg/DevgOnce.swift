//
//  DevgOnce.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit
import Devg

public class DevgOnce: NSObject {
    public enum ThreadType {
        case none
        case main
        case background
    }
    
    static var shared: DevgOnce = DevgOnce()
    
    var senders: [AnyObject] = Array<AnyObject>()
    
    public class func run(withSender sender: AnyObject, delay: Double = 0.5, threadType: DevgOnce.ThreadType = .main, action: @escaping () -> Void) {
        DevgOnce.shared.run(withSender: sender, delay: delay, threadType: threadType, action: action)
    }
    
    func run(withSender sender: AnyObject, delay: Double, threadType: DevgOnce.ThreadType, action: @escaping () -> Void) {
        if !self.isExist(ofSender: sender) {
            self.senders.append(sender)
            
            switch threadType {
            case .main:
                DispatchQueue.main.async {
                    action()
                }
                break
            case .background:
                DispatchQueue.global().async {
                    action()
                }
                break
            default:
                action()
                break
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                if let index: Int = self.index(ofSender: sender) {
                    self.senders.remove(at: index)
                }
            }
        }
    }
    
    func isExist(ofSender sender: AnyObject) -> Bool {
        if let _: Int = self.index(ofSender: sender) {
            return true
        }
        
        return false
    }
    
    func index(ofSender sender: AnyObject) -> Int? {
        for index in 0..<senders.count {
            if let object: AnyObject = senders.get(index) {
                if sender === object {
                    return index
                }
            }
        }
        
        return nil
    }
}
