//
//  DevgImageOperationQueue.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit
import Devg

public class DevgImageOperationQueue: NSObject {
    public enum ResizeType: Int {
        case none
        case aspectFit
        case aspectFill
        case widthFit
    }
    
    var imageDownloadOperations: [String: [String: BlockOperation]] = Dictionary<String, Dictionary<String, BlockOperation>>()
    var imageOperation: OperationQueue = OperationQueue()
    
    public static var shared : DevgImageOperationQueue = DevgImageOperationQueue()
    
    public func loadImage(_ imageView: UIImageView, imageUrlString: String, resizeType: ResizeType? = nil, defaultImage: UIImage = UIImage(), indexPath: IndexPath, file: String = #file, completionHandler: ((_ image: UIImage) -> Void)? = nil, errorHandler: ((_ error: NSError?) -> Void)? = nil) {
        let lastFile: String = (file as NSString).lastPathComponent
        var image: UIImage = defaultImage
        if self.checkURL(imageUrlString) {
            var fileName: String
            if let string: String = imageUrlString.toURLEncode(.full) {
                fileName = string.toBase64()
            }
            else {
                fileName = imageUrlString.toBase64()
            }
            var isCached: Bool = false
            let imagePath: String = DevgImageLoader.savePath() + "/" + fileName
            if FileManager.default.fileExists(atPath: imagePath) {
                if let cacheImage: UIImage = UIImage(contentsOfFile: imagePath) {
                    image = cacheImage
                    isCached = true
                }
            }
            
            var newImage: UIImage? = image
            if let type: ResizeType = resizeType {
                switch type {
                case .aspectFill:
                    newImage = image.aspectFill(imageView.bounds.size)
                    break
                case .aspectFit:
                    newImage = image.aspectFit(imageView.bounds.size)
                    break
                case .widthFit:
                    newImage = image.widthFit(imageView.frame.size.width)
                default:
                    break
                }
            }
            
            imageView.image = newImage
            
            if isCached {
                completionHandler?(newImage!)
            }
            else {
                let loadImageIntoCellOp: BlockOperation = BlockOperation()
                loadImageIntoCellOp.addExecutionBlock({ () -> Void in
                    if let url: NSURL = NSURL(string: imageUrlString) {
                        if let imageData: NSData = NSData(contentsOf: url as URL) {
                            if let uiimage: UIImage = UIImage(data: imageData as Data) {
                                if let data: NSData = uiimage.pngData() as NSData? {
                                    data.write(toFile: imagePath, atomically: true)
                                }
                                OperationQueue.main.addOperation({ () -> Void in
                                    if loadImageIntoCellOp.isCancelled == false {
                                        newImage = uiimage
                                        if let type: ResizeType = resizeType {
                                            switch type {
                                            case .aspectFill:
                                                newImage = uiimage.aspectFill(imageView.bounds.size)
                                                break
                                            case .aspectFit:
                                                newImage = uiimage.aspectFit(imageView.bounds.size)
                                                break
                                            case .widthFit:
                                                newImage = uiimage.widthFit(imageView.frame.size.width)
                                            default:
                                                break
                                            }
                                        }
                                        
                                        imageView.image = newImage
                                        completionHandler?(newImage!)
                                    }
                                    
                                    if self.imageDownloadOperations[lastFile] != nil {
                                        self.imageDownloadOperations[lastFile]!.removeValue(forKey: "\(indexPath.section)+\(indexPath.row)")
                                    }
                                })
                            }
                            else {
                                imageView.image = defaultImage
                                let error = NSError(domain: Devg.bundelID(), code: 415, userInfo: ["URL": imageUrlString])
                                errorHandler?(error)
                            }
                        }
                        else {
                            imageView.image = defaultImage
                            let error = NSError(domain: Devg.bundelID(), code: 415, userInfo: ["URL": imageUrlString])
                            errorHandler?(error)
                        }
                    }
                    else {
                        imageView.image = defaultImage
                        let error = NSError(domain: Devg.bundelID(), code: 404, userInfo: ["URL": imageUrlString])
                        errorHandler?(error)
                    }
                })
                
                if self.imageDownloadOperations[lastFile] != nil {
                    self.imageDownloadOperations[lastFile]!["\(indexPath.section)+\(indexPath.row)"] = loadImageIntoCellOp
                }
                else {
                    self.imageDownloadOperations[lastFile] = ["\(indexPath.section)+\(indexPath.row)": loadImageIntoCellOp]
                }
                self.imageOperation.addOperation(loadImageIntoCellOp)
            }
        }
        else {
            imageView.image = defaultImage
            let error = NSError(domain: Devg.bundelID(), code: 404, userInfo: ["URL": imageUrlString])
            errorHandler?(error)
        }
    }
    
    public func removeOperationQueue(_ indexPath: IndexPath, file: String = #file) {
        let lastFile: String = (file as NSString).lastPathComponent
        if let ongoingDownloadOperation: BlockOperation = self.imageDownloadOperations[lastFile]?["\(indexPath.section)+\(indexPath.row)"] {
            ongoingDownloadOperation.cancel()
            self.imageDownloadOperations[lastFile]!.removeValue(forKey: "\(indexPath.section)+\(indexPath.row)")
        }
    }
    
    public func removeAllOperationQueues(file: String = #file) {
        let lastFile: String = (file as NSString).lastPathComponent
        if self.imageDownloadOperations[lastFile] != nil {
            for (key, queue) in self.imageDownloadOperations[lastFile]! {
                queue.cancel()
                self.imageDownloadOperations[lastFile]!.removeValue(forKey: key)
            }
        }
    }
    
    func checkURL(_ urlString: String?) -> Bool {
        if let urlString = urlString {
            if NSURL(string: urlString) != nil {
                return true
            }
        }
        return false
    }
}
