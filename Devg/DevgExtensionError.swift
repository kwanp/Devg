//
//  DevgExtensionError.swift
//  Devg
//
//  Created by Kwan on 29/03/2017.
//  Copyright © 2017 KimMinKu. All rights reserved.
//

import Foundation

extension Error {
    public var code: Int {
        return (self as NSError).code
    }
}
