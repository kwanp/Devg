//
//  DevgExtensionUITableView.swift
//  Devg
//
//  Created by Kwan on 14/02/2017.
//  Copyright © 2017 KimMinKu. All rights reserved.
//

import UIKit

public extension UITableView {
    public func lastIndexPath() -> IndexPath? {
        let lastSection: Int = self.numberOfSections - 1
        if lastSection < 0 {
            return nil
        }
        
        let lastRowForLastSection: Int = self.numberOfRows(inSection: lastSection) - 1
        if lastRowForLastSection < 0 {
            return nil
        }
        
        return IndexPath(row: lastRowForLastSection, section: lastSection)
    }
    
    public func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}
