//
//  DevgWebView.swift
//  Devg
//
//  Created by Kwan on 29/03/2017.
//  Copyright © 2017 Kwan. All rights reserved.
//

import UIKit
import Foundation
import Devg
import WebKit
import DevgOnce

@objc public protocol DevgWebViewDelegate: AnyObject {
    func request(url: String?, _ webView: DevgWebView) -> Bool
    func start(_ webView: DevgWebView, url: String?)
    func end(_ webView: DevgWebView)
    func documentReady(_ webView: DevgWebView)
    func error(_ error: Error, _ webView: DevgWebView)
    func progress(_ progress: Double, _ webView: DevgWebView)
    func title(_ title: String?)
}

open class DevgWebViewPool: WKProcessPool {
    open static let shared: WKProcessPool = WKProcessPool()
}

open class DevgWebView: UIView, WKScriptMessageHandler {
    open weak var delegate: DevgWebViewDelegate?
    open var isCookieSaveToFile: Bool = true
    
    open var isCache: Bool = true
    
    internal var webKit: WKWebView!
    var isFirstLoad: Bool = true
    
    open var fingerPrint: String?
    open var fingerPrintCompletion: ((_ result: String?) -> Void)?
    
    open func defaultUserAgent(_ completion:@escaping (_ userAgent: String) -> Void) {
        let webKit: WKWebView = WKWebView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        self.addSubview(webKit)
        webKit.loadHTMLString("<HTML></HTML>", baseURL: nil)
        webKit.evaluateJavaScript("navigator.userAgent") { (string, error) in
            completion(String(describing: string).unwrap())
            webKit.removeFromSuperview()
        }
    }
    
    open var url: String? {
        return self.webKit.url?.absoluteString
    }
    
    open var title: String? {
        return self.webKit.title
    }
    
    open var contentInset: UIEdgeInsets {
        get {
            return self.webKit.scrollView.contentInset
        }
        set(value) {
            self.webKit.scrollView.contentInset = value
        }
    }
    
    open var userAgent: String? {
        get {
            return self.webKit.customUserAgent
        }
        set(value) {
            self.webKit.customUserAgent = value
        }
    }
    
    open var canBack: Bool {
        return self.webKit.canGoBack
    }
    
    open var canForward: Bool {
        return self.webKit.canGoForward
    }
    
    override init(frame: CGRect) {
        fatalError("Use the init(coder:)")
    }
    
    init() {
        fatalError("Use the init(coder:)")
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        let userContent: WKUserContentController = WKUserContentController();
        let documentReady: WKUserScript = WKUserScript(source: "webkit.messageHandlers.documentReady.postMessage(1);", injectionTime: WKUserScriptInjectionTime.atDocumentEnd, forMainFrameOnly: true)
        userContent.addUserScript(documentReady)
        userContent.add(self, name: "documentReady")
        
        userContent.add(self, name: "fingerPrint")
        
        let configuration: WKWebViewConfiguration = WKWebViewConfiguration()
        configuration.userContentController = userContent
        configuration.processPool = DevgWebViewPool.shared
        self.webKit = WKWebView(frame: self.bounds, configuration: configuration)
        for subView: UIView in self.subviews {
            if let scrollView: UIScrollView = subView as? UIScrollView {
                scrollView.scrollsToTop = false
            }
        }
        
        self.webKit.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        
        self.webKit.allowsLinkPreview = false
        
        self.webKit.isOpaque = false
        self.webKit.backgroundColor = UIColor.clear
        
        self.webKit.navigationDelegate = self
        self.webKit.uiDelegate = self
        self.webKit.scrollView.bounces = false
        self.webKit.scrollView.scrollsToTop = true
        
        self.addSubview(self.webKit, topConstant: 0, leftConstant: 0, rightConstant: 0, bottomConstant: 0)
    }
    
    public func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "documentReady" {
            self.javascript("window.location.href") { (result, error) in
                self.cookieLoad(url: result ?? "")
            }
            
            self.javascript("document.title", completion: { (result, error) in
                self.delegate?.title(result)
            })
            self.delegate?.documentReady(self)
        }
        if message.name == "fingerPrint" {
            let result: String? = message.body as? String
            self.fingerPrint = result
            self.fingerPrintCompletion?(result)
        }
    }
    
    override open func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            self.delegate?.progress(self.webKit.estimatedProgress, self)
            if self.webKit.estimatedProgress == 1 {
                Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(DevgWebView.isEnd), userInfo: nil, repeats: false)
            }
        }
    }
    
    deinit {
        self.webKit.removeObserver(self, forKeyPath: "estimatedProgress")
    }
}

extension DevgWebView {
    func cookieName(url: String) -> String {
        guard let domain: String = URL(string: url)?.host else {
            return "COOKIE"
        }
        
        let subStrings: [String] = domain.components(separatedBy: ".")
        var firstName: String = ""
        let count: Int = subStrings.count
        if count > 2 {
            firstName = subStrings[count - 2] + "." + subStrings[count - 1]
        }
        else if count == 2 {
            firstName = domain
        }
        
        return "COOKIE_\(firstName)"
    }
    
    func cookieLoad(url: String) {
        if self.isFirstLoad && isCookieSaveToFile {
            self.isFirstLoad = false
            if let string: String = Devg.getContents(name: self.cookieName(url: url)) {
                let cookies: [String] = string.components(separatedBy: " ")
                var javascript: String = ""
                for cookie: String in cookies {
                    javascript = javascript + "document.cookie='\(cookie)';"
                }
                self.javascript("\(javascript) 'set cookies';", completion: { (result, error) in
                    self.reload()
                })
            }
        }
    }
    
    func cookieSave(url: String) {
        if !self.isFirstLoad && isCookieSaveToFile {
            self.javascript("document.cookie", completion: { (result, error) in
                if let string: String = result {
                    if !string.isEmpty {
                        let _: Bool = Devg.createFile(name: self.cookieName(url: url), contents: string)
                    }
                }
            })
        }
    }
}

extension DevgWebView {
    @objc func isEnd() {
        if self.webKit.estimatedProgress == 1 {
            self.delegate?.end(self)
        }
    }
}

extension DevgWebView {
    open func load(url: String, headers: [String: String]? = nil) {
        if let urlObject: URL = URL(string: url) {
            var request: URLRequest = URLRequest(url: urlObject)
            self.isFirstLoad = true
            if isCookieSaveToFile {
                if let string: String = Devg.getContents(name: self.cookieName(url: url)) {
                    request.setValue(string, forHTTPHeaderField: "Cookie")
                }
            }
            if let headers: [String: String] = headers {
                for (key, value) in headers {
                    request.setValue(value, forHTTPHeaderField: key)
                }
            }
            self.webKit.load(request as URLRequest)
        }
        else {
            self.delegate?.error(NSError(domain: url, code: 400, userInfo: nil) as Error, self)
        }
    }
    
    open func removeCache() {
        let websiteDataTypes = NSSet(array: [WKWebsiteDataTypeDiskCache, WKWebsiteDataTypeMemoryCache])
        let date = NSDate(timeIntervalSince1970: 0)
        WKWebsiteDataStore.default().removeData(ofTypes: websiteDataTypes as! Set, modifiedSince: date as Date, completionHandler:{ })
    }
    
    open func javascript(_ script: String, completion: ((_ result: String?, _ error: Error?) -> Void)?) {
        self.webKit.evaluateJavaScript(script) { (object, error) in
            completion?(object as? String, error)
        }
    }
    
    open func back() {
        self.webKit.goBack()
    }
    
    open func forward() {
        self.webKit.goForward()
    }
    
    open func reload() {
        self.webKit.reload()
    }
    
    open func scrollToTop(animated: Bool) {
        self.webKit.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: animated)
    }
    
    open func fingerPrint(completion: @escaping (_ result: String?) -> Void) {
        if let string: String = self.fingerPrint {
            completion(string)
            return
        }
        
        guard let path: String = Bundle(identifier: "net.devg.DevgWebView")?.path(forResource: "fingerprint2", ofType: "js") else {
            completion(nil)
            return
        }
        
        var getScript: String?
        do {
            getScript = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
        }
        catch {
            completion(nil)
            return
        }
        
        guard let fingerPrintScript: String = getScript else {
            completion(nil)
            return
        }
        
        let optionsScript: String = "var options = {excludeScreenResolution: true, excludeAvailableScreenResolution: true, excludeSessionStorage: true, excludeOpenDatabase: true, excludePlatform: true, excludeCanvas: true, excludeWebGL: true, excludeUserAgent: true, excludeLanguage: true};"
        let runScript: String = "new Fingerprint2(options).get(function(result){webkit.messageHandlers.fingerPrint.postMessage(result);});"
        
        self.javascript(fingerPrintScript+optionsScript+runScript) { (result, error) in
            if error != nil {
                completion(nil)
                return
            }
            
            self.fingerPrintCompletion = completion
            return
        }
    }
}

extension DevgWebView: WKUIDelegate, WKNavigationDelegate {
    public func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        e("웹킷 고질 오류. 흰화면.")
        DevgOnce.run(withSender: webView) {
            webView.reload()
        }
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation) {
        
    }
    
    public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.delegate?.error(error, self)
    }
    
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: (@escaping (WKNavigationActionPolicy) -> Void)) {
        
        self.cookieSave(url: navigationAction.request.url?.absoluteString ?? "")
        
        if !self.isCache {
            self.removeCache()
        }
        
        if let isMain: Bool = navigationAction.targetFrame?.isMainFrame {
            if isMain {
                self.delegate?.start(self, url: navigationAction.request.url?.absoluteString)
            }
        }
        
        guard let delegate: DevgWebViewDelegate = self.delegate else {
            decisionHandler(.allow)
            return
        }
        
        if delegate.request(url: navigationAction.request.url?.absoluteString, self) {
            decisionHandler(.allow)
        }
        else {
            decisionHandler(.cancel)
        }
    }
    
    public func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                        completionHandler: @escaping () -> Void) {
        
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "확인", style: .default, handler: { (action) in
            completionHandler()
        }))
        
        Devg.visibleViewController().present(alertController, animated: true, completion: nil)
    }
    
    
    public func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                        completionHandler: @escaping (Bool) -> Void) {
        
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "확인", style: .default, handler: { (action) in
            completionHandler(true)
        }))
        
        alertController.addAction(UIAlertAction(title: "취소", style: .default, handler: { (action) in
            completionHandler(false)
        }))
        
        Devg.visibleViewController().present(alertController, animated: true, completion: nil)
    }
    
    
    public func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo,
                        completionHandler: @escaping (String?) -> Void) {
        
        let alertController = UIAlertController(title: "", message: prompt, preferredStyle: .alert)
        
        alertController.addTextField { (textField) in
            textField.text = defaultText
        }
        
        alertController.addAction(UIAlertAction(title: "확인", style: .default, handler: { (action) in
            if let text = alertController.textFields?.first?.text {
                completionHandler(text)
            } else {
                completionHandler(defaultText)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "취소", style: .default, handler: { (action) in
            completionHandler(nil)
        }))
        
        Devg.visibleViewController().present(alertController, animated: true, completion: nil)
    }
    
    public func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame == nil {
            webView.load(navigationAction.request)
        }
        return nil
    }
}
